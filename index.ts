import { toastOptions } from "@/const";

import useGenericFormFieldElements from "./src/components/GenericFormFields";
import sleep from "./src/hooks/form/utils/sleep";

import "./src/styles/index.sass";

import {
	TNormalFieldProps,
	TRadioButtonsGroupFieldProps,
	TSelectFieldProps,
	TVerifyInputMUIProps,
} from "@/types/components";
import { TUseControlledFormParams, TUseControlledFormResult } from "@/types/controlledFormHook";
import { TCountry } from "@/types/countryRegion";
import { TErrors, TErrorsCustomSetter, TNormalError, TVerificationCodeError } from "@/types/errors";
import {
	CustomKeyboardEvent,
	FormBlurEvent,
	FormButtonBlurEvent,
	FormChangeEvent,
	FormDivBlurEvent,
	FormInputChangeEvent,
	FormMultipleSelectMUIChangeEvent,
	FormNormalSelectChangeEvent,
	FormNormalSelectMUIChangeEvent,
	FormPhoneChangeEvent,
	FormSubmitEvent,
	FormVerificationCodeBlurEvent,
	FormVerificationCodeChangeEvent,
	FormVerificationCodeKeyDownEvent,
	SelectChangeEvent,
} from "@/types/events";
import {
	FieldTypes,
	TCheckboxField,
	TCountryRegionSelectField,
	TDateField,
	TEmailField,
	TMultipleSelectField,
	TNormalField,
	TNormalSelectField,
	TRadioGroupField,
	TVerificationCodeField,
} from "@/types/fields";
import { TUseFormFieldParams, TUseFormFieldResult } from "@/types/formFieldHook";
import {
	CheckboxGroupValueDef,
	CheckboxValueDef,
	CountryRegionSelectValueDef,
	CustomValueDef,
	DateValueDef,
	EmailValueDef,
	IntersectionOf,
	MultilineValueDef,
	MultipleSelectValueDef,
	NormalSelectValueDef,
	NormalValueDef,
	RadioGroupValueDef,
	TFieldDef,
	TFieldDefOut,
	TFieldsDef,
	TFieldsDefOut,
	TFormDef,
	TFormDefOut,
	TFormsDef,
	TFormsDefOut,
	TFormTypeInputDef,
	TFormValueDef,
	TFormValuesDef,
	VerificationCodeValueDef,
} from "@/types/formModel";
import {
	StepSetStateAction,
	StepSetStateActionData,
	StepSetStateActionFunction,
	StepSetStateActionFunctionParams,
	StepSetStateActionFunctionResult,
	TUseFormStepParams,
	TUseFormStepResult,
} from "@/types/formStepHook";
import { TUseFormValuesParams, TUseFormValuesResult } from "@/types/formValuesHook";
import {
	TGenericNormalFieldProps,
	TGenericRadioButtonsGroupFieldProps,
	TGenericSelectFieldProps,
	UseGenericFormFieldsProps,
} from "@/types/genericFormFieldHook";
import {
	CheckboxInputValue,
	DateInputValue,
	EmailInputValue,
	MultipleSelectInputValue,
	NormalInputValue,
	NormalSelectInputValue,
	RadioGroupInputValue,
	SelectInputValue,
	VerificationCodeInputValue,
} from "@/types/inputValues";
import { TLanguage } from "@/types/language";
import {
	TRadioGroupOption,
	TRadioGroupOptions,
	TRadioGroupOptionsFunct,
	TSelectOption,
	TSelectOptions,
	TSelectOptionsFunct,
} from "@/types/options";
import {
	NormalTRef,
	RadioGroupTRef,
	SelectTRef,
	TGetRef,
	TRef,
	TRefCurrent,
	TRefObject,
	TRefs,
} from "@/types/refs";
import {
	TCustomValidationNormal,
	TValidationFunctions,
	TValidationGroup,
	TValidationGroupKeys,
	ValidateOne,
} from "@/types/validations";

import ControlledFormWrapper from "@/components/ControlledFormWrapper";
import OutlinedDiv from "@/components/GenericFormFields/CustomFields/OutlinedDiv";
import withCustomField from "@/components/GenericFormFields/CustomFields/withCustomField";
import Timer from "@/components/Timer";
import useFormField from "@/hooks/form/hooks/useFormField";
import useFormStep from "@/hooks/form/hooks/useFormStep";
import useFormValues from "@/hooks/form/hooks/useFormValues";
import useTimer from "@/hooks/form/hooks/useTimer";
import useControlledForm from "@/hooks/form/useControlledForm";
import buildFormModel from "@/hooks/form/utils/buildFormModel";
import buildForms from "@/hooks/form/utils/buildForms";
import removeUndefinedProps from "@/hooks/form/utils/removeUndefinedProps";

export {
	toastOptions,
	useGenericFormFieldElements,
	OutlinedDiv,
	withCustomField,
	sleep,
	removeUndefinedProps,
	useControlledForm,
	useFormField,
	useFormValues,
	useFormStep,
	useTimer,
	Timer,
	buildFormModel,
	buildForms,
	ControlledFormWrapper,
};

export type {
	// Components
	TNormalFieldProps,
	TSelectFieldProps,
	TRadioButtonsGroupFieldProps,
	TVerifyInputMUIProps,

	// Controlled form hook
	TUseControlledFormParams,
	TUseControlledFormResult,

	// Country region
	TCountry,

	// Errors
	TErrors,
	TErrorsCustomSetter,
	TNormalError,
	TVerificationCodeError,

	// Events
	SelectChangeEvent,
	FormPhoneChangeEvent,
	FormVerificationCodeChangeEvent,
	FormNormalSelectMUIChangeEvent,
	FormMultipleSelectMUIChangeEvent,
	FormNormalSelectChangeEvent,
	FormInputChangeEvent,
	FormChangeEvent,
	FormButtonBlurEvent,
	FormDivBlurEvent,
	FormBlurEvent,
	FormVerificationCodeBlurEvent,
	CustomKeyboardEvent,
	FormVerificationCodeKeyDownEvent,
	FormSubmitEvent,

	// Fields
	TNormalField,
	TVerificationCodeField,
	TCheckboxField,
	TEmailField,
	TDateField,
	TCountryRegionSelectField,
	TNormalSelectField,
	TMultipleSelectField,
	TRadioGroupField,
	FieldTypes,

	// Form field hook
	TUseFormFieldParams,
	TUseFormFieldResult,

	// Form step hook
	StepSetStateAction,
	StepSetStateActionData,
	StepSetStateActionFunction,
	StepSetStateActionFunctionParams,
	StepSetStateActionFunctionResult,
	TUseFormStepParams,
	TUseFormStepResult,

	// Form model
	NormalValueDef,
	MultilineValueDef,
	VerificationCodeValueDef,
	CheckboxValueDef,
	EmailValueDef,
	DateValueDef,
	CountryRegionSelectValueDef,
	NormalSelectValueDef,
	MultipleSelectValueDef,
	RadioGroupValueDef,
	CheckboxGroupValueDef,
	CustomValueDef,
	TFormValueDef,
	TFieldDef,
	TFieldDefOut,
	TFieldsDef,
	TFieldsDefOut,
	TFormDef,
	TFormDefOut,
	TFormsDef,
	TFormsDefOut,
	IntersectionOf,
	TFormValuesDef,
	TFormTypeInputDef,

	// Form values hook
	TUseFormValuesParams,
	TUseFormValuesResult,

	// Generic form field hook
	TGenericNormalFieldProps,
	TGenericRadioButtonsGroupFieldProps,
	TGenericSelectFieldProps,
	UseGenericFormFieldsProps,

	// Input values
	NormalInputValue,
	DateInputValue,
	NormalSelectInputValue,
	RadioGroupInputValue,
	MultipleSelectInputValue,
	SelectInputValue,
	EmailInputValue,
	VerificationCodeInputValue,
	CheckboxInputValue,

	// Language
	TLanguage,

	// Options
	TRadioGroupOption,
	TRadioGroupOptions,
	TRadioGroupOptionsFunct,
	TSelectOption,
	TSelectOptions,
	TSelectOptionsFunct,

	// Refs
	TGetRef,
	TRefCurrent,
	NormalTRef,
	SelectTRef,
	RadioGroupTRef,
	TRefObject,
	TRef,
	TRefs,

	// Validations
	TCustomValidationNormal,
	TValidationGroupKeys,
	TValidationGroup,
	TValidationFunctions,
	ValidateOne,
};
