import { FC, useEffect, useImperativeHandle, useRef, useState } from "react";
import { Box, Typography } from "@mui/material";

import { CustomFieldChangeEvent } from "@/types/events";
import { CustomTRef } from "@/types/refs";

import OutlinedDiv from "@/components/GenericFormFields/CustomFields/OutlinedDiv";

type MyCustomFieldProps = {
	label: string;
	name: string;
	fieldRef: CustomTRef;
	error?: boolean;
	errorText?: string;
	onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
	onChange: (e: CustomFieldChangeEvent) => void;
};

const MyCustomField: FC<MyCustomFieldProps> = ({
	label,
	name,
	fieldRef,
	error,
	errorText,
	onChange,
	onBlur,
}) => {
	const [value, setValue] = useState("");
	const divRef = useRef<HTMLDivElement>(null);
	const buttonRef = useRef<HTMLButtonElement>(null);

	useEffect(() => {
		const e: CustomFieldChangeEvent = {
			target: {
				value: value,
				name,
			},
		};
		onChange(e);
	}, [value]);

	const handleFocus = () => {
		buttonRef.current?.focus();
	};

	useImperativeHandle(fieldRef, () => ({
		...(divRef.current as HTMLDivElement),
		value,
		focus: handleFocus,
	}));

	const handleChange = (value: string) => setValue(value);

	return (
		<Box ref={divRef}>
			<OutlinedDiv label={label} error={error} errorText={errorText} onBlur={onBlur} fullWidth>
				<Typography variant="h5">Custom field name: {name}</Typography>
				<Typography variant="h5">Custom field label: {label}</Typography>
				<Typography variant="h5">Custom field value: {value}</Typography>
				<button ref={buttonRef} type="button" onClick={() => handleChange("Value 1")}>
					Value 1
				</button>
				<button type="button" onClick={() => handleChange("Value 2")}>
					Value 2
				</button>
				<button type="button" onClick={() => handleChange("Value 3")}>
					Value 3
				</button>
				<button type="button" onClick={() => handleChange("Value 4")}>
					Value 4
				</button>
			</OutlinedDiv>
		</Box>
	);
};

export default MyCustomField;
