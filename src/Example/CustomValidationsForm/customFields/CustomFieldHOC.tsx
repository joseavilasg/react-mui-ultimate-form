import { useCallback } from "react";
import { myForms } from "@/Example/formsModel";
import { Box, Typography } from "@mui/material";

import OutlinedDiv from "@/components/GenericFormFields/CustomFields/OutlinedDiv";
import withCustomField from "@/components/GenericFormFields/CustomFields/withCustomField";
import useFormField from "@/hooks/form/hooks/useFormField";

const CustomFieldHOC = () => {
	const { field, value, required, error, errorText, handleBlur, handleChange, ref } = useFormField(
		{
			formModel: myForms["customValidationsForm"],
			fieldName: "custom",
		},
	);

	return useCallback(
		withCustomField(
			({
				name,
				label,
				value,
				required,
				setValue,
				error,
				errorText,
				onBlur,
				toFocusRef,
				toContainerRef,
			}) => {
				const handleChange = (value: string) => setValue(value);
				return (
					<Box ref={toContainerRef}>
						<OutlinedDiv
							label={label}
							required={required}
							error={error}
							errorText={errorText}
							onBlur={onBlur}
							fullWidth
						>
							<Typography variant="h5">Custom field name: {name}</Typography>
							<Typography variant="h5">Custom field label: {label}</Typography>
							<Typography variant="h5">Custom field value: {value.data}</Typography>
							<button ref={toFocusRef} type="button" onClick={() => handleChange("Value 1")}>
								Value 1
							</button>
							<button type="button" onClick={() => handleChange("Value 2")}>
								Value 2
							</button>
							<button type="button" onClick={() => handleChange("Value 3")}>
								Value 3
							</button>
							<button type="button" onClick={() => handleChange("Value 4")}>
								Value 4
							</button>
						</OutlinedDiv>
					</Box>
				);
			},
		),
		[],
	)({
		...field,
		required,
		value,
		error,
		errorText,
		onChange: handleChange,
		onBlur: handleBlur,
		fieldRef: ref,
	});
};

export default CustomFieldHOC;
