import ControlledFormWrapper from "@/components/ControlledFormWrapper";

import { myForms } from "../formsModel";
import Step1 from "./steps/Step1";
import Step2 from "./steps/Step2";
import Step3 from "./steps/Step3";

const CustomValidationsForm = () => {
	return (
		<ControlledFormWrapper
			formModel={myForms["customValidationsForm"]}
			onSuccessSubmit={({ values, stepInfo: { activeStep, setActiveStep } }) => {
				if (activeStep === 0) {
					setActiveStep(1);
				} else if (activeStep === 1) {
					setActiveStep(2);
				} else {
					console.log({ values });
					if (values.custom.type === "DateInputValue") {
						values.custom.data;
					}
				}
			}}
			onBack={({ activeStep, setActiveStep }) => {
				setActiveStep(activeStep - 1);
			}}
			// disableInternalMainHook
		>
			{[Step1, Step2, Step3]}
		</ControlledFormWrapper>
	);
};

export default CustomValidationsForm;
