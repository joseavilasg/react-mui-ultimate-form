import { myForms } from "@/Example/formsModel";

import useGenericFormFieldElements from "@/components/GenericFormFields";

const Step3 = () => {
	const {
		components: { InputField },
		steps: {
			fields: { lastnames },
		},
	} = useGenericFormFieldElements({ formModel: myForms["customValidationsForm"] });

	return (
		<>
			<InputField {...lastnames} />
		</>
	);
};

export default Step3;
