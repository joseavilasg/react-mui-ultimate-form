import { myForms } from "@/Example/formsModel";
import { Typography } from "@mui/material";

import useGenericFormFieldElements from "@/components/GenericFormFields";

const Step1 = () => {
	const {
		components: { InputField },
		steps: {
			title,
			fields: { name },
		},
	} = useGenericFormFieldElements({ formModel: myForms["customValidationsForm"] });

	return (
		<>
			<Typography mb={2}>{title}</Typography>
			<InputField {...name} />
		</>
	);
};

export default Step1;
