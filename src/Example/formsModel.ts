import buildFormModel from "@/hooks/form/utils/buildFormModel";
import buildForms from "@/hooks/form/utils/buildForms";

const firstForm = buildFormModel({
	formId: "firstForm",
	steps: [
		{
			stepId: "1",
			stepType: "Constructive",
			fields: {
				username: {
					name: "username",
					label: "Username",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
						isNotEmail: [
							true,
							"No se permite una dirección de correo como nombre de usuario",
						],
					},
				},
				password: {
					name: "password",
					label: "Password",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				email: {
					name: "email",
					label: "Email",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
						isEmail: true,
					},
				},
				verificationCode: {
					name: "verificationCode",
					label: "Verification code",
					type: "VerificationCodeInputValue",
					initialValue: Array(5).fill(""),
					validations: {
						isRequired: true,
						codeLength: 5,
					},
				},
				lastnames: {
					name: "lastnames",
					label: "Apellidos",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				names: {
					name: "names",
					label: "Names",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				genre: {
					name: "genre",
					label: "Genre",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				phone: {
					name: "phone",
					label: "Mobile phone",
					type: "NormalInputValue",
					initialValue: "",
					validations: { isRequired: true, isPhoneNumber: true },
				},
				birthday: {
					name: "birthday",
					label: "Birthday",
					type: "DateInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
						isNotGreaterThanToday: true,
						isNotLessThan: "01/01/2000",
					},
				},
				interests: {
					name: "interests",
					label: "Interests",
					type: "NormalSelectInputValue",
					initialValue: "",
					options: [
						{ value: 1, name: "Music" },
						{ value: 2, name: "Movies" },
						{ value: 3, name: "Series" },
					],
					validations: {
						isRequired: true,
					},
				},
				favSongs: {
					name: "favSongs",
					label: "What are your favorite songs?",
					type: "MultipleSelectInputValue",
					initialValue: [],
					options: [
						{
							value: 1,
							name: "weapon, Against the Current",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				about: {
					name: "about",
					label: "Tell us about you",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				country: {
					name: "country",
					label: "Country",
					type: "CountryRegionSelectField",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				region: {
					name: "region",
					label: "Region",
					type: "CountryRegionSelectField",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				province: {
					name: "province",
					label: "Province",
					type: "CountryRegionSelectField",
					initialValue: "",
					validations: {
						isRequired: false, // If true, won't work
					},
				},
				district: {
					name: "district",
					label: "District",
					type: "CountryRegionSelectField",
					initialValue: "",
					validations: {
						isRequired: false, // If true, won't work
					},
				},
				privacyPolicy: {
					name: "privacyPolicy",
					label: "I accept that my data is used according to your [privacy policy](https://google.com/).",
					type: "CheckboxInputValue",
					initialValue: false,
					validations: { isRequired: true },
				},
			},
		},
	],
});

const multistepForm = buildFormModel({
	formId: "multistepForm",
	steps: [
		{
			stepId: "1",
			stepType: "Constructive",
			fields: {
				names: {
					name: "names",
					label: "Names",
					type: "NormalInputValue",
					initialValue: "",
					validations: { isRequired: true },
				},
			},
		},
		{
			stepId: "2",
			stepType: "Informative",
			title: "This step is only to give indications.",
		},
		{
			stepId: "3",
			stepType: "Constructive",
			fields: {
				lastnames: {
					name: "lastnames",
					label: "Lastnames",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
			},
		},
	],
});

const withPersistedValuesForm = buildFormModel({
	formId: "withPersistedValuesForm",
	steps: [
		{
			stepId: "1",
			stepType: "Constructive",
			fields: {
				genre: {
					name: "genre",
					label: "Genre",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				favFood: {
					name: "favFood",
					label: "Fav food",
					type: "CheckboxGroupInputValue",
					initialValue: [],
					options: [
						{
							value: "1",
							name: "Rice",
						},
						{
							value: "2",
							name: "Chicken",
						},
						{
							value: "3",
							name: "Rabbit",
						},
						{
							value: "4",
							name: "Rabbit",
						},
						{
							value: "5",
							name: "Rabbit",
						},
						{
							value: "6",
							name: "Rabbit",
						},
						{
							value: "7",
							name: "Rabbit",
						},
						{
							value: "8",
							name: "Rabbit",
						},
						{
							value: "9",
							name: "Rabbit",
						},
						{
							value: "10",
							name: "Rabbit",
						},
						{
							value: "11",
							name: "Rabbit",
						},
						{
							value: "12",
							name: "Rabbit",
						},
						{
							value: "13",
							name: "Rabbit",
						},
						{
							value: "14",
							name: "Rabbit",
						},
						{
							value: "15",
							name: "Rabbit",
						},
						{
							value: "16",
							name: "Rabbit",
						},
						{
							value: "17",
							name: "Rabbit",
						},
						{
							value: "18",
							name: "Rabbit",
						},
						{
							value: "19",
							name: "Rabbit",
						},
						{
							value: "20",
							name: "Rabbit",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				names: {
					name: "names",
					label: "Names",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				lastnames: {
					name: "lastnames",
					label: "Lastnames",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
				favMusic: {
					name: "favMusic",
					label: "Fav music",
					type: "MultipleSelectInputValue",
					initialValue: [],
					options: [],
					validations: {
						isRequired: true,
					},
				},
				interests: {
					name: "interests",
					label: "Interests",
					type: "NormalSelectInputValue",
					initialValue: "",
					options: [],
					validations: {
						isRequired: true,
					},
				},
				slug: {
					name: "slug",
					label: "Slug",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
						isSlug: true,
					},
				},
			},
		},
	],
});

const customValidationsForm = buildFormModel({
	formId: "customValidationsForm",
	steps: [
		{
			stepId: "1",
			stepType: "Constructive",
			title: "Title step 1: Complete this field",
			fields: {
				name: {
					name: "name",
					label: "Names",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						// Validators order matters
						isRequired: true,
						custom: (value) => {
							// console.log({ value });
							return [
								value === "Jose",
								"Custom validation example string error. (Value must be equal to 'Jose')",
							];
						},
					},
				},
			},
		},
		{
			stepId: "1",
			stepType: "Constructive",
			fields: {
				custom: {
					name: "custom",
					label: "Custom",
					type: "AnyNameCustomField",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
			},
		},
		{
			stepId: "1",
			stepType: "Constructive",
			fields: {
				lastnames: {
					name: "lastnames",
					label: "Lastnames",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						// Validators order matters
						isRequired: true,
					},
				},
			},
		},
	],
});

const withTimerForm = buildFormModel({
	formId: "withTimerForm",
	steps: [
		{
			stepType: "Constructive",
			minTimeMs: 15000,
			fields: {
				name: {
					name: "name",
					label: "Names",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						isRequired: true,
					},
				},
			},
		},
		{
			stepType: "Constructive",
			minTimeMs: 10000,
			fields: {
				lastnames: {
					name: "lastnames",
					label: "Lastnames",
					type: "NormalInputValue",
					initialValue: "",
					validations: {
						// Validators order matters
						isRequired: true,
					},
				},
			},
		},
	],
});

const radioButtonsForm = buildFormModel({
	formId: "radioButtonsForm",
	steps: [
		{
			stepType: "Constructive",
			fields: {
				genre1: {
					name: "genre1",
					label: "Genre 1 => A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it. A very large label by repeating it.",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre2: {
					name: "genre2",
					label: "Genre 2",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre3: {
					name: "genre3",
					label: "Genre 3",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre4: {
					name: "genre4",
					label: "Genre 4",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre5: {
					name: "genre5",
					label: "Genre 5",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre6: {
					name: "genre6",
					label: "Genre 6",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre7: {
					name: "genre7",
					label: "Genre 7",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre8: {
					name: "genre8",
					label: "Genre 8",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre9: {
					name: "genre9",
					label: "Genre 9",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre10: {
					name: "genre10",
					label: "Genre 10",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre11: {
					name: "genre11",
					label: "Genre 11",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre12: {
					name: "genre12",
					label: "Genre 12",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre13: {
					name: "genre13",
					label: "Genre 13",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre14: {
					name: "genre14",
					label: "Genre 14",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre15: {
					name: "genre15",
					label: "Genre 15",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre16: {
					name: "genre16",
					label: "Genre 16",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre17: {
					name: "genre17",
					label: "Genre 17",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre18: {
					name: "genre18",
					label: "Genre 18",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre19: {
					name: "genre19",
					label: "Genre 19",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
				genre20: {
					name: "genre20",
					label: "Genre 20",
					type: "RadioGroupInputValue",
					initialValue: "",
					options: [
						{
							value: "1",
							name: "Male",
						},
						{
							value: "2",
							name: "Female",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					],
					validations: {
						isRequired: true,
					},
				},
			},
		},
	],
});

export const myForms = buildForms(
	firstForm,
	multistepForm,
	withPersistedValuesForm,
	customValidationsForm,
	withTimerForm,
	radioButtonsForm,
);
