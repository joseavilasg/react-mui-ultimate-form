import { toastOptions } from "@/const";
import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import ArrowBackOutlinedIcon from "@mui/icons-material/ArrowBackOutlined";
import SendIcon from "@mui/icons-material/Send";
import { Box, Button } from "@mui/material";
import { toast } from "react-toastify";
import { useShallow } from "zustand/react/shallow";

import useFormStep from "@/hooks/form/hooks/useFormStep";
import useControlledForm from "@/hooks/form/useControlledForm";

import { myForms } from "../formsModel";
import Step1Form from "./Forms/Step1Form";
import Step2Form from "./Forms/Step2Form";

const MultistepForm = () => {
	const { activeStep, setActiveStep } = useFormStep({ formModel: myForms["multistepForm"] });
	const { values, handleSubmit } = useControlledForm({
		formModel: myForms["multistepForm"],
		activeStep,
		lang: "en",
	});

	function renderStepContent(step: number) {
		switch (step) {
			case 0:
				return <Step1Form />;
			case 1:
				return <>Title</>;
			case 2:
				return <Step2Form />;
			default:
				return <div>Not Found</div>;
		}
	}

	const handleBack = () => {
		setActiveStep(activeStep - 1);
	};

	const { formsState } = useBoundStore<string, string, FormsSlice<string, string>>(
		useShallow((s) => s),
	);
	console.log({ formsState });

	return (
		<Box
			component="form"
			sx={{
				"& > :not(style)": { m: 2 },
				"& .MuiTypography-root": {
					width: 1,
				},
				"& .MuiSubtitle": {
					mb: 0,
				},
				"& .MuiDescription": {},
				"& .ResendCodeOption, & .ChangeEmailOption": { width: 1, my: 0 },
				"& .ChangeEmailOption": { mb: 2 },
				width: 1,
			}}
			margin="auto"
			display="flex"
			flexWrap="wrap"
			justifyContent="center"
			alignItems="center"
			noValidate
			autoComplete="off"
			onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
				e.preventDefault();
				handleSubmit(() => {
					if (activeStep === 0) {
						setActiveStep(1);
					} else if (activeStep === 1) {
						setActiveStep(2);
					} else {
						console.log({ values });
						toast("Values printed in the console.", {
							...toastOptions,
							toastId: "multistep-fom",
							type: "success",
						});
					}
				}, "multistep-form");
			}}
		>
			{renderStepContent(activeStep)}
			{activeStep !== 0 && (
				<Button
					variant="outlined"
					color="primary"
					startIcon={<ArrowBackOutlinedIcon />}
					onClick={handleBack}
				>
					Atrás
				</Button>
			)}
			<Button type="submit" variant="contained" endIcon={<SendIcon />}>
				{activeStep === 2 ? "Send" : "Next"}
			</Button>
		</Box>
	);
};

export default MultistepForm;
