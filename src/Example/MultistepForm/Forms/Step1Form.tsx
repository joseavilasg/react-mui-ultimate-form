import { myForms } from "@/Example/formsModel";

import useGenericFormFieldElements from "@/components/GenericFormFields";

const Step1Form = () => {
	const {
		components: { InputField },
		steps: {
			fields: { names },
		},
	} = useGenericFormFieldElements({
		formModel: myForms["multistepForm"],
	});
	return (
		<>
			<InputField {...names} />
		</>
	);
};

export default Step1Form;
