import { myForms } from "@/Example/formsModel";

import useGenericFormFieldElements from "@/components/GenericFormFields";

const Step2Form = () => {
	const {
		components: { InputField },
		steps: {
			fields: { lastnames },
		},
	} = useGenericFormFieldElements({
		formModel: myForms["multistepForm"],
	});
	console.log({ lastnames });
	return (
		<>
			<InputField {...lastnames} />
		</>
	);
};

export default Step2Form;
