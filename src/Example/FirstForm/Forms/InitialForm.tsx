import { myForms } from "@/Example/formsModel";

import useGenericFormFieldElements from "@/components/GenericFormFields";
import sleep from "@/hooks/form/utils/sleep";

const InitialForm = () => {
	const {
		components: {
			CheckBoxField,
			InputField,
			PasswordField,
			VerifyInputField,
			RadioButtonsGroupField,
			MobilePhoneField,
			DateField,
			SelectField,
			MultipleSelectField,
			MultilineInputField,
			CountryRegionSelectorFields,
		},
		steps: {
			fields: {
				username,
				password,
				email,
				verificationCode,
				lastnames,
				names,
				genre,
				phone,
				birthday,
				interests,
				favSongs,
				about,
				country,
				region,
				province,
				district,
				privacyPolicy,
			},
		},
	} = useGenericFormFieldElements({
		formModel: myForms["firstForm"],
	});

	return (
		<>
			<InputField {...username} />
			<PasswordField {...password} />
			<InputField {...email} />
			<VerifyInputField {...verificationCode} />
			<InputField {...lastnames} />
			<InputField {...names} />
			<RadioButtonsGroupField
				{...genre}
				options={async () => {
					await sleep(1000);
					return [
						{
							value: "1",
							name: "Female",
						},
						{
							value: "2",
							name: "Male",
						},
						{
							value: "3",
							name: "Don't wanna say",
						},
					];
				}}
				row
				sx={{ justifyContent: "space-between" }}
			/>
			<MobilePhoneField {...phone} />
			<DateField {...birthday} />
			<SelectField {...interests} />
			<MultipleSelectField {...favSongs} />
			<MultilineInputField {...about} />
			<CountryRegionSelectorFields
				country={country}
				region={region}
				province={province}
				district={district}
				// nativeSelect
			/>
			<CheckBoxField {...privacyPolicy} />
		</>
	);
};

export default InitialForm;
