import { useState } from "react";
import { toastOptions } from "@/const";
import { Box, Button } from "@mui/material";
import { toast } from "react-toastify";

import useControlledForm from "@/hooks/form/useControlledForm";

import { myForms } from "../formsModel";
import InitialForm from "./Forms/InitialForm";

const FirstForm = () => {
	const [activeStep] = useState(0);
	const { values, handleSubmit } = useControlledForm({
		formModel: myForms["firstForm"],
		activeStep,
		lang: "en",
	});

	function renderStepContent(step: number) {
		switch (step) {
			case 0:
				return <InitialForm />;
			default:
				return <div>Not Found</div>;
		}
	}

	return (
		<Box
			component="form"
			sx={{
				"& > :not(style)": { m: 2 },
				"& .MuiTypography-root": {
					width: 1,
				},
				"& .MuiSubtitle": {
					mb: 0,
				},
				"& .MuiDescription": {},
				"& .ResendCodeOption, & .ChangeEmailOption": { width: 1, my: 0 },
				"& .ChangeEmailOption": { mb: 2 },
				width: 1,
			}}
			margin="auto"
			display="flex"
			flexWrap="wrap"
			justifyContent="center"
			alignItems="center"
			noValidate
			autoComplete="off"
			onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
				e.preventDefault();
				handleSubmit(() => {
					console.log({ values });
					toast("Values printed in the console.", {
						...toastOptions,
						toastId: "first-fom",
						type: "success",
					});
				}, "first-form");
			}}
		>
			{renderStepContent(activeStep)}
			<Button type="submit" variant="contained">
				Send
			</Button>
		</Box>
	);
};

export default FirstForm;
