import { myForms } from "@/Example/formsModel";

import useGenericFormFieldElements from "@/components/GenericFormFields";

const Step3Form = () => {
	const {
		components: { InputField },
		steps: {
			fields: { lastnames },
		},
	} = useGenericFormFieldElements({
		formModel: myForms["multistepForm"],
		autoFocusOnRender: true,
	});
	console.log({ lastnames });
	return (
		<>
			<InputField {...lastnames} />
		</>
	);
};

export default Step3Form;
