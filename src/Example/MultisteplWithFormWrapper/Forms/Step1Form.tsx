import { myForms } from "@/Example/formsModel";

import useGenericFormFieldElements from "@/components/GenericFormFields";
import useFormField from "@/hooks/form/hooks/useFormField";

const Step1Form = () => {
	const {
		components: { InputField },
		steps: {
			fields: { names },
		},
	} = useGenericFormFieldElements({
		formModel: myForms["multistepForm"],
		autoFocusOnRender: true,
		persistedValues: {
			names: "Jose 2",
			lastnames: "Avila",
		},
	});

	console.log({ names });
	const { field } = useFormField({ formModel: myForms["multistepForm"], fieldName: "names" });
	console.log({ field });

	return (
		<>
			<InputField {...names} persistedValue="Jose" />
		</>
	);
};

export default Step1Form;
