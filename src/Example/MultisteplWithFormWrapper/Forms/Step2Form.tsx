import { myForms } from "@/Example/formsModel";
import { Typography } from "@mui/material";

import useGenericFormFieldElements from "@/components/GenericFormFields";

const Step2Form = () => {
	const {
		steps: { title },
	} = useGenericFormFieldElements({
		formModel: myForms["multistepForm"],
		autoFocusOnRender: true,
	});
	return (
		<>
			<Typography>{title}</Typography>
		</>
	);
};

export default Step2Form;
