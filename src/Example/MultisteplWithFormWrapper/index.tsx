import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import { useShallow } from "zustand/react/shallow";

import ControlledFormWrapper from "@/components/ControlledFormWrapper";

import { myForms } from "../formsModel";
import Step1Form from "./Forms/Step1Form";
import Step2Form from "./Forms/Step2Form";
import Step3Form from "./Forms/Step3Form";

const MultisteplWithFormWrapper = () => {
	const { formsState } = useBoundStore<string, string, FormsSlice<string, string>>(
		useShallow((s) => s),
	);
	console.log({ formsState });
	return (
		<ControlledFormWrapper
			formModel={myForms["multistepForm"]}
			lang="en"
			onSuccessSubmit={({ values, stepInfo: { activeStep, setActiveStep } }) => {
				if (activeStep === 0) {
					setActiveStep(1);
				} else if (activeStep === 1) {
					console.log("ENTRA");
					setActiveStep(2);
				} else {
					console.log({ values });
					values.names.data;
				}
			}}
			onBack={({ activeStep, setActiveStep }) => {
				setActiveStep(activeStep - 1);
			}}
		>
			{[Step1Form, Step2Form, Step3Form]}
		</ControlledFormWrapper>
	);
};

export default MultisteplWithFormWrapper;
