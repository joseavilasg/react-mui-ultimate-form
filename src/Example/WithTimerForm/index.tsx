import { useEffect, useRef } from "react";
import { Box, Stack, Typography } from "@mui/material";
import dayjs from "dayjs";

import ControlledFormWrapper from "@/components/ControlledFormWrapper";
import useGenericFormFieldElements from "@/components/GenericFormFields";
import Timer from "@/components/Timer";
import useFormStep from "@/hooks/form/hooks/useFormStep";

import { myForms } from "../formsModel";

const WithTimerForm = () => {
	const {
		steps: { fields, title },
		components: { InputField },
	} = useGenericFormFieldElements({ formModel: myForms["withTimerForm"] });

	const { minTimeMs, activeStep, stepsCompleted, stepDispatch } = useFormStep({
		formModel: myForms["withTimerForm"],
	});

	console.log({ stepsCompleted, activeStep });
	useEffect(() => {
		if (stepsCompleted[activeStep]) {
			stepDispatch(({ stepsLength }) => {
				if (activeStep < stepsLength - 1) {
					return {
						activeStep: activeStep + 1,
					};
				}
				// 	// Last step. Here we send info, etc.
			});
		}
	}, [stepsCompleted, activeStep]);

	const targetDateTime = useRef(dayjs().add(minTimeMs, "millisecond"));

	useEffect(() => {
		targetDateTime.current = dayjs().add(minTimeMs, "millisecond");
	}, [minTimeMs]);

	return (
		<>
			<ControlledFormWrapper
				formModel={myForms["withTimerForm"]}
				onSuccessSubmit={({ values, stepInfo: { activeStep, setActiveStep, stepsLength } }) => {
					if (activeStep === stepsLength - 1) {
						console.log({ values });
					} else {
						setActiveStep(activeStep + 1);
					}
				}}
				// customBackButton={() => <></>}
			>
				{title && <Typography>{title}</Typography>}
				<Stack spacing={2}>
					{Object.entries(fields).map(([key, field]) => {
						if (field.type === "NormalInputValue") {
							return <InputField key={key} {...field} />;
						}
					})}
				</Stack>
			</ControlledFormWrapper>
			{minTimeMs && !stepsCompleted[activeStep] ? (
				<Box
					sx={{ width: 500, margin: "auto", display: "flex", justifyContent: "space-between" }}
				>
					<Timer
						targetDateTime={targetDateTime.current}
						onFinishedTime={() => {
							window.alert("Time is over.");
							stepDispatch(({ stepsCompleted, stepsLength, activeStep }) => {
								if (activeStep < stepsLength - 1) {
									return {
										stepsCompleted: {
											...stepsCompleted,
											[activeStep]: true,
										},
										activeStep: activeStep + 1,
									};
								}

								return {
									stepsCompleted: {
										...stepsCompleted,
										[activeStep]: true,
									},
								};
							});
						}}
						onNSecondsLeft={[
							{
								secondsLeft: 5,
								action: () => {
									alert("You have 5 seconds left.");
								},
							},
							{
								secondsLeft: 10,
								action: () => {
									alert("You have 10 seconds left.");
								},
							},
						]}
						hideDays
						hideHours
					>
						{({ timerData }) => {
							return timerData.map((item, i) => {
								return (
									<Box key={i} sx={{ textAlign: "center" }}>
										<Typography>{item.number}</Typography>
										<Typography>{item.text}</Typography>
									</Box>
								);
							});
						}}
					</Timer>
				</Box>
			) : null}
		</>
	);
};

export default WithTimerForm;
