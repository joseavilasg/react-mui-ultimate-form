import { useRef } from "react";
import { Stack } from "@mui/material";

import ControlledFormWrapper from "@/components/ControlledFormWrapper";
import useGenericFormFieldElements from "@/components/GenericFormFields";

import { myForms } from "../formsModel";

const RadioButtonsForm = () => {
	const {
		components: { RadioButtonsGroupField },
		steps: { fields },
	} = useGenericFormFieldElements({ formModel: myForms["radioButtonsForm"] });
	console.log("Re render");
	const fieldComponents = useRef(
		Object.entries(fields).map(([key, field]) => {
			if (field.type === "RadioGroupInputValue") {
				console.log("RENDER ARRAY");
				return <RadioButtonsGroupField key={key} {...field} labelType="Inside" />;
			}
		}),
	);
	return (
		<ControlledFormWrapper
			formModel={myForms["radioButtonsForm"]}
			onSuccessSubmit={({ values }) => console.log(values)}
		>
			<Stack spacing={2}>{fieldComponents.current}</Stack>
		</ControlledFormWrapper>
	);
};

export default RadioButtonsForm;
