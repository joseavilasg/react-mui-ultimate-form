import ControlledFormWrapper from "@/components/ControlledFormWrapper";

import { myForms } from "../formsModel";
import MainForm from "./MainForm";

const WithPersistedValuesForm = () => {
	return (
		<ControlledFormWrapper
			formModel={myForms["withPersistedValuesForm"]}
			onSuccessSubmit={({ values }) => {
				console.log({ values });
			}}
			lang="es"
		>
			<MainForm />
		</ControlledFormWrapper>
	);
};

export default WithPersistedValuesForm;
