import { Stack } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";

import useGenericFormFieldElements from "@/components/GenericFormFields";
import sleep from "@/hooks/form/utils/sleep";

import { myForms } from "../formsModel";

const MainForm = () => {
	const {
		components: {
			RadioButtonsGroupField,
			CheckboxGroupField,
			InputField,
			MultipleSelectField,
			SelectField,
		},
		steps: {
			fields: { genre, favFood, lastnames, names, favMusic, interests, slug },
		},
	} = useGenericFormFieldElements({
		formModel: myForms["withPersistedValuesForm"],
		autoFocusOnRender: true,
		persistedValues: {
			genre: "3",
			names: "José",
			favMusic: [1],
		},
	});
	return (
		<Grid container spacing={2} flexGrow={1}>
			<Grid xs={12} md={6}>
				<Stack direction="column" spacing={2}>
					<RadioButtonsGroupField {...genre} row labelType="Outlined" />
					<CheckboxGroupField {...favFood} row labelType="Outlined" />
					<InputField {...names} />
				</Stack>
			</Grid>
			<Grid xs={12} md={6}>
				<Stack direction="column" spacing={2}>
					<InputField {...lastnames} />
					<MultipleSelectField
						{...favMusic}
						options={async () => {
							await sleep(3000);
							return [
								{
									value: 1,
									name: "weapon, Against the Current",
								},
								{
									value: 2,
									name: "sundress, State Champs",
								},
							];
						}}
						renderedValueSeparator=" | "
					/>
					<SelectField {...interests} options={[{ value: "Music" }, { value: "Movies" }]} />
					<InputField {...slug} />
				</Stack>
			</Grid>
		</Grid>
	);
};

export default MainForm;
