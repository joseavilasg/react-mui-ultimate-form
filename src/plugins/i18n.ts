import { locales } from "@/lang";
import i18nConfig from "i18next";
import { initReactI18next } from "react-i18next";

i18nConfig.use(initReactI18next).init({
	resources: locales,
	lng: "en", // Set the default language
	fallbackLng: "en",
	interpolation: {
		escapeValue: false,
	},
});

export default i18nConfig;
