import { TFunction } from "i18next";

import i18nConfig from "./i18n";

type Translations = {
	formValidator: {
		notArrayEmptyText: string;
		notEmptyText: string;
		notEmailText: string;
		notPhoneNumberText: string;
		notNumberText: string;
		notLessThanText: (minDate: string) => string;
		notGreaterThanTodayText: string;
		notCheckedText: string;
		notVerificationCodeCompleteText: string;
	};
};
const translations: Translations = {} as Translations;

const getTranslations = (t?: TFunction<"translation", undefined>) => {
	return (() => {
		if (t) {
			return {
				formValidator: {
					notArrayEmptyText: t("formValidator:notArrayEmptyText"),
					notEmptyText: t("formValidator:notEmptyText"),
					notEmailText: t("formValidator:notEmailText"),
					notPhoneNumberText: t("formValidator:notPhoneNumberText"),
					notNumberText: t("formValidator:notNumberText"),
					notSlugText: t("formValidator:notSlugText"),
					notLessThanText: (minDate: string) =>
						t("formValidator:notLessThanText", { minDate }),
					notGreaterThanTodayText: t("formValidator:notGreaterThanTodayText"),
					notCheckedText: t("formValidator:notCheckedText"),
					notVerificationCodeCompleteText: t("formValidator:notVerificationCodeCompleteText"),
				},
				mobilePhoneMUIProps: {
					searchPlaceholder: t("mobilePhoneMUIProps:searchPlaceholder"),
					searchNotFound: t("mobilePhoneMUIProps:searchNotFound"),
				},
				selectFieldMUIProps: {
					noneOptionText: t("selectFieldMUIProps:noneOptionText"),
					loadingOptionsText: t("selectFieldMUIProps:loadingOptionsText"),
				},
				autocompleteMUIProps: {
					noOptionsText: t("autocompleteMUIProps:noOptionsText"),
				},
				radioButtonsGroupMUI: {
					noOptionsAvailableText: t("radioButtonsGroupMUI:noOptionsAvailableText"),
				},
				formWrapper: {
					buttonSendText: t("formWrapper:buttonSendText"),
					buttonNextText: t("formWrapper:buttonNextText"),
					buttonBackText: t("formWrapper:buttonBackText"),
				},
				toastOptions: {
					warnMessage: t("toastOptions:warnMessage"),
				},
			};
		}
		return {
			formValidator: {
				notArrayEmptyText: i18nConfig.t("formValidator:notArrayEmptyText"),
				notEmptyText: i18nConfig.t("formValidator:notEmptyText"),
				notEmailText: i18nConfig.t("formValidator:notEmailText"),
				notPhoneNumberText: i18nConfig.t("formValidator:notPhoneNumberText"),
				notNumberText: i18nConfig.t("formValidator:notNumberText"),
				notSlugText: i18nConfig.t("formValidator:notSlugText"),
				notLessThanText: (minDate: string) =>
					i18nConfig.t("formValidator:notLessThanText", { minDate }),
				notGreaterThanTodayText: i18nConfig.t("formValidator:notGreaterThanTodayText"),
				notCheckedText: i18nConfig.t("formValidator:notCheckedText"),
				notVerificationCodeCompleteText: i18nConfig.t(
					"formValidator:notVerificationCodeCompleteText",
				),
			},
			mobilePhoneMUIProps: {
				searchPlaceholder: i18nConfig.t("mobilePhoneMUIProps:searchPlaceholder"),
				searchNotFound: i18nConfig.t("mobilePhoneMUIProps:searchNotFound"),
			},
			selectFieldMUIProps: {
				noneOptionText: i18nConfig.t("selectFieldMUIProps:noneOptionText"),
			},
			autocompleteMUIProps: {
				noOptionsText: i18nConfig.t("autocompleteMUIProps:noOptionsText"),
			},
			radioButtonsGroupMUI: {
				noOptionsAvailableText: i18nConfig.t("radioButtonsGroupMUI:noOptionsAvailableText"),
			},
			formWrapper: {
				buttonSendText: i18nConfig.t("formWrapper:buttonSendText"),
				buttonNextText: i18nConfig.t("formWrapper:buttonNextText"),
				buttonBackText: i18nConfig.t("formWrapper:buttonBackText"),
			},
			toastOptions: {
				warnMessage: i18nConfig.t("toastOptions:warnMessage"),
			},
		};
	})();
};

// i18next seems ready -> initial fill translations
if (i18nConfig.isInitialized) {
	getTranslations();
}

export { translations, getTranslations };
