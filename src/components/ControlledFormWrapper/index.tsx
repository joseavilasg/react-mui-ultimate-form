import { getTranslations } from "@/plugins/translations";
import ArrowBackOutlinedIcon from "@mui/icons-material/ArrowBackOutlined";
import ArrowForwardOutlinedIcon from "@mui/icons-material/ArrowForwardOutlined";
import SendIcon from "@mui/icons-material/Send";
import { Box, Button, ButtonProps, Stack, StackProps, SxProps, Theme } from "@mui/material";
import { getI18n } from "react-i18next";

import { CustomHandleSubmit } from "@/types/events";
import { TFormDef } from "@/types/formModel";
import { TLanguage } from "@/types/language";

import useFormStep from "@/hooks/form/hooks/useFormStep";
import useFormValues from "@/hooks/form/hooks/useFormValues";
import useControlledForm from "@/hooks/form/useControlledForm";

type FormWrapperProps<F extends string, T extends string> = {
	children: React.FunctionComponent[] | React.ReactNode;
	/**
	 * Useful when using useControlledForm outside of this component.
	 */
	disableInternalMainHook?: true;
	onSuccessSubmit: CustomHandleSubmit<T>;
	onBack?: (params: {
		activeStep: number;
		setActiveStep: React.Dispatch<React.SetStateAction<number>>;
	}) => void;
	formModel: TFormDef<F, T>;
	lang?: TLanguage;
	backButtonText?: string;
	nextButtonText?: string;
	sendButtonText?: string;
	backButtonIcon?: React.ReactNode;
	nextButtonIcon?: React.ReactNode;
	sendButtonIcon?: React.ReactNode;
	slotProps?: {
		formBoxSx?: SxProps<Theme>;
		stackProps?: StackProps;
		backButtonProps?: ButtonProps;
		nextSendButtonProps?: ButtonProps;
	};
	customBackButton?: (params: {
		activeStep: number;
		setActiveStep: React.Dispatch<React.SetStateAction<number>>;
	}) => React.ReactNode;
	customNextSendButton?: (params: { activeStep: number; stepsLength: number }) => React.ReactNode;
};

const ControlledFormWrapper = <F extends string, T extends string>({
	children,
	disableInternalMainHook,
	onSuccessSubmit,
	formModel,
	lang,
	onBack,
	backButtonText,
	nextButtonText,
	sendButtonText,
	backButtonIcon,
	nextButtonIcon,
	sendButtonIcon,
	slotProps,
	customBackButton,
	customNextSendButton,
}: FormWrapperProps<F, T>) => {
	const { activeStep, setActiveStep } = useFormStep({ formModel });

	const { handleSubmit, stepsLength } = (() => {
		if (disableInternalMainHook) {
			const { handleSubmit } = useFormValues({ formModel });
			const stepsLength = (() => {
				const { steps } = formModel;
				return steps.length;
			})();
			return { handleSubmit, stepsLength };
		}
		const {
			handleSubmit,
			stepInfo: { stepsLength },
		} = useControlledForm({
			formModel,
			activeStep,
			lang,
		});
		return { handleSubmit, stepsLength };
	})();

	const translations = getTranslations(getI18n().t);

	return (
		<Box
			component="form"
			sx={{
				width: 1,
				// margin: "auto",
				// display: "flex",
				// flexWrap: "wrap",
				// justifyContent: "center",
				// alignItems: "center",
				...slotProps?.formBoxSx,
			}}
			noValidate
			autoComplete="off"
			onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
				e.preventDefault();
				handleSubmit(onSuccessSubmit, formModel.formId, {
					activeStep,
					setActiveStep,
				});
			}}
		>
			{(() => {
				if (Array.isArray(children)) {
					return children.map((Child, index) => {
						if (typeof Child === "function") {
							if (index === activeStep) {
								return <Child key={index} />;
							} else {
								return null;
							}
						} else {
							return Child;
						}
					});
				} else {
					return children;
				}
			})()}
			<Stack
				direction="row"
				spacing={2}
				sx={{
					width: 1,
					mt: 2,
					justifyContent: "flex-end",
				}}
				{...slotProps?.stackProps}
			>
				{activeStep !== 0 &&
					(customBackButton ? (
						customBackButton({ activeStep, setActiveStep })
					) : (
						<Button
							variant="outlined"
							color="primary"
							{...slotProps?.backButtonProps}
							startIcon={
								backButtonIcon !== undefined ? backButtonIcon : <ArrowBackOutlinedIcon />
							}
							onClick={() => {
								if (onBack) {
									onBack({ activeStep, setActiveStep });
								} else {
									setActiveStep((step) => step - 1);
								}
							}}
						>
							{backButtonText ?? translations.formWrapper.buttonBackText}
						</Button>
					))}
				{customNextSendButton ? (
					customNextSendButton({ activeStep, stepsLength })
				) : (
					<Button
						type="submit"
						variant="contained"
						endIcon={
							activeStep === stepsLength - 1 ? (
								sendButtonIcon !== undefined ? (
									sendButtonIcon
								) : (
									<SendIcon />
								)
							) : nextButtonIcon !== undefined ? (
								nextButtonIcon
							) : (
								<ArrowForwardOutlinedIcon />
							)
						}
						{...slotProps?.nextSendButtonProps}
					>
						{activeStep === stepsLength - 1
							? sendButtonText ?? translations.formWrapper.buttonSendText
							: nextButtonText ?? translations.formWrapper.buttonNextText}
					</Button>
				)}
			</Stack>
		</Box>
	);
};

export default ControlledFormWrapper;
