import React, { FC, useEffect } from "react";
import { Dayjs } from "dayjs";

import useTimer from "@/hooks/form/hooks/useTimer";

type TimerProps = {
	children: (params: {
		timerData: { number: string; text: string }[];
		total: number;
	}) => React.ReactNode;
	onFinishedTime?: () => void;
	onNSecondsLeft?: { secondsLeft: number; action: () => void }[];
	targetDateTime: Dayjs;
	upperCase?: boolean;
	textShortened?: boolean;
	hideDays?: true;
	hideHours?: true;
	/** Two digits is default format */
	formatValue?: (value: number) => string;
};

const Timer: FC<TimerProps> = ({
	children,
	targetDateTime,
	onFinishedTime,
	onNSecondsLeft,
	formatValue,
	textShortened = false,
	upperCase = false,
	hideDays,
	hideHours,
}) => {
	const { days, hours, minutes, seconds, total } = useTimer({
		targetDateTime,
		onNSecondsLeft,
	});

	useEffect(() => {
		if (total === 0) {
			onFinishedTime?.();
		}
	}, [total]);

	const formatTwoDigits = (num: number) => num.toString().padStart(2, "0");

	let dayText = days === 1 ? "Día" : "Días";
	let hourText = hours === 1 ? "Hora" : "Horas";
	let minuteText = "Min.";
	let secondText = "Seg.";

	if (!textShortened) {
		minuteText = minutes === 1 ? "Minuto" : "Minutos";
		secondText = seconds === 1 ? "Segundo" : "Segundos";
	}

	if (upperCase) {
		dayText = dayText.toUpperCase();
		hourText = hourText.toUpperCase();
		minuteText = minuteText.toUpperCase();
		secondText = secondText.toUpperCase();
	}

	const timerData: { number: string; text: string }[] = [];
	if (!hideDays) {
		timerData.push({
			number: formatValue ? formatValue(days) : formatTwoDigits(days),
			text: dayText,
		});
	}

	if (!hideHours) {
		timerData.push({
			number: formatValue ? formatValue(hours) : formatTwoDigits(hours),
			text: hourText,
		});
	}

	timerData.push(
		{
			number: formatValue ? formatValue(minutes) : formatTwoDigits(minutes),
			text: minuteText,
		},
		{
			number: formatValue ? formatValue(seconds) : formatTwoDigits(seconds),
			text: secondText,
		},
	);

	return <>{children({ timerData: timerData, total })}</>;
};

export default Timer;
