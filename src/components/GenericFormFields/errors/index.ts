export const newNoNamePropError = (field: string) =>
	new Error(`There is no 'name' prop in ${field}`);
