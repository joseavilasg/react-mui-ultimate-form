import { useEffect } from "react";
import { KEYS } from "@/const";

import { FormNormalSelectMUIChangeEvent } from "@/types/events";
import { TGenericSelectFieldProps } from "@/types/genericFormFieldHook";
import { NormalSelectInputValue } from "@/types/inputValues";
import { SelectTRef } from "@/types/refs";

import useFocus from "@/hooks/form/hooks/useFocus";

import SelectFieldMUI from "./CustomFields/SelectFieldMUI";

const SelectField = <T extends string>({
	name,
	label,
	field,
	shouldFocus,
	options = [],
	disableLazyLoadOptions = false,
}: TGenericSelectFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	const customLabel = `${label}${required ? " *" : ""}`;

	const handleKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === KEYS.ENTER) {
			e.preventDefault();
		}
	};

	const handleSelectDisplayKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === KEYS.ENTER) {
			e.preventDefault();
			const target = e.target as HTMLElement;

			let form: HTMLFormElement | null = null;
			let currentNode: HTMLElement | null = target;
			while (currentNode.parentNode) {
				if (currentNode.tagName === "FORM") {
					form = currentNode as HTMLFormElement;
					break;
				}
				currentNode = currentNode.parentNode as HTMLElement;
			}

			if (form) {
				form.requestSubmit();
			}
		}
		if (e.key === " " || e.key === KEYS.ARROWDOWN || e.key === KEYS.ARROWUP) {
			e.preventDefault();
			(ref as SelectTRef).current?.openSelect();
		}
	};

	return (
		<SelectFieldMUI
			selectRef={ref as SelectTRef}
			error={error as boolean}
			errorText={errorText}
			name={name}
			required={required as boolean}
			label={customLabel}
			value={value.data as NormalSelectInputValue}
			onChange={handleChange as any as FormNormalSelectMUIChangeEvent}
			onBlur={handleBlur as any}
			options={options}
			disableLazyLoadOptions={disableLazyLoadOptions}
			MenuProps={{
				onKeyDown: handleKeyDown,
			}}
			SelectDisplayProps={{
				onKeyDown: handleSelectDisplayKeyDown,
			}}
		/>
	);
};

export default SelectField;
