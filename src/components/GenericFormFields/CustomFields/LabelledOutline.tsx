import React from "react";
import { FormControl, FormHelperText, InputLabel, useFormControl } from "@mui/material";
import NotchedOutline from "@mui/material/OutlinedInput/NotchedOutline";
import useTheme from "@mui/material/styles/useTheme";

type LabelledOutlineProps = {
	id?: string;
	label: React.ReactNode;
	children: React.ReactNode;
	className?: string;
	fullWidth?: boolean;
	error?: boolean;
	errorText?: string;
	required?: boolean;
	labelInside?: true;
};

const LabelledOutlineContent = ({
	id,
	label,
	children,
	className,
	fullWidth,
	error,
	errorText,
	required,
	labelInside,
}: LabelledOutlineProps) => {
	const theme = useTheme();
	const value = useFormControl();

	const customLabel = required ? `${label} *` : label;

	return (
		<div
			className={className}
			style={{ position: "relative", width: fullWidth ? "100%" : "auto" }}
		>
			{!labelInside && (
				<InputLabel htmlFor={id} variant="outlined" shrink>
					{customLabel}
				</InputLabel>
			)}
			<div style={{ position: "relative" }}>
				{/* padding: 18.5px 14px */}
				<div id={id} style={{ padding: "14px 14px", borderRadius: "4px" }}>
					{labelInside && (
						<InputLabel
							htmlFor={id}
							variant="outlined"
							sx={{
								"&.MuiFormLabel-root": {
									transform: "translate(0,0)",
									position: "relative",
									mb: 1,
									whiteSpace: "normal",
								},
							}}
							shrink={false}
						>
							{customLabel}
						</InputLabel>
					)}
					{children}
					<NotchedOutline
						notched
						{...(!labelInside && { label: customLabel })}
						error={error}
						style={{
							borderColor: error
								? theme.palette.error.main
								: value?.focused
								? theme.palette.primary.main
								: "",
							borderWidth: value?.focused ? 2 : 1,
						}}
					/>
				</div>
			</div>
			<FormHelperText>{errorText}</FormHelperText>
		</div>
	);
};

const LabelledOutline = (props: LabelledOutlineProps) => {
	return (
		<FormControl error={props.error} fullWidth={props.fullWidth}>
			<LabelledOutlineContent {...props} />
		</FormControl>
	);
};
export default LabelledOutline;
