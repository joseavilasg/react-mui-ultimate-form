import { useEffect, useImperativeHandle, useRef, useState } from "react";
import { getTranslations } from "@/plugins/translations";
import { type SelectProps } from "@mui/material";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import OutlinedInput from "@mui/material/OutlinedInput";
import Select from "@mui/material/Select";
import useTheme from "@mui/material/styles/useTheme";
import { getI18n } from "react-i18next";
import { useIsFirstRender } from "usehooks-ts";

import { TSelectOptions } from "@/types/options";
import { SelectTRef } from "@/types/refs";

interface SelectFieldMUIProps<T> extends SelectProps<T> {
	selectRef: SelectTRef;
	error: boolean;
	errorText: string;
	options: TSelectOptions | (() => Promise<TSelectOptions | void>);
	disableLazyLoadOptions: boolean;
	value: T;
}

const SelectFieldMUI = <T,>({
	selectRef,
	label,
	error,
	errorText,
	options,
	disableLazyLoadOptions,
	value,
	...props
}: SelectFieldMUIProps<T>) => {
	const theme = useTheme();

	// const { t } = useTranslation();
	const translations = getTranslations(getI18n().t).selectFieldMUIProps;

	const [open, setOpen] = useState(false);
	const inputRef = useRef<HTMLInputElement>(null);

	const [newOptions, setNewOptions] = useState<TSelectOptions>(
		Array.isArray(options) ? options : [],
	);
	const loading =
		(open || disableLazyLoadOptions) && typeof options === "function" && newOptions.length === 0;
	const isFirstRender = useIsFirstRender();

	useEffect(() => {
		if (!loading) {
			return undefined;
		}
		if (typeof options === "function") {
			(async () => {
				const data = await options();
				if (data) {
					setNewOptions(data);
				}
			})();
		} else {
			setNewOptions(options);
		}
	}, [loading]);

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useImperativeHandle(selectRef, () => ({
		...(inputRef.current as HTMLInputElement),
		openSelect: handleOpen,
		closeSelect: handleClose,
	}));

	return (
		<FormControl
			error={error}
			sx={{
				width: 1,
			}}
			variant="outlined"
		>
			<InputLabel>{label}</InputLabel>
			<Select
				{...props}
				value={isFirstRender || newOptions.length === 0 ? "" : value} // Same as areOptionsLoaded in MultipleSelectFieldMUI
				open={open}
				onClose={handleClose}
				onOpen={handleOpen}
				inputRef={inputRef}
				label={label}
				input={
					<OutlinedInput
						label={label}
						endAdornment={
							loading ? (
								<Box sx={{ pr: 3 }}>
									<CircularProgress color="inherit" size={20} />
								</Box>
							) : null
						}
					/>
				}
			>
				{loading ? (
					<MenuItem disabled>{translations.loadingOptionsText}</MenuItem>
				) : (
					<MenuItem sx={{ color: theme.palette.text.disabled }}>
						{translations.noneOptionText}
					</MenuItem>
				)}
				{newOptions.map((option, i) => {
					return (
						<MenuItem key={i} value={option.value}>
							{option.name ?? option.value}
						</MenuItem>
					);
				})}
			</Select>
			<FormHelperText>{errorText}</FormHelperText>
		</FormControl>
	);
};

export default SelectFieldMUI;
