import { useEffect, useImperativeHandle, useRef, useState } from "react";
import { getTranslations } from "@/plugins/translations";
import {
	Checkbox,
	CircularProgress,
	FormControl,
	FormControlLabel,
	FormGroup,
	FormHelperText,
	FormLabel,
	SxProps,
	Theme,
	Typography,
} from "@mui/material";
import { getI18n } from "react-i18next";

import { CheckboxGroupChangeEvent } from "@/types/events";
import { CheckboxGroupInputValue } from "@/types/inputValues";
import { TCheckboxGroupOptions, TCheckboxGroupOptionsFunct } from "@/types/options";
import { CheckboxGroupTRef } from "@/types/refs";

import LabelledOutline from "./LabelledOutline";

export type LabelType = "Outlined" | "Outside" | "Inside" | "None";

type CheckboxGroupMUIProps<T extends CheckboxGroupInputValue> = {
	checkboxGroupRef: CheckboxGroupTRef;
	label?: string;
	required?: boolean;
	initialValue?: T;
	value: T;
	name: string;
	error: boolean;
	errorText: string;
	options: TCheckboxGroupOptions | TCheckboxGroupOptionsFunct;
	row?: boolean;
	standalone?: boolean;
	labelType?: LabelType;
	onBlur?: React.FocusEventHandler<HTMLDivElement>;
	sx?: SxProps<Theme>;
	onChange: (e: CheckboxGroupChangeEvent) => void;
};

const CheckboxGroupMUI = <T extends CheckboxGroupInputValue>({
	checkboxGroupRef,
	label,
	required,
	error,
	errorText,
	options,
	name,
	value,
	row,
	standalone = false,
	labelType = "Outlined",
	onBlur,
	initialValue,
	onChange,
	...props
}: CheckboxGroupMUIProps<T>) => {
	const divRef = useRef<HTMLDivElement>(null);
	const firstRadioRef = useRef<HTMLInputElement>(null);

	const [selectedCheckboxesValues, setSelectedCheckboxesValues] =
		useState<CheckboxGroupInputValue>(initialValue ?? ([] as CheckboxGroupInputValue));

	const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
		const value = event.target.value;
		if (checked) {
			setSelectedCheckboxesValues((prevState) => {
				if (prevState.find((e) => e === value)) {
					return prevState;
				}
				return [...prevState, value];
			});
		} else {
			setSelectedCheckboxesValues((prevState) => {
				return [...prevState.filter((e) => e !== value)];
			});
		}
	};

	useEffect(() => {
		const e: CheckboxGroupChangeEvent = {
			target: {
				value: selectedCheckboxesValues,
				name,
			},
		};
		onChange(e);
	}, [selectedCheckboxesValues]);

	const handleFocus = () => {
		firstRadioRef.current?.focus();
	};

	useImperativeHandle(checkboxGroupRef, () => ({
		...(divRef.current as HTMLDivElement),
		value,
		focus: handleFocus,
	}));

	const translations = getTranslations(getI18n().t);

	const [newOptions, setNewOptions] = useState<TCheckboxGroupOptions>(
		Array.isArray(options) ? options : [],
	);

	const loading = newOptions.length === 0 && typeof options === "function";

	useEffect(() => {
		if (!loading) {
			return undefined;
		}
		if (typeof options === "function") {
			(async () => {
				const data = await options();
				if (data) {
					setNewOptions(data);
				}
			})();
		} else {
			setNewOptions(options);
		}
	}, [loading]);

	const CheckboxGroupContent = (
		<FormGroup
			{...props}
			onBlur={(e) => {
				const event = { ...e, target: { ...e.target, name, value } };
				onBlur?.(event);
			}}
			row={row}
			ref={divRef}
		>
			{loading && <CircularProgress />}
			{newOptions.length === 0 && !loading && (
				<Typography>{translations.radioButtonsGroupMUI.noOptionsAvailableText}</Typography>
			)}
			{newOptions.map((option, idx) => {
				if (idx === 0) {
					return (
						<FormControlLabel
							key={option.value}
							value={option.value}
							control={
								<Checkbox
									checked={selectedCheckboxesValues.some((e) => e === option.value)}
									inputRef={firstRadioRef}
									onChange={handleCheckboxChange}
								/>
							}
							label={standalone ? null : newOptions[0].name ?? newOptions[0].value}
						/>
					);
				}
				return (
					<FormControlLabel
						key={option.value}
						value={option.value}
						control={
							<Checkbox
								checked={selectedCheckboxesValues.some((e) => e === option.value)}
								onChange={handleCheckboxChange}
							/>
						}
						label={standalone ? null : option.name ?? option.value}
					/>
				);
			})}
		</FormGroup>
	);

	if (labelType === "Outlined") {
		return (
			<LabelledOutline
				label={label}
				error={error}
				errorText={errorText}
				required={required}
				fullWidth
			>
				{CheckboxGroupContent}
			</LabelledOutline>
		);
	}
	if (labelType === "Outside") {
		return (
			<FormControl error={error} required={required} fullWidth>
				<FormLabel required={required}>{label}</FormLabel>
				{CheckboxGroupContent}
				<FormHelperText>{errorText}</FormHelperText>
			</FormControl>
		);
	}
	if (labelType === "None") {
		return (
			<FormControl error={error} required={required} fullWidth>
				{CheckboxGroupContent}
				<FormHelperText>{errorText}</FormHelperText>
			</FormControl>
		);
	}
	return null;
};

export default CheckboxGroupMUI;
