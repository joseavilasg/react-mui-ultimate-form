import { useEffect, useImperativeHandle, useRef, useState } from "react";
import { getTranslations } from "@/plugins/translations";
import {
	CircularProgress,
	FormControl,
	FormControlLabel,
	FormHelperText,
	FormLabel,
	Radio,
	RadioGroup,
	Typography,
	type RadioGroupProps,
} from "@mui/material";
import { getI18n } from "react-i18next";

import { TRadioGroupOptions } from "@/types/options";
import { RadioGroupTRef } from "@/types/refs";

import { LabelType } from "./CheckboxGroupMUI";
import LabelledOutline from "./LabelledOutline";

type RadioButtonsGroupMUIProps<T extends string> = Omit<
	RadioGroupProps,
	"value" | "defaultValue"
> & {
	radioGroupRef: RadioGroupTRef;
	label?: string;
	required?: boolean;
	defaultValue?: T;
	value: T;
	error: boolean;
	errorText: string;
	options: TRadioGroupOptions | (() => Promise<TRadioGroupOptions | void>);
	row?: boolean;
	standalone?: boolean;
	labelType?: LabelType;
	onBlur?: React.FocusEventHandler<HTMLDivElement>;
};

const RadioButtonsGroupMUI = <T extends string>({
	radioGroupRef,
	label,
	required,
	error,
	errorText,
	options,
	name,
	value,
	row,
	standalone = false,
	labelType = "Outlined",
	onBlur,
	...props
}: RadioButtonsGroupMUIProps<T>) => {
	const divRef = useRef<HTMLDivElement>(null);
	const firstRadioRef = useRef<HTMLInputElement>(null);

	const handleFocus = () => {
		firstRadioRef.current?.focus();
	};

	useImperativeHandle(radioGroupRef, () => ({
		...(divRef.current as HTMLDivElement),
		value,
		focus: handleFocus,
	}));

	const translations = getTranslations(getI18n().t);

	const [newOptions, setNewOptions] = useState<TRadioGroupOptions>(
		Array.isArray(options) ? options : [],
	);

	const loading = newOptions.length === 0 && typeof options === "function";

	useEffect(() => {
		if (!loading) {
			return undefined;
		}
		if (typeof options === "function") {
			(async () => {
				const data = await options();
				if (data) {
					setNewOptions(data);
				}
			})();
		} else {
			setNewOptions(options);
		}
	}, [loading]);

	const RadioGroupContent = (
		<RadioGroup
			{...props}
			onBlur={(e) => {
				const event = { ...e, target: { ...e.target, name, value } };
				onBlur?.(event);
			}}
			row={row}
			ref={divRef}
			aria-labelledby="radio-buttons-group-label"
			value={value}
			name={name}
		>
			{loading && <CircularProgress />}
			{newOptions.length === 0 && !loading && (
				<Typography>{translations.radioButtonsGroupMUI.noOptionsAvailableText}</Typography>
			)}
			{newOptions.map((option, idx) => {
				if (idx === 0) {
					return (
						<FormControlLabel
							key={newOptions[0].value}
							value={newOptions[0].value}
							control={<Radio inputRef={firstRadioRef} />}
							label={standalone ? null : newOptions[0].name ?? newOptions[0].value}
						/>
					);
				}
				return (
					<FormControlLabel
						key={option.value}
						value={option.value}
						control={<Radio />}
						label={standalone ? null : option.name ?? option.value}
					/>
				);
			})}
		</RadioGroup>
	);

	if (labelType === "Outlined") {
		return (
			<LabelledOutline
				label={label}
				error={error}
				errorText={errorText}
				required={required}
				fullWidth
			>
				{RadioGroupContent}
			</LabelledOutline>
		);
	}
	if (labelType === "Inside") {
		return (
			<LabelledOutline
				label={label}
				labelInside
				error={error}
				errorText={errorText}
				required={required}
				fullWidth
			>
				{RadioGroupContent}
			</LabelledOutline>
		);
	}
	if (labelType === "Outside") {
		return (
			<FormControl error={error} required={required} fullWidth>
				<FormLabel required={required}>{label}</FormLabel>
				{RadioGroupContent}
				<FormHelperText>{errorText}</FormHelperText>
			</FormControl>
		);
	}
	if (labelType === "None") {
		return (
			<FormControl error={error} required={required} fullWidth>
				{RadioGroupContent}
				<FormHelperText>{errorText}</FormHelperText>
			</FormControl>
		);
	}
	return null;
};

export default RadioButtonsGroupMUI;
