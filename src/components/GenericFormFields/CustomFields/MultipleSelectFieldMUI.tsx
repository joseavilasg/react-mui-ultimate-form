import { useEffect, useImperativeHandle, useRef, useState } from "react";
import { getTranslations } from "@/plugins/translations";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import CircularProgress from "@mui/material/CircularProgress";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";
import InputLabel from "@mui/material/InputLabel";
import ListItemText from "@mui/material/ListItemText";
import MenuItem from "@mui/material/MenuItem";
import OutlinedInput from "@mui/material/OutlinedInput";
import Select, { SelectProps } from "@mui/material/Select";
import { getI18n } from "react-i18next";
import { useIsFirstRender } from "usehooks-ts";

import { MultipleSelectInputValue } from "@/types/inputValues";
import { TSelectOptions } from "@/types/options";
import { SelectTRef } from "@/types/refs";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
			width: 250,
		},
	},
};
interface MultipleSelectFieldMUIProps<T>
	extends Omit<SelectProps<T>, "renderValue" | "input" | "multiple" | "value"> {
	selectRef: SelectTRef;
	error: boolean;
	errorText: string;
	options: TSelectOptions | (() => Promise<TSelectOptions | void>);
	disableLazyLoadOptions: boolean;
	value: T;
	rederedValueSeparator?: string;
	onMenuItemKeyDown?: (event: React.KeyboardEvent<HTMLLIElement>) => void;
}

const MultipleSelectFieldMUI = <T extends MultipleSelectInputValue>({
	selectRef,
	label,
	error,
	errorText,
	options,
	disableLazyLoadOptions,
	value,
	onMenuItemKeyDown,
	rederedValueSeparator,
	...props
}: MultipleSelectFieldMUIProps<T>) => {
	const [open, setOpen] = useState(false);
	const inputRef = useRef<HTMLInputElement>(null);

	// const { t } = useTranslation();
	const translations = getTranslations(getI18n().t);

	const [newOptions, setNewOptions] = useState<TSelectOptions>(
		Array.isArray(options) ? options : [],
	);
	const loading =
		(open || disableLazyLoadOptions) && typeof options === "function" && newOptions.length === 0;

	const [areOptionsLoaded, setAreOptionsLoaded] = useState(false);
	const isFirstRender = useIsFirstRender();

	useEffect(() => {
		if (typeof options !== "function") {
			setAreOptionsLoaded(true);
		}
	}, []);

	useEffect(() => {
		if (!loading) {
			return undefined;
		}
		if (typeof options === "function") {
			(async () => {
				const data = await options();
				if (data) {
					setNewOptions(data);
					setAreOptionsLoaded(true);
				}
			})();
		} else {
			setNewOptions(options);
			setAreOptionsLoaded(true);
		}
	}, [loading]);

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	useImperativeHandle(selectRef, () => ({
		...(inputRef.current as HTMLInputElement),
		openSelect: handleOpen,
		closeSelect: handleClose,
	}));

	return (
		<FormControl
			error={error}
			sx={{
				width: 1,
			}}
			variant="outlined"
		>
			<InputLabel>{label}</InputLabel>
			<Select
				{...props}
				multiple
				value={
					isFirstRender
						? ([] as unknown as T)
						: !areOptionsLoaded
						? ([] as unknown as T)
						: value // Same as newOptions.length === 0 in SelectFieldMUI
				}
				open={open}
				onOpen={handleOpen}
				onClose={handleClose}
				inputRef={inputRef}
				label={label}
				input={
					<OutlinedInput
						label={label}
						endAdornment={
							loading ? (
								<Box sx={{ pr: 3 }}>
									<CircularProgress color="inherit" size={20} />
								</Box>
							) : null
						}
					/>
				}
				renderValue={(selected: T) =>
					selected
						.map((val) => newOptions.find((opt) => opt.value === val))
						.filter((opt) => opt !== undefined)
						.map((opt) => opt?.name ?? opt?.value)
						.join(rederedValueSeparator ? `${rederedValueSeparator} ` : "; ")
				}
				MenuProps={{ ...props.MenuProps, ...MenuProps }}
			>
				{loading && (
					<MenuItem disabled>{translations.selectFieldMUIProps.loadingOptionsText}</MenuItem>
				)}
				{!newOptions.length && !loading && (
					<MenuItem disabled>{translations.autocompleteMUIProps?.noOptionsText}</MenuItem>
				)}
				{newOptions.map((option) => {
					return (
						<MenuItem
							key={option.value}
							value={option.value} // For ts to not complain. But, it seems that this can recieve Array<any>
							onKeyDown={onMenuItemKeyDown}
						>
							<Checkbox
								checked={
									value.findIndex((val) => {
										// if (val.name) {
										// 	return val.name === option?.name;
										// }
										// return val.value === option.value;
										return val === option.value;
									}) > -1
								}
								onKeyDown={(e) => {
									e.preventDefault();
								}}
							/>
							<ListItemText primary={option.name ?? option.value} />
						</MenuItem>
					);
				})}
			</Select>
			<FormHelperText>{errorText}</FormHelperText>
		</FormControl>
	);
};

export default MultipleSelectFieldMUI;
