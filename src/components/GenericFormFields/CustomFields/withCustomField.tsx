import { useEffect, useImperativeHandle, useRef, useState } from "react";

import { CustomChangeEvent, FormBlurEvent, FormChangeEvent } from "@/types/events";
import { FieldTypes } from "@/types/fields";
import { CustomTRef, TRef } from "@/types/refs";

type CustomFieldValue = { data: any; type: `${string}CustomField` };
type CustomFieldProps<T extends string> = {
	label: string;
	name: T;
	value: { data: unknown; type: FieldTypes };
	fieldRef: TRef;
	required: boolean;
	error: boolean | boolean[];
	errorText: string;
	// onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
	onBlur: FormBlurEvent<T>;
	// onChange: FormCustomChangeEvent;
	onChange: FormChangeEvent<T>;
};

type InternalProps = {
	label: string;
	name: string;
	value: CustomFieldValue;
	required: boolean;
	error: boolean;
	errorText: string;
	setValue: React.Dispatch<React.SetStateAction<any>>;
	toFocusRef: React.RefObject<HTMLButtonElement>;
	toContainerRef: React.RefObject<HTMLDivElement>;
	onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
};

const withCustomField = (WrappedFieldComponent: React.ComponentType<InternalProps>) => {
	return <T extends string>({
		name,
		label,
		required,
		error,
		errorText,
		onChange,
		onBlur,
		fieldRef,
		value: _value,
	}: CustomFieldProps<T>) => {
		const [value, _setValue] = useState(_value);
		const setValue = (newVal: any) => {
			_setValue((s) => {
				return { ...s, data: newVal };
			});
		};
		const toContainerRef = useRef<HTMLDivElement>(null);
		const toFocusRef = useRef<HTMLButtonElement>(null);

		useEffect(() => {
			if (name && value.data) {
				const e = {
					target: {
						value: value.data,
						name,
					},
				} as CustomChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement, T>;
				onChange(e);
			}
		}, [value, name]);

		const handleFocus = () => {
			toFocusRef.current?.focus();
		};

		useImperativeHandle(fieldRef as CustomTRef, () => ({
			...(toContainerRef.current as HTMLDivElement),
			value: value.data,
			focus: handleFocus,
		}));

		return (
			<WrappedFieldComponent
				label={label}
				name={name}
				required={required}
				value={value as CustomFieldValue}
				error={(() => {
					if (Array.isArray(error)) {
						if (error.length > 0) return error[0];
						return false;
					}
					return error;
				})()}
				errorText={errorText ?? ""}
				setValue={setValue}
				toFocusRef={toFocusRef}
				toContainerRef={toContainerRef}
				onBlur={
					onBlur as unknown as React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>
				}
			/>
		);
	};
};

export default withCustomField;
