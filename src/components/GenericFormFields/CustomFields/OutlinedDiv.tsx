import React from "react";
import { InputBaseComponentProps, TextField } from "@mui/material";

type InputComponentProps = InputBaseComponentProps;
type OutlinedDivProps = {
	children: React.ReactNode;
	label: React.ReactNode;
	required?: boolean;
	fullWidth?: boolean;
	error?: boolean;
	errorText?: string;
	onBlur?: React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>;
};
const InputComponent = React.forwardRef<unknown, InputComponentProps>((props, ref) => {
	return <div {...(props as any)} ref={ref} />;
});
const OutlinedDiv = ({
	children,
	label,
	required,
	fullWidth,
	error,
	errorText,
	onBlur,
}: OutlinedDivProps) => {
	return (
		<TextField
			fullWidth={fullWidth}
			error={error}
			helperText={errorText}
			variant="outlined"
			label={required ? `${label} *` : label}
			multiline
			onBlur={onBlur}
			InputLabelProps={{ shrink: true }}
			InputProps={{
				inputComponent: InputComponent,
			}}
			inputProps={{ children: children }}
		/>
	);
};
export default OutlinedDiv;
