import { useEffect } from "react";
import TextField from "@mui/material/TextField";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import { getI18n } from "react-i18next";

import { CustomChangeEvent } from "@/types/events";
import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";
import { DateInputValue } from "@/types/inputValues";

import useFocus from "@/hooks/form/hooks/useFocus";
import formatDateIfString from "@/hooks/form/utils/formatDateIfString";

const DateField = <T extends string>({
	name,
	label,
	field,
	shouldFocus,
}: TGenericNormalFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	return (
		<LocalizationProvider
			dateAdapter={AdapterDayjs}
			adapterLocale={getI18n().language}
			dateLibInstance={dayjs}
		>
			<DatePicker
				inputRef={ref as React.MutableRefObject<HTMLInputElement>}
				label={label}
				value={formatDateIfString(value.data as DateInputValue)}
				// inputFormat="DD/MM/YYYY" // Not neccesary 'cause i18n
				// minDate={dayjs("1900/01/01")} // Not neccesary. The hook handles it
				// maxDate={dayjs()}
				onChange={(value: unknown) => {
					const e = {
						target: { value: value, name } as unknown as HTMLInputElement,
					};
					handleChange(e as CustomChangeEvent<HTMLInputElement, T>);
				}}
				renderInput={(params: object) => (
					<TextField
						{...params}
						error={error as boolean}
						helperText={errorText}
						name={name}
						color="primary"
						required={required as boolean}
						sx={{
							width: 1,
						}}
						onBlur={handleBlur as any}
						variant="outlined"
					/>
				)}
			/>
		</LocalizationProvider>
	);
};

export default DateField;
