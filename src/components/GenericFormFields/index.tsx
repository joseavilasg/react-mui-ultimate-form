import { useCallback, useEffect, useRef, useState } from "react";

import {
	TCheckboxGroupFieldProps,
	TNormalFieldProps,
	TRadioButtonsGroupFieldProps,
	TSelectFieldProps,
	TVerifyInputMUIProps,
} from "@/types/components";
import { TFormFieldsDefStepOut, TFormPersistedValuesDef } from "@/types/formModel";
import {
	TGenericCheckboxGroupFieldProps,
	TGenericNormalFieldProps,
	TGenericRadioButtonsGroupFieldProps,
	TGenericSelectFieldProps,
	UseGenericFormFieldsProps,
} from "@/types/genericFormFieldHook";

import useFormField from "@/hooks/form/hooks/useFormField";
import useFormValues from "@/hooks/form/hooks/useFormValues";
import getActiveStep from "@/hooks/form/utils/getActiveStep";

import GenericCheckBoxField from "./CheckBoxField";
import GenericCheckboxGroup from "./CheckboxGroupField";
import GenericCountryRegionSelectorFields, {
	CountryRegionSelectorFieldsProps,
} from "./CountryRegionSelectorFields";
import GenericDateField from "./DateField";
import { newNoNamePropError } from "./errors";
import GenericInputField from "./InputField";
import GenericMobilePhoneField from "./MobilePhoneField";
import GenericMultilineInputField from "./MultilineInputField";
import GenericMultipleSelectField from "./MultipleSelectField";
import GenericPasswordField from "./PasswordField";
import GenericRadioButtonsGroup from "./RadioButtonsGroupField";
import GenericSelectField from "./SelectField";
import GenericVerifyInputField from "./VerifyInputField";

function useGenericFormFieldElements<F extends string, T extends string>({
	formModel,
	autoFocusOnRender = false,
	persistedValues,
}: UseGenericFormFieldsProps<F, T>) {
	const {
		values,
		handleSubmit,
		stepInfo: { activeStep },
	} = useFormValues({
		formModel,
	});
	const { steps: _steps } = formModel;
	const [steps, _setSteps] = useState(getActiveStep({ steps: _steps, activeStep }));
	const stepsRef = useRef(steps);
	const setSteps = (state: TFormFieldsDefStepOut<T>) => {
		stepsRef.current = state;
		_setSteps(state);
	};
	useEffect(() => {
		setSteps(getActiveStep({ steps: _steps, activeStep }));
	}, [activeStep]);

	const getShouldFocus = useCallback(
		(name: string) => {
			const firstKeyNameFromActiveFormModel = Object.keys(stepsRef.current)[0];
			const shouldFocus = name === firstKeyNameFromActiveFormModel;
			return autoFocusOnRender ? shouldFocus : false;
		},
		[stepsRef.current, activeStep],
	);

	const createNormalField = (Component: React.ComponentType<TGenericNormalFieldProps<T>>) => {
		return useCallback(
			(props: TNormalFieldProps<T>) => {
				if (!props?.name) {
					throw newNoNamePropError("NormalField");
				}
				const field = useFormField<F, T>({
					formModel: formModel,
					fieldName: props.name,
					persistedValue: (persistedValues?.[props.name]
						? persistedValues?.[props.name]
						: props.persistedValue) as TFormPersistedValuesDef<T>[T],
				});
				// const field = useField(
				// 	props.name,
				// 	persistedValues?.[props.name] || props.persistedValue,
				// );

				// if first key match with the actual name, then focus on it
				const shouldFocus = getShouldFocus(props.name);

				return (
					<Component
						{...props}
						shouldFocus={shouldFocus}
						field={field} // Pasar el campo de formulario adecuado al componente específico
					/>
				);
			},
			[activeStep, formModel],
		);
	};
	const createSelectField = (Component: React.ComponentType<TGenericSelectFieldProps<T>>) => {
		return useCallback(
			(props: TSelectFieldProps<T>) => {
				if (!props?.name) {
					throw newNoNamePropError("SelectField");
				}
				const field = useFormField<F, T>({
					formModel: formModel,
					fieldName: props.name,
					persistedValue: persistedValues?.[props.name]
						? persistedValues?.[props.name]
						: props.persistedValue,
				});

				// if first key match with the actual name, then focus on it
				const shouldFocus = getShouldFocus(props.name);

				return (
					<Component
						{...props}
						shouldFocus={shouldFocus}
						field={field} // Pass the appropriate form field to the specific component
					/>
				);
			},
			[activeStep, formModel],
		);
	};
	const createRadioButtonsGroupField = (
		Component: React.ComponentType<TGenericRadioButtonsGroupFieldProps<T>>,
	) => {
		return useCallback(
			(props: TRadioButtonsGroupFieldProps<T>) => {
				if (!props?.name) {
					throw newNoNamePropError("RadioButtonsGroupField");
				}
				const field = useFormField<F, T>({
					formModel: formModel,
					fieldName: props.name,
					persistedValue: persistedValues?.[props.name]
						? persistedValues?.[props.name]
						: props.persistedValue,
				});

				// if first key match with the actual name, then focus on it
				const shouldFocus = getShouldFocus(props.name);

				return (
					<Component
						{...props}
						shouldFocus={shouldFocus}
						field={field} // Pass the appropriate form field to the specific component
					/>
				);
			},
			[activeStep, formModel],
		);
	};
	const createCheckboxGroupField = (
		Component: React.ComponentType<TGenericCheckboxGroupFieldProps<T>>,
	) => {
		return useCallback(
			(props: TCheckboxGroupFieldProps<T>) => {
				if (!props?.name) {
					throw newNoNamePropError("CheckboxGroupField");
				}
				const field = useFormField<F, T>({
					formModel: formModel,
					fieldName: props.name,
					persistedValue: persistedValues?.[props.name]
						? persistedValues?.[props.name]
						: props.persistedValue,
				});

				// if first key match with the actual name, then focus on it
				const shouldFocus = getShouldFocus(props.name);

				return (
					<Component
						{...props}
						shouldFocus={shouldFocus}
						field={field} // Pass the appropriate form field to the specific component
					/>
				);
			},
			[activeStep, formModel],
		);
	};

	const InputField = createNormalField(GenericInputField);
	const PasswordField = createNormalField(GenericPasswordField);
	const MultilineInputField = createNormalField(GenericMultilineInputField);
	const DateField = createNormalField(GenericDateField);
	const MobilePhoneField = createNormalField(GenericMobilePhoneField);
	const CheckBoxField = createNormalField(GenericCheckBoxField);

	const SelectField = createSelectField(GenericSelectField);
	const MultipleSelectField = createSelectField(GenericMultipleSelectField);
	const RadioButtonsGroupField = createRadioButtonsGroupField(GenericRadioButtonsGroup);
	const CheckboxGroupField = createCheckboxGroupField(GenericCheckboxGroup);

	// TODO: Handle next focus when using country region selector, since this is implemented internally
	const CountryRegionSelectorFields = useCallback(
		(
			props: Omit<
				CountryRegionSelectorFieldsProps<T>,
				"countryField" | "regionField" | "provinceField" | "districtField"
			>,
		) => {
			// const countryField = useField(
			// 	props.country.name,
			// 	persistedValues?.[props.country.name] || props.country.persistedValue,
			// );
			if (
				!(
					props?.country?.name &&
					props?.district?.name &&
					props?.province?.name &&
					props?.region?.name
				)
			) {
				throw newNoNamePropError("CountryRegionSelectorFields");
			}
			const countryField = useFormField<F, T>({
				formModel: formModel,
				fieldName: props.country.name,
				persistedValue: persistedValues?.[props.country.name]
					? persistedValues?.[props.country.name]
					: props.country.persistedValue,
			});
			const regionField = useFormField<F, T>({
				formModel: formModel,
				fieldName: props.region.name,
				persistedValue: persistedValues?.[props.region.name]
					? persistedValues?.[props.region.name]
					: props.region.persistedValue,
			});
			const provinceField = useFormField<F, T>({
				formModel: formModel,
				fieldName: props.province.name,
				persistedValue: persistedValues?.[props.province.name]
					? persistedValues?.[props.province.name]
					: props.province.persistedValue,
			});
			const districtField = useFormField<F, T>({
				formModel: formModel,
				fieldName: props.district.name,
				persistedValue: persistedValues?.[props.district.name]
					? persistedValues?.[props.district.name]
					: props.district.persistedValue,
			});
			return (
				<GenericCountryRegionSelectorFields
					{...props}
					countryField={countryField}
					regionField={regionField}
					provinceField={provinceField}
					districtField={districtField}
				/>
			);
		},
		[activeStep, formModel],
	);

	const VerifyInputField = useCallback(
		(props: Omit<TVerifyInputMUIProps<T>, "field" | "shouldFocus">) => {
			if (!props?.name) {
				throw newNoNamePropError("VerifyInputField");
			}
			const field = useFormField<F, T>({
				formModel: formModel,
				fieldName: props.name,
				persistedValue: persistedValues?.[props.name]
					? persistedValues?.[props.name]
					: props.persistedValue,
			});
			// (props.name, persistedValues?.[props.name] || props.persistedValue);

			const shouldFocus = getShouldFocus(props.name);

			return <GenericVerifyInputField {...props} field={field} shouldFocus={shouldFocus} />;
		},
		[activeStep, formModel],
	);

	return {
		components: {
			InputField,
			PasswordField,
			MultilineInputField,
			DateField,
			SelectField,
			MultipleSelectField,
			RadioButtonsGroupField,
			CheckboxGroupField,
			MobilePhoneField,
			CheckBoxField,
			CountryRegionSelectorFields,
			VerifyInputField,
		},
		steps,
		values,
		handleSubmit,
	};
}

export default useGenericFormFieldElements;
