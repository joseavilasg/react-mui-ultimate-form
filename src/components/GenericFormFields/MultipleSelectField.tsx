import { useEffect } from "react";
import { KEYS } from "@/const";

import { FormMultipleSelectMUIChangeEvent } from "@/types/events";
import { TGenericSelectFieldProps } from "@/types/genericFormFieldHook";
import { MultipleSelectInputValue } from "@/types/inputValues";
import { SelectTRef } from "@/types/refs";

import useFocus from "@/hooks/form/hooks/useFocus";

import MultipleSelectFieldMUI from "./CustomFields/MultipleSelectFieldMUI";

const MultipleSelectField = <T extends string>({
	name,
	label,
	field,
	options = [],
	shouldFocus,
	disableLazyLoadOptions = false,
	renderedValueSeparator,
}: TGenericSelectFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	const customLabel = `${label}${required ? " *" : ""}`;

	const handleKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === KEYS.ENTER) {
			e.preventDefault();
		}
	};

	// TODO: infer type of value from formInitialValues and formFields type.
	const handleSelectDisplayKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === KEYS.ENTER) {
			e.preventDefault();
			const target = e.target as HTMLElement;

			let form: HTMLFormElement | null = null;
			let currentNode: HTMLElement | null = target;
			while (currentNode.parentNode) {
				if (currentNode.tagName === "FORM") {
					form = currentNode as HTMLFormElement;
					break;
				}
				currentNode = currentNode.parentNode as HTMLElement;
			}

			if (form) {
				form.requestSubmit();
			}
		}
		if (e.key === " " || e.key === KEYS.ARROWDOWN || e.key === KEYS.ARROWUP) {
			e.preventDefault();
			(ref as SelectTRef).current?.openSelect();
		}
	};

	return (
		<MultipleSelectFieldMUI
			selectRef={ref as SelectTRef}
			rederedValueSeparator={renderedValueSeparator}
			error={error as boolean}
			errorText={errorText}
			name={name}
			required={required as boolean}
			label={customLabel}
			value={value.data as MultipleSelectInputValue}
			onChange={handleChange as any as FormMultipleSelectMUIChangeEvent}
			onBlur={handleBlur as any}
			options={options}
			disableLazyLoadOptions={disableLazyLoadOptions}
			MenuProps={{
				onKeyDown: handleKeyDown,
			}}
			SelectDisplayProps={{
				onKeyDown: handleSelectDisplayKeyDown,
			}}
		/>
	);
};

export default MultipleSelectField;
