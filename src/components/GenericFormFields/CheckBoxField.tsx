import { useEffect } from "react";
import {
	Checkbox,
	FormControl,
	FormControlLabel,
	FormGroup,
	FormHelperText,
	Link,
	Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import Markdown from "markdown-to-jsx";

import { FormButtonBlurEvent } from "@/types/events";
import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";

import useFocus from "@/hooks/form/hooks/useFocus";

const CheckBoxField = <T extends string>({
	name,
	label,
	field,
	shouldFocus,
}: TGenericNormalFieldProps<T>) => {
	const theme = useTheme();
	const { ref, required, error, errorText, value, handleBlur, handleCheckBoxChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	return (
		<FormControl
			error={error as boolean}
			sx={{
				width: 1,
				mr: {
					desktop: "100px",
				},
			}}
		>
			<FormGroup
				sx={{
					"& .MuiTypography-root": {
						width: "auto",
					},
				}}
			>
				<FormControlLabel
					control={
						<Checkbox
							inputRef={ref as React.RefObject<HTMLInputElement>}
							required={required as boolean}
							name={name}
							checked={Boolean(value.data as string)}
							onChange={handleCheckBoxChange as any}
							onBlur={handleBlur as any as FormButtonBlurEvent}
						/>
					}
					label={
						<Markdown
							options={{
								// forceBlock: true, // This was causing clicking on the label to work weird
								overrides: {
									p: ({ children, ...rest }) => {
										return (
											<Typography {...rest} color={theme.palette.text.primary}>
												{children}
											</Typography>
										);
									},
									a: ({ children, ...rest }) => {
										return (
											<Link
												{...rest}
												underline="always"
												rel="noopener noreferrer"
												target="_blank"
											>
												{children}
											</Link>
										);
									},
								},
							}}
						>
							{label}
						</Markdown>
					}
				/>
			</FormGroup>
			<FormHelperText>{errorText}</FormHelperText>
		</FormControl>
	);
};

export default CheckBoxField;
