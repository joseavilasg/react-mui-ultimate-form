import { Ref, useEffect } from "react";
import { getI18n } from "react-i18next";
import { PhoneInputMUI } from "react-mui-phone-input";

import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";
import { TLanguage } from "@/types/language";

import useFocus from "@/hooks/form/hooks/useFocus";

const MobilePhoneField = <T extends string>({
	name,
	label,
	field,
	shouldFocus,
}: TGenericNormalFieldProps<T>) => {
	const { ref, error, errorText, required, handlePhoneChange, value } = field;
	// const { i18n } = useTranslation();

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	return (
		<>
			<PhoneInputMUI
				ref={ref as Ref<HTMLInputElement>}
				variant="outlined"
				lang={getI18n().language as TLanguage}
				name={name}
				value={value.data as string}
				label={label}
				required={required as boolean}
				helperText={errorText}
				forceInternationalFormat
				defaultCountryIso2="PE"
				error={error as boolean}
				onChange={({ e, value, formattedValue, selectedCountry }) => {
					handlePhoneChange(value, selectedCountry, e as any, formattedValue);
				}}
			/>
		</>
	);
};

export default MobilePhoneField;
