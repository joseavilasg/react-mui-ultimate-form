import { useEffect } from "react";

import { FormDivBlurEvent } from "@/types/events";
import { TGenericRadioButtonsGroupFieldProps } from "@/types/genericFormFieldHook";
import { RadioGroupInputValue } from "@/types/inputValues";
import { RadioGroupTRef } from "@/types/refs";

import useFocus from "@/hooks/form/hooks/useFocus";

import RadioButtonsGroupMUI from "./CustomFields/RadioButtonsGroupMUI";

const RadioButtonsGroupField = <T extends string>({
	name,
	label,
	field,
	sx,
	options = [],
	row,
	standalone,
	labelType,
	shouldFocus,
}: TGenericRadioButtonsGroupFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleRadioButtonChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus(); // TODO: Not working by now, fix it.
		}
	}, [shouldFocus]);

	return (
		<RadioButtonsGroupMUI
			radioGroupRef={ref as RadioGroupTRef}
			sx={sx}
			row={row}
			standalone={standalone}
			labelType={labelType}
			error={error as boolean}
			errorText={errorText}
			name={name}
			required={required as boolean}
			label={label}
			value={value.data as RadioGroupInputValue}
			onChange={handleRadioButtonChange as any}
			onBlur={handleBlur as any as FormDivBlurEvent}
			options={options}
		/>
	);
};

export default RadioButtonsGroupField;
