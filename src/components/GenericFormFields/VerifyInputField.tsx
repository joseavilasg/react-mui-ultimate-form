import { useEffect, useState } from "react";
import { Box, FormControl, FormHelperText, InputLabel, TextField } from "@mui/material";

import { TVerifyInputMUIProps } from "@/types/components";
import { VerificationCodeInputValue } from "@/types/inputValues";
import { NormalTRef, TRef, TRefCurrent } from "@/types/refs";

import useFocus from "@/hooks/form/hooks/useFocus";
import useGetRef from "@/hooks/form/hooks/useGetRef";

const VerifyInputField = <T extends string>({
	name,
	label,
	sx,
	showLabel = false,
	showHelperText = false,
	field,
	shouldFocus,
}: TVerifyInputMUIProps<T>) => {
	const {
		ref: refs,
		error: errors,
		value: values,
		required,
		errorText,
		handleVerificationCodeBlur: handleBlur,
		handleVerificationCodeChange: handleChange,
		handleVerificationCodeKeyDown: handleKeyDown,
	} = field;

	const getRef = useGetRef();

	refs.current = {};

	const [refRender, setRefRender] = useState<NormalTRef>(refs.current[0]);
	const [setFocus] = useFocus(refRender);

	useEffect(() => {
		if (
			(refs?.current as TRefCurrent<"record">)?.[
				(values.data as VerificationCodeInputValue).length - 1
			]
		) {
			if ((refs.current as TRefCurrent<"record">) !== null) {
				setRefRender((refs.current as NonNullable<TRefCurrent<"record">>)[0]);
			}
		}
	}, []);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus, refRender]);

	return (
		<Box sx={{ display: "flex", flexWrap: "wrap", width: 1 }} style={{ marginTop: "3rem" }}>
			<FormControl fullWidth>
				{showLabel && (
					<InputLabel
						htmlFor={name + 1}
						sx={{ width: 1, mt: -6 }}
						required={required as boolean}
						error={Array.isArray(errors) ? errors.find((error) => error === true) : errors}
					>
						{label}
					</InputLabel>
				)}
				<Box
					display="flex"
					flexWrap="nowrap"
					justifyContent="space-between"
					alignItems="center"
					component="div"
					sx={{
						"& .MuiInputBase-input": {
							maxWidth: "28px",
							textAlign: "center",
							fontSize: "40px",
							fontWeight: "bold",
							padding: "8px 14px",
						},
						width: 1,
						...sx,
					}}
				>
					{((values.data || []) as string[]).map((value, i) => {
						const index: string = i.toString();

						(refs?.current as Record<string, TRef>)[index] = getRef(index);
						return (
							<TextField
								id={name + i}
								key={i}
								type="tel"
								error={(errors as boolean[])[i]}
								// disabled={disabled}
								value={value}
								inputRef={getRef(index)}
								inputProps={{
									pattern: "[0-9]*",
									/* ref: refs, */ "data-id": `${i}`,
								}}
								tabIndex={i}
								onKeyDown={handleKeyDown}
								onBlur={handleBlur}
								onChange={handleChange}
							/>
						);
					})}
				</Box>
				{showHelperText && (
					<FormHelperText error={(errors as boolean[])?.some((error) => error === true)}>
						{errorText}
					</FormHelperText>
				)}
			</FormControl>
		</Box>
	);
};

export default VerifyInputField;
