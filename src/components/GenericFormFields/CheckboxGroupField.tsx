import { useEffect } from "react";

import { FormDivBlurEvent } from "@/types/events";
import { TGenericCheckboxGroupFieldProps } from "@/types/genericFormFieldHook";
import { CheckboxGroupInputValue } from "@/types/inputValues";
import { CheckboxGroupTRef } from "@/types/refs";

import useFocus from "@/hooks/form/hooks/useFocus";

import CheckboxGroupMUI from "./CustomFields/CheckboxGroupMUI";

const CheckboxGroupField = <T extends string>({
	name,
	label,
	field,
	sx,
	options = [],
	row,
	standalone,
	labelType,
	shouldFocus,
}: TGenericCheckboxGroupFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleCheckboxGroupChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus(); // TODO: Not working by now, fix it.
		}
	}, [shouldFocus]);

	return (
		<CheckboxGroupMUI
			checkboxGroupRef={ref as CheckboxGroupTRef}
			sx={sx}
			row={row}
			standalone={standalone}
			labelType={labelType}
			error={error as boolean}
			errorText={errorText}
			name={name}
			required={required as boolean}
			label={label}
			value={value.data as CheckboxGroupInputValue}
			onChange={handleCheckboxGroupChange as any}
			onBlur={handleBlur as any as FormDivBlurEvent}
			options={options}
		/>
	);
};

export default CheckboxGroupField;
