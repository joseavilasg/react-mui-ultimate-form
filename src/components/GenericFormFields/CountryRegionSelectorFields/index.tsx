import { useEffect, useRef, useState } from "react";

import { TSelectFieldProps } from "@/types/components";
import { FormChangeEvent, FormNormalSelectChangeEvent } from "@/types/events";
import { TUseFormFieldResult } from "@/types/formFieldHook";

import CustomSelectField from "./CustomSelectField";
import countryRegionData from "./CustomSelectField/country-region-data-es";
import { FormChangeAutocompleteEvent } from "./CustomSelectField/CustomAutocomplete";
import {
	Country,
	District,
	getCountry,
	getDistrict,
	getProvince,
	getRegion,
	ICountry,
	IDistrict,
	IProvince,
	IRegion,
	Province,
	Region,
} from "./CustomSelectField/helpers";
import { getDistrictsByProvince, getProvincesByRegion, getRegionsByCountry } from "./helpers";

// TODO: add restriction to province and district, since only Peru has it
export interface CountryRegionSelectorFieldsProps<T extends string> {
	country: Omit<TSelectFieldProps<T>, "options">;
	region: Omit<TSelectFieldProps<T>, "options">;
	province: Omit<TSelectFieldProps<T>, "options">;
	district: Omit<TSelectFieldProps<T>, "options">;
	countryField: TUseFormFieldResult<T>;
	regionField: TUseFormFieldResult<T>;
	provinceField: TUseFormFieldResult<T>;
	districtField: TUseFormFieldResult<T>;
	nativeSelect?: boolean;
}

export type THandleCustomChangeNative = (
	customHandle: FormNormalSelectChangeEvent,
) => React.ChangeEventHandler<HTMLSelectElement>;

export type THandleCustomChangeAutocomplete = (
	customHandle: FormChangeEvent,
) => FormChangeAutocompleteEvent;
// type FieldNameType = string;

const CountryRegionSelectorFields = <T extends string>(
	props: CountryRegionSelectorFieldsProps<T>,
) => {
	const {
		countryField,
		country: countryProps,
		regionField,
		region: regionProps,
		provinceField,
		province: provinceProps,
		districtField,
		district: districtProps,
		nativeSelect = false,
	} = props;

	let countryVal: ICountry = countryField.value.data as string;
	let regionVal: IRegion = regionField.value.data as string;
	let provinceVal: IProvince = provinceField.value.data as string;
	let districtVal: IDistrict = districtField.value.data as string;

	if (!nativeSelect) {
		countryVal = getCountry(countryField.value.data as string);
		regionVal = getRegion(countryField.value.data as string, regionField.value.data as string);
		provinceVal = getProvince(
			countryField.value.data as string,
			regionField.value.data as string,
			provinceField.value.data as string,
		);
		districtVal = getDistrict(
			countryField.value.data as string,
			regionField.value.data as string,
			provinceField.value.data as string,
			districtField.value.data as string,
		);
	}

	// Country
	const [countryValue, _setCountryValue] = useState<ICountry>(countryVal);
	const countryValueRef = useRef(countryValue);
	const setCountryValue = (state: ICountry) => {
		countryValueRef.current = state;
		_setCountryValue(state);
	};
	useEffect(() => {
		setCountryValue(countryVal);
		const regions = getRegionsByCountry(countryValueRef.current, nativeSelect);
		setRegions(regions);
	}, [countryVal]);

	// Region
	const [regions, _setRegions] = useState<IRegion[]>(
		((countryVal as Country)?.regions as IRegion[]) || [],
	);
	const regionsRef = useRef(regions);
	const setRegions = (state: IRegion[]) => {
		regionsRef.current = state;
		_setRegions(state);
	};
	const [regionValue, _setRegionValue] = useState<IRegion>(regionVal);
	const regionValueRef = useRef(regionValue);
	const setRegionValue = (state: IRegion) => {
		regionValueRef.current = state;
		_setRegionValue(state);
	};
	useEffect(() => {
		setRegionValue(regionVal);
		setProvinces(getProvincesByRegion(regionValueRef.current, regionsRef.current, nativeSelect));
	}, [regionVal]);

	// Province
	const [provinces, _setProvinces] = useState<IProvince[]>(
		((regionVal as Region)?.provinces as IProvince[]) || [],
	);
	const provincesRef = useRef(provinces);
	const setProvinces = (state: IProvince[]) => {
		provincesRef.current = state;
		_setProvinces(state);
	};
	const [provinceValue, _setProvinceValue] = useState<IProvince>(provinceVal);
	const provinceValueRef = useRef(provinceValue);
	const setProvinceValue = (state: IProvince) => {
		provinceValueRef.current = state;
		_setProvinceValue(state);
	};
	useEffect(() => {
		setProvinceValue(provinceVal);
		setDistricts(
			getDistrictsByProvince(provinceValueRef.current, provincesRef.current, nativeSelect),
		);
	}, [provinceVal]);

	// District
	const [districts, _setDistricts] = useState<IDistrict[]>(
		((provinceVal as Province)?.districts as IDistrict[]) || [],
	);
	const districtsRef = useRef(districts);
	const setDistricts = (state: IDistrict[]) => {
		districtsRef.current = state;
		_setDistricts(state);
	};
	const [districtValue, _setDistrictValue] = useState<IDistrict>(districtVal);
	const districtValueRef = useRef(districtValue);
	const setDistrictValue = (state: IDistrict) => {
		districtValueRef.current = state;
		_setDistrictValue(state);
	};
	useEffect(() => {
		setDistrictValue(districtVal);
	}, [provinceVal]);

	const cleanCountry = () => {
		setCountryValue("");
		setRegions([]);
		cleanRegion();
	};

	const cleanRegion = () => {
		setRegionValue("");
		setProvinces([]);
		cleanProvince();
	};

	const cleanProvince = () => {
		setProvinceValue("");
		setDistricts([]);
		cleanDistrict();
	};

	const cleanDistrict = () => {
		setDistrictValue("");
	};

	const handleCountryChangeNative: THandleCustomChangeNative = (customHandle) => (e) => {
		cleanCountry();
		customHandle(e);
		setCountryValue(e.target.value);
		setRegions(getRegionsByCountry(countryValueRef.current, true));
	};

	const handleCountryChangeAutocomplete: THandleCustomChangeAutocomplete =
		(customHandle) => (_, newValue) => {
			const e = {
				target: {
					name: countryProps.name,
					value: (newValue as Country)?.countryShortCode || "",
				},
			} as unknown as React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
			cleanCountry();
			customHandle(e);
			setCountryValue(newValue as ICountry);
			setRegions(getRegionsByCountry(countryValueRef.current, false));
		};

	const handleRegionChangeNative: THandleCustomChangeNative = (customHandle) => (e) => {
		cleanRegion();
		customHandle(e);
		setRegionValue(e.target.value);
		setProvinces(getProvincesByRegion(regionValueRef.current, regionsRef.current, true));
	};
	const handleRegionChangeAutocomplete: THandleCustomChangeAutocomplete =
		(customHandle) => (event, newValue) => {
			const e = {
				target: {
					name: regionProps.name,
					value: (newValue as Region)?.shortCode || "",
				},
			} as unknown as React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
			cleanRegion();
			customHandle(e);
			setRegionValue(newValue as IRegion);
			setProvinces(getProvincesByRegion(regionValueRef.current, regionsRef.current, false));
		};

	const handleProvinceChangeNative: THandleCustomChangeNative = (customHandle) => (e) => {
		setDistricts([]);
		customHandle(e);
		setProvinceValue(e.target.value);
		setDistricts(getDistrictsByProvince(provinceValueRef.current, provincesRef.current, true));
	};
	const handleProvinceChangeAutocomplete: THandleCustomChangeAutocomplete =
		(customHandle) => (_, newValue) => {
			const e = {
				target: {
					name: provinceProps.name,
					value: (newValue as Province)?.name || "",
				},
			} as unknown as React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
			cleanProvince();
			customHandle(e);
			setProvinceValue(newValue as IProvince);
			setDistricts(getDistrictsByProvince(provinceValueRef.current, provincesRef.current, true));
		};

	const handleDistrictChangeNative: THandleCustomChangeNative = (customHandle) => (e) => {
		cleanDistrict();
		customHandle(e);
		setDistrictValue(e.target.value);
	};
	const handleDistrictChangeAutocomplete: THandleCustomChangeAutocomplete =
		(customHandle) => (_, newValue) => {
			const e = {
				target: {
					name: districtProps.name,
					value: (newValue as District)?.name || "",
				},
			} as unknown as React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
			cleanDistrict();
			customHandle(e);
			setDistrictValue(newValue as IProvince);
		};

	return (
		<>
			<CustomSelectField
				useNativeSelect={nativeSelect}
				field={countryField}
				name={countryProps.name}
				label={countryProps.label}
				value={countryValue}
				options={(countryRegionData as unknown as ICountry[]) || []}
				onChangeAutocomplete={handleCountryChangeAutocomplete}
				onChangeNative={handleCountryChangeNative}
			/>
			<CustomSelectField
				useNativeSelect={nativeSelect}
				field={regionField}
				name={regionProps.name}
				label={regionProps.label}
				value={regionValue}
				options={regions}
				onChangeAutocomplete={handleRegionChangeAutocomplete}
				onChangeNative={handleRegionChangeNative}
			/>
			{((countryValue as Country)?.countryShortCode === "PE" || countryValue === "PE") && (
				<>
					<CustomSelectField
						useNativeSelect={nativeSelect}
						field={provinceField}
						name={provinceProps.name}
						label={provinceProps.label}
						value={provinceValue}
						options={provinces}
						onChangeAutocomplete={handleProvinceChangeAutocomplete}
						onChangeNative={handleProvinceChangeNative}
					/>
					<CustomSelectField
						useNativeSelect={nativeSelect}
						field={districtField}
						name={districtProps.name}
						label={districtProps.label}
						value={districtValue}
						options={districts}
						onChangeAutocomplete={handleDistrictChangeAutocomplete}
						onChangeNative={handleDistrictChangeNative}
					/>
				</>
			)}
		</>
	);
};

export default CountryRegionSelectorFields;
