import { getTranslations } from "@/plugins/translations";
import { getI18n } from "react-i18next";

import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";

import {
	THandleCustomChangeAutocomplete,
	THandleCustomChangeNative,
} from "../../CountryRegionSelectorFields";
import CustomAutocomplete from "./CustomAutocomplete";
import CustomNativeSelect from "./CustomNativeSelect";
import {
	Country,
	District,
	ICountry,
	IDistrict,
	IProvince,
	IRegion,
	Province,
	Region,
} from "./helpers";

type CustomInputFieldProps<T extends string> = Omit<
	TGenericNormalFieldProps<T>,
	"options" | "shouldFocus"
> & {
	value: ICountry | IRegion | IProvince | IDistrict;
	options: ICountry[] | IRegion[] | IProvince[] | IDistrict[];
	onChangeAutocomplete: THandleCustomChangeAutocomplete;
	onChangeNative: THandleCustomChangeNative;
	useNativeSelect: boolean;
};

const CustomSelectField = <T extends string>({
	name,
	label,
	value,
	options,
	onChangeAutocomplete,
	onChangeNative,
	useNativeSelect,
	field,
}: CustomInputFieldProps<T>) => {
	const { ref, error, errorText, required, handleBlur, handleChange } = field;

	const translations = getTranslations(getI18n().t).autocompleteMUIProps;

	const getOptionLabel = (option: unknown) => {
		if (name === "country") {
			return ((option as Country)?.countryName as string) || "";
		} else if (name === "region") {
			return ((option as Region)?.name as string) || "";
		} else if (name === "province") {
			return ((option as Province)?.name as string) || "";
		} else if (name === "district") {
			return ((option as District)?.name as string) || "";
		}

		return "";
	};

	const isOptionEqualToValue = (option: unknown, value: unknown) => {
		if (name === "country") {
			return (option as Country)?.countryShortCode === (value as Country)?.countryShortCode;
		} else if (name === "region") {
			return (option as Region)?.shortCode === (value as Region)?.shortCode;
		} else if (name === "province") {
			return (option as Province)?.name === (value as Province)?.name;
		} else if (name === "district") {
			return (option as District)?.name === (value as District)?.name;
		}

		return false;
	};

	// Update values if new value is empty.
	// Up today, october, this is causing infinite loop
	// const handleInputChange: FormChangeAutocompleteEvent = (event, newValue) => {
	// 	console.log({ newValue });
	// 	if (!newValue) {
	// 		const e = {
	// 			target: { name, value: "" },
	// 		} as unknown as React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
	// 		handleChange(e as any);
	// 	}
	// };

	if (useNativeSelect) {
		return (
			<CustomNativeSelect
				selectRef={ref}
				error={error as boolean}
				errorText={errorText as string}
				required={required as boolean}
				label={label}
				name={name}
				value={value as string}
				onChange={onChangeNative(handleChange as any)}
				onBlur={handleBlur as any}
			>
				{(() => {
					return options.map((option, i) => {
						return (
							<option
								key={i}
								value={
									(option as Country)?.countryShortCode ||
									(option as Region)?.shortCode ||
									(option as Province | District)?.name
								}
							>
								{(option as Country)?.countryName ||
									(option as Region | Province | District)?.name}
							</option>
						);
					});
				})()}
			</CustomNativeSelect>
		);
	}

	return (
		<CustomAutocomplete
			selectRef={ref}
			error={error as boolean}
			errorText={errorText as string}
			required={required as boolean}
			options={options as Country[] | Region[] | Province[] | District[]}
			noOptionsText={translations?.noOptionsText}
			getOptionLabel={getOptionLabel}
			isOptionEqualToValue={isOptionEqualToValue}
			label={label}
			name={name}
			value={value as Country | Region | Province | District}
			onChange={onChangeAutocomplete(handleChange as any)}
			// onInputChange={handleInputChange}
			onBlur={handleBlur as any}
		/>
	);
};

export default CustomSelectField;
