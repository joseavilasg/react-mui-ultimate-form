import React, { FC } from "react";
import { FormControl, FormHelperText, InputBase, InputLabel, NativeSelect } from "@mui/material";

import { TRef } from "@/types/refs";

import useStyles from "./useStyles";

interface CustomNativeSelectProps {
	children?: React.ReactNode;
	selectRef: TRef;
	error: boolean;
	errorText: string;
	required: boolean;
	label: string;
	name: string;
	value: string;
	onChange: React.ChangeEventHandler<HTMLSelectElement>;
	onBlur: React.FocusEventHandler<HTMLInputElement>;
}

const CustomNativeSelect: FC<CustomNativeSelectProps> = (props) => {
	const { children, selectRef, error, errorText, required, label, name, value, onChange, onBlur } =
		props;

	const styles = useStyles();

	return (
		<FormControl error={error} fullWidth>
			<InputLabel sx={{ ...styles.InputLabel }}>
				{label}
				{required ? " *" : ""}
			</InputLabel>
			<NativeSelect
				name={name}
				required={required}
				variant="outlined"
				value={value}
				input={
					error ? (
						<InputBase sx={{ ...styles.InputBaseError }} />
					) : (
						<InputBase sx={{ ...styles.InputBase }} />
					)
				}
				inputRef={selectRef}
				onChange={onChange}
				onBlur={onBlur}
				// onKeyPress={(e: any) => {
				// 	if (e.key == "Enter") {
				// 		// handleSubmit();
				// 	}
				// }}
				// defaultValue={defaultValue}
			>
				<option value="">{""}</option>
				{children}
			</NativeSelect>
			<FormHelperText>{errorText}</FormHelperText>
		</FormControl>
	);
};

export default CustomNativeSelect;
