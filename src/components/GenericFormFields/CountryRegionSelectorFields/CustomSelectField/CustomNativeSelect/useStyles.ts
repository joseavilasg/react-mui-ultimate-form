import { useTheme } from "@mui/material";

const useStyles = () => {
	const theme = useTheme();
	return {
		InputLabel: {
			backgroundColor: "#fff",
			padding: "0 5px",
			marginLeft: "-5px",
			color: theme.palette.text.secondary,
		},
		InputBase: {
			"label + &": {
				marginTop: theme.spacing(0),
				// zIndex: -1,
				position: "relative",
			},
			"& .MuiInputBase-input": {
				minHeight: "34px",
				borderRadius: 1,
				position: "relative",
				backgroundColor: theme.palette.background.paper,
				border: `1px solid #0000003b`,
				// #ced4da || #0000003b
				fontSize: 16,
				padding: "10px 26px 10px 12px",
				// transition: theme.transitions.create(["border-color", "box-shadow"]),
				"&:hover": {
					borderColor: "#000",
				},
				"&:focus": {
					borderColor: theme.palette.primary.main,
					borderWidth: "2px",
					marginTop: "-2px",
				},
			},
		},
		InputBaseError: {
			"label + &": {
				marginTop: theme.spacing(0),
				// zIndex: -1,
				position: "relative",
			},
			"& .MuiInputBase-input": {
				minHeight: "34px",
				borderRadius: 1,
				position: "relative",
				backgroundColor: theme.palette.background.paper,
				border: `1px solid ${theme.palette.error.main}`,
				fontSize: 16,
				padding: "10px 26px 10px 12px",
				// transition: theme.transitions.create(["border-color", "box-shadow"]),
			},
		},
	};
};

export default useStyles;
