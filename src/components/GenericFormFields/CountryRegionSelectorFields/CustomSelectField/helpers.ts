import countryRegionData from "./country-region-data-es";

export type District = {
	name: string;
} | null;

export type IDistrict = District | string;

export type Province = {
	name: string;
	districts: IDistrict[];
} | null;

export type IProvince = Province | string;

export type Region = {
	name: string;
	shortCode?: string;
	provinces?: IProvince[];
} | null;

export type IRegion = Region | string;

export type Country = {
	countryName: string;
	countryShortCode: string;
	regions: IRegion[];
} | null;

export type ICountry = Country | string;

interface IGetCountry {
	(countryVal: string): ICountry;
}

interface IGetRegion {
	(countryVal: string, regionVal: string): IRegion;
}

interface IGetProvince {
	(countryVal: string, regionVal: string, provinceVal: string): IProvince;
}

interface IGetDistrict {
	(countryVal: string, regionVal: string, provinceVal: string, districtVal: string): IDistrict;
}

export const getCountry: IGetCountry = (countryVal) => {
	let data: ICountry = "";
	if (countryVal) {
		data = countryRegionData?.find((c) => c.countryShortCode == countryVal) as Country;
	}
	return data;
};

export const getRegion: IGetRegion = (countryVal, regionVal) => {
	let data: IRegion = "";
	if (countryVal && regionVal) {
		const regions = (getCountry(countryVal) as Country)?.regions;
		data = regions?.find((r) => (r as Region)?.shortCode == regionVal) as Region;
	}
	return data;
};

export const getProvince: IGetProvince = (countryVal, regionVal, provinceVal) => {
	let data: IProvince = "";
	if (countryVal && regionVal && provinceVal) {
		const provinces = (getRegion(countryVal, regionVal) as Region)?.provinces;
		data = provinces?.find((p) => (p as Province)?.name == provinceVal) as Province;
	}
	return data;
};

export const getDistrict: IGetDistrict = (countryVal, regionVal, provinceVal, districtVal) => {
	let data: IDistrict = "";
	if (countryVal && regionVal && provinceVal && districtVal) {
		const districts = (getProvince(countryVal, regionVal, provinceVal) as Province)?.districts;
		data = districts?.find((d) => (d as District)?.name == districtVal) as District;
	}
	return data;
};
