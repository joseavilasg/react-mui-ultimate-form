import * as React from "react";
import { Box, BoxProps } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import { VariableSizeList } from "react-window";

import { Country, District, Province, Region } from "../helpers";

const LISTBOX_PADDING = 8; // px

interface IRenderRow {
	data: Country[] | Region[] | Province[] | District[];
	index: number;
	style: React.CSSProperties;
}

type IDataSet = [BoxProps, Country | Region | Province | District];

const renderRow: React.FC<IRenderRow> = (props) => {
	const { data, index, style } = props;
	const dataSet = data[index] as unknown as IDataSet;
	const inlineStyle = {
		...style,
		top: (style?.top as number) + LISTBOX_PADDING,
	};

	return (
		<Box
			component="li"
			sx={{ "& > img": { mr: 2, flexShrink: 0 } }}
			flexWrap="nowrap"
			style={inlineStyle}
			{...dataSet[0]}
		>
			{(dataSet[1] as Country)?.countryShortCode && (
				<img
					loading="lazy"
					width="20"
					src={`https://flagcdn.com/${(
						dataSet[1] as Country
					)?.countryShortCode.toLowerCase()}.svg`}
					srcSet={`https://flagcdn.com/${(
						dataSet[1] as Country
					)?.countryShortCode.toLowerCase()}.svg 2x`}
					alt=""
				/>
			)}
			{(dataSet[1] as Country)?.countryName ||
				(dataSet[1] as Region | Province | District)?.name}
			{(dataSet[1] as Country)?.countryShortCode
				? ` (${(dataSet[1] as Country)?.countryShortCode})`
				: ""}
		</Box>
	);
};

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef<HTMLDivElement>(function OuterElementType(props, ref) {
	const outerProps = React.useContext(OuterElementContext);
	return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data: number) {
	const ref = React.useRef<VariableSizeList>(null);
	React.useEffect(() => {
		if (ref.current != null) {
			ref.current.resetAfterIndex(0, true);
		}
	}, [data]);
	return ref;
}

interface ListboxComponentProps {
	children: IDataSet[];
}

// Adapter for react-window
const ListboxComponent: React.FC<ListboxComponentProps> = React.forwardRef(
	function ListboxComponent(props, ref) {
		const { children, ...other } = props;
		const itemData: IDataSet[] = [];
		children.forEach((item: IDataSet) => {
			itemData.push(item);
			// itemData.push(...(item.children || []));
		});

		const theme = useTheme();
		const smUp = useMediaQuery(theme.breakpoints.up("xs"), {
			noSsr: true,
		});

		const itemCount = itemData.length;
		const itemSize = smUp ? 36 : 48;

		const getChildSize = (child: IDataSet) => {
			if (Object.prototype.hasOwnProperty.call(child, "group")) {
				// Never executes, because group is not a property of IDataSet. Anyways, I let this for future use.
				// if (child.hasOwnProperty("group")) {
				return 48;
			}

			return itemSize;
		};

		const getHeight = () => {
			if (itemCount > 8) {
				return 8 * itemSize;
			}
			return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
		};

		const gridRef = useResetCache(itemCount);

		return (
			<div ref={ref as React.ForwardedRef<HTMLDivElement>}>
				<OuterElementContext.Provider value={other}>
					<VariableSizeList
						itemData={itemData}
						height={getHeight() + 2 * LISTBOX_PADDING}
						width="100%"
						ref={gridRef}
						outerElementType={OuterElementType}
						innerElementType="ul"
						itemSize={(index) => getChildSize(itemData[index])}
						overscanCount={5}
						itemCount={itemCount}
					>
						{renderRow}
					</VariableSizeList>
				</OuterElementContext.Provider>
			</div>
		);
	},
);

export default ListboxComponent;
