import React, { HtmlHTMLAttributes, JSXElementConstructor } from "react";
import {
	Autocomplete,
	autocompleteClasses,
	FormControl,
	FormHelperText,
	TextField,
} from "@mui/material";
import Popper from "@mui/material/Popper";
import { styled } from "@mui/material/styles";

import { FormBlurEvent } from "@/types/events";
import { TRef } from "@/types/refs";

import {
	Country,
	District,
	ICountry,
	IDistrict,
	IProvince,
	IRegion,
	Province,
	Region,
} from "../helpers";
import ListboxComponent from "./ListBoxComponentReactWindow";

const StyledPopper = styled(Popper)({
	[`& .${autocompleteClasses.listbox}`]: {
		boxSizing: "border-box",
		"& ul": {
			padding: 0,
			margin: 0,
		},
	},
});

export interface FormChangeAutocompleteEvent {
	(
		event: React.ChangeEvent<HTMLInputElement>,
		newValue: ICountry | IRegion | IProvince | IDistrict,
	): void;
}

interface ICustomAutoCompleteProps {
	selectRef: TRef;
	error: boolean;
	errorText: string;
	required?: boolean;
	options: Country[] | Region[] | Province[] | District[];
	noOptionsText?: string;
	label: string;
	getOptionLabel: (option: unknown) => string;
	isOptionEqualToValue: (option: unknown, value: unknown) => boolean;
	name: string;
	value: Country | Region | Province | District;
	onChange: FormChangeAutocompleteEvent;
	onInputChange?: FormChangeAutocompleteEvent;
	onBlur: FormBlurEvent;
}

const CustomAutocomplete: React.FC<ICustomAutoCompleteProps> = (props) => {
	const {
		selectRef,
		error,
		errorText,
		required,
		options,
		noOptionsText,
		label,
		getOptionLabel,
		isOptionEqualToValue,
		name,
		value,
		onChange,
		onInputChange,
		onBlur,
	} = props;
	return (
		<FormControl
			error={error}
			sx={{
				width: 1,
			}}
		>
			<Autocomplete
				PopperComponent={StyledPopper}
				ListboxComponent={
					ListboxComponent as JSXElementConstructor<HtmlHTMLAttributes<HTMLElement>>
				}
				options={options as Array<Country | Region | Province | District>}
				noOptionsText={noOptionsText}
				value={value || null}
				onChange={
					onChange as (
						event: React.SyntheticEvent<Element, Event>,
						newValue: Country | Region | Province | District,
					) => void
				}
				onInputChange={
					onInputChange as (event: React.SyntheticEvent<Element, Event>, value: string) => void
				}
				onBlur={onBlur as any as React.FocusEventHandler<HTMLDivElement>} // TODO: Check all any used and refactor
				autoHighlight
				openOnFocus={false}
				getOptionLabel={getOptionLabel}
				isOptionEqualToValue={isOptionEqualToValue}
				renderOption={(props, option) => [props, option] as React.ReactNode}
				renderInput={(params) => {
					return (
						<TextField
							{...params}
							inputRef={selectRef}
							name={name}
							error={error}
							required={required}
							label={label}
							inputProps={{
								...params.inputProps,
								autoComplete: "new-password", // disable autocomplete and autofill
							}}
						/>
					);
				}}
			/>
			<FormHelperText>{errorText}</FormHelperText>
		</FormControl>
	);
};

export default CustomAutocomplete;
