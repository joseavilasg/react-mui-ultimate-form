import decodeUnscapedUri from "@/hooks/form/utils/decodeUnscapedUri";

import countryRegionData from "./CustomSelectField/country-region-data-es";
import {
	Country,
	District,
	ICountry,
	IDistrict,
	IProvince,
	IRegion,
	Province,
	Region,
} from "./CustomSelectField/helpers";

export const getRegionsByCountry = (country: ICountry, isNativeSelect: boolean): IRegion[] => {
	const regions = (() => {
		if (isNativeSelect) {
			return countryRegionData.find((c) => c.countryShortCode === country)?.regions || [];
		}

		return (
			countryRegionData.find(
				(c) => c.countryShortCode === (country as Country)?.countryShortCode,
			)?.regions || ([] as IRegion[])
		);
	})();
	return regions.map((region: IRegion) => {
		const text = (region as Region)?.name || "";
		return {
			...(region as Region),
			name: decodeUnscapedUri(text),
		};
	});
};
export const getProvincesByRegion = (
	region: IRegion,
	regions: IRegion[],
	isNativeSelect: boolean,
): IProvince[] => {
	const provinces = (() => {
		if (isNativeSelect) {
			return (
				(regions.find((r) => (r as Region)?.shortCode == region) as Region)?.provinces || []
			);
		}

		return (
			(regions.find((r) => (r as Region)?.shortCode == (region as Region)?.shortCode) as Region)
				?.provinces || []
		);
	})();
	return provinces.map((province: IProvince) => {
		const text = (province as Province)?.name || "";
		return {
			...(province as Province),
			name: decodeUnscapedUri(text),
		} as IProvince;
	});
};
export const getDistrictsByProvince = (
	province: IProvince,
	provinces: IProvince[],
	isNativeSelect: boolean,
): IDistrict[] => {
	const districts = (() => {
		if (isNativeSelect) {
			return (
				(provinces.find((r) => (r as Province)?.name === province) as Province)?.districts || []
			);
		}

		return (
			(provinces.find((r) => (r as Province)?.name == (province as Province)?.name) as Province)
				?.districts || []
		);
	})();
	return districts.map((province: IDistrict) => {
		const text = (province as District)?.name || "";
		return {
			name: decodeUnscapedUri(text),
		};
	});
};
