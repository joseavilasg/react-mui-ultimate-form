import { useEffect, useState } from "react";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
	FormControl,
	FormHelperText,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
} from "@mui/material";

import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";

import useFocus from "@/hooks/form/hooks/useFocus";

const PasswordField = <T extends string>({
	name,
	label,
	disabled = false,
	field,
	shouldFocus,
}: TGenericNormalFieldProps<T>) => {
	const [showPassword, setShowPassword] = useState(false);
	const { ref, error, errorText, required, value, handleBlur, handleChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	const handleClickShowPassword = () => setShowPassword((show) => !show);

	const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
		event.preventDefault();
	};

	return (
		<FormControl fullWidth variant="outlined" disabled={disabled}>
			<InputLabel error={error as boolean} htmlFor="outlined-adornment-password">
				Password
			</InputLabel>
			<OutlinedInput
				disabled={disabled}
				id="outlined-adornment-password"
				inputRef={ref}
				error={error as boolean}
				required={required as boolean}
				name={name}
				label={label}
				type={showPassword ? "text" : "password"}
				value={value.data}
				onChange={handleChange as any}
				onBlur={handleBlur as any}
				endAdornment={
					<InputAdornment position="end" sx={{ color: "inherit" }}>
						<IconButton
							aria-label="toggle password visibility"
							color="inherit"
							onClick={handleClickShowPassword}
							onMouseDown={handleMouseDownPassword}
							edge="end"
						>
							{showPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					</InputAdornment>
				}
			/>
			<FormHelperText error={error as boolean} sx={{ ml: 1 }}>
				{errorText}
			</FormHelperText>
		</FormControl>
	);
};

export default PasswordField;
