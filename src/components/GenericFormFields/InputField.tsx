import { useEffect } from "react";
import { TextField } from "@mui/material";

import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";

import useFocus from "@/hooks/form/hooks/useFocus";

const InputField = <T extends string>({
	name,
	label,
	disabled = false,
	field,
	shouldFocus,
}: TGenericNormalFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	return (
		<TextField
			inputRef={ref}
			// autoFocus
			disabled={disabled}
			error={error as boolean}
			helperText={errorText}
			name={name}
			color="primary"
			required={required as boolean}
			label={label}
			variant="outlined"
			value={value.data}
			sx={{
				width: 1,
			}}
			onChange={handleChange as any}
			onBlur={handleBlur as any}
		/>
	);
};

export default InputField;
