import { useEffect } from "react";
import { KEYS } from "@/const";
import { TextField } from "@mui/material";

import { TGenericNormalFieldProps } from "@/types/genericFormFieldHook";

import useFocus from "@/hooks/form/hooks/useFocus";

const MultilineInputField = <T extends string>({
	name,
	label,
	disabled = false,
	field,
	shouldFocus,
}: TGenericNormalFieldProps<T>) => {
	const { ref, error, errorText, required, value, handleBlur, handleChange } = field;

	const [setFocus] = useFocus(ref);

	useEffect(() => {
		if (shouldFocus) {
			setFocus();
		}
	}, [shouldFocus]);

	const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
		if (e.key === KEYS.ENTER && !e.altKey && !e.shiftKey) {
			e.preventDefault();
			const formEl = e.currentTarget.form;
			formEl?.requestSubmit();
		}
		if (e.key === KEYS.ENTER && (e.altKey || e.shiftKey)) {
			e.preventDefault();
			const customInputEvent = {
				target: {
					name,
					value: value + "\n",
				},
			} as unknown as React.ChangeEvent<HTMLInputElement>;
			handleChange(customInputEvent as any);
		}
	};

	return (
		<TextField
			inputRef={ref}
			// autoFocus
			disabled={disabled}
			error={error as boolean}
			helperText={errorText}
			name={name}
			color="primary"
			required={required as boolean}
			label={label}
			variant="outlined"
			value={value.data}
			multiline
			minRows={1}
			maxRows={6}
			fullWidth
			sx={{
				".MuiOutlinedInput-root": {
					// pt: 1,
					pb: 3,
					// py: 0,
					boxSizing: "border-box",
				},
			}}
			onChange={handleChange as any}
			onBlur={handleBlur as any}
			inputProps={{ onKeyDown: handleKeyDown }}
		/>
	);
};

export default MultilineInputField;
