import {
	TCheckboxGroupFieldProps,
	TNormalFieldProps,
	TRadioButtonsGroupFieldProps,
	TSelectFieldProps,
} from "./components";
import { TUseFormFieldResult } from "./formFieldHook";
import { TFormDef, TFormPersistedValuesDef } from "./formModel";

export type UseGenericFormFieldsProps<F extends string, T extends string> = {
	formModel: TFormDef<F, T>;
	// usePersistedValuesContext: () => TPersistedValuesContext<FF>;
	// activeStep?: number;
	autoFocusOnRender?: boolean;
	persistedValues?: TFormPersistedValuesDef<T>;
	// lang?: "es" | "en";
};

export type TGenericNormalFieldProps<
	T extends string,
	HidePV extends boolean = true,
> = (HidePV extends true ? Omit<TNormalFieldProps<T>, "persistedValue"> : TNormalFieldProps<T>) & {
	field: TUseFormFieldResult<T>;
	shouldFocus: boolean;
};
export type TGenericSelectFieldProps<
	T extends string,
	HidePV extends boolean = true,
> = (HidePV extends true ? Omit<TSelectFieldProps<T>, "persistedValue"> : TSelectFieldProps<T>) & {
	field: TUseFormFieldResult<T>;
	shouldFocus: boolean;
};
export type TGenericRadioButtonsGroupFieldProps<
	T extends string,
	HidePV extends boolean = true,
> = (HidePV extends true
	? Omit<TRadioButtonsGroupFieldProps<T>, "persistedValue">
	: TRadioButtonsGroupFieldProps<T>) & {
	field: TUseFormFieldResult<T>;
	shouldFocus: boolean;
};
export type TGenericCheckboxGroupFieldProps<
	T extends string,
	HidePV extends boolean = true,
> = (HidePV extends true
	? Omit<TCheckboxGroupFieldProps<T>, "persistedValue">
	: TCheckboxGroupFieldProps<T>) & {
	field: TUseFormFieldResult<T>;
	shouldFocus: boolean;
};
