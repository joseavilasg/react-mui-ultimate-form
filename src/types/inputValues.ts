import { Dayjs } from "dayjs";

export type NormalInputValue = string;
export type MultilineInputValue = string;
export type DateInputValue = Dayjs | string;
export type NormalSelectInputValue = string | number;
export type RadioGroupInputValue = string;
export type MultipleSelectInputValue = NormalSelectInputValue[];
export type SelectInputValue = NormalSelectInputValue | MultipleSelectInputValue;
export type EmailInputValue = {
	value: string;
	isCodeSent: boolean;
	isCodeForwarded: boolean;
	isVerified: boolean;
};
export type VerificationCodeInputValue = string[];
export type CheckboxInputValue = boolean;
export type CheckboxGroupInputValue = NormalSelectInputValue[]; // String array will represent values, just like MultipleSelect works
export type ValueTypes =
	| NormalInputValue
	| MultilineInputValue
	| DateInputValue
	| NormalSelectInputValue
	| RadioGroupInputValue
	| MultipleSelectInputValue
	| SelectInputValue
	| EmailInputValue
	| VerificationCodeInputValue
	| CheckboxInputValue
	| CheckboxGroupInputValue;
