import {
	FormBlurEvent,
	FormChangeEvent,
	FormCheckboxGroupChangeEvent,
	FormInputChangeEvent,
	FormPhoneChangeEvent,
	FormSubmitEvent,
	FormVerificationCodeBlurEvent,
	FormVerificationCodeChangeEvent,
	FormVerificationCodeKeyDownEvent,
} from "./events";
import { TFieldsDefOut, TFormDef, TFormValuesDef } from "./formModel";
import { TRef } from "./refs";

export type TUseFormFieldParams<F extends string, T extends string> = {
	formModel: TFormDef<F, T>;
	fieldName: T;
	persistedValue?: Partial<TFormValuesDef<T>>[T] | TFormValuesDef<T>[T];
};

// type TUseFormField = {
// 	<F extends string, T extends string>(params: TUseFormFieldParams<F, T>): TUseFormFieldResult<T>;
// };

export type TUseFormFieldResult<T extends string> = {
	isLoading: boolean;
	field: TFieldsDefOut<T>[T];
	value: TFormValuesDef<T>[T];
	required: boolean;
	ref: TRef;
	error: boolean | boolean[];
	errorText: string;
	handleChange: FormChangeEvent<T>;
	handlePhoneChange: FormPhoneChangeEvent<T>;
	handleVerificationCodeChange: FormVerificationCodeChangeEvent;
	handleCheckBoxChange: FormInputChangeEvent<T>;
	handleRadioButtonChange: FormChangeEvent<T>;
	handleCheckboxGroupChange: FormCheckboxGroupChangeEvent<T>;
	handleBlur: FormBlurEvent<T>;
	handleVerificationCodeBlur: FormVerificationCodeBlurEvent;
	handleVerificationCodeKeyDown: FormVerificationCodeKeyDownEvent;
	handleSubmit: FormSubmitEvent<T>;
};
