import { TErrors } from "./errors";
import {
	FormBlurEvent,
	FormChangeEvent,
	FormCheckboxGroupChangeEvent,
	FormInputChangeEvent,
	FormPhoneChangeEvent,
	FormSubmitEvent,
	FormVerificationCodeBlurEvent,
	FormVerificationCodeChangeEvent,
	FormVerificationCodeKeyDownEvent,
} from "./events";
import { StepType, TFormDef, TFormFieldsDefStepOut, TFormValuesDef } from "./formModel";
import { TLanguage } from "./language";
import { TGetRef } from "./refs";

export type TUseControlledFormParams<F extends string, T extends string> = {
	formModel: TFormDef<F, T>;
	activeStep?: number;
	lang?: TLanguage;
	// usePersistedValuesContext?: () => TPersistedValuesContext<T>;
};

export type TUseControlledFormResult<T extends string> = {
	stepInfo: {
		stepId: string;
		minTimeMs: number;
		stepType: StepType;
		stepsLength: number;
	};
	values: TFormValuesDef<T>;
	getRef: TGetRef<T>;
	errors: TErrors<TFormValuesDef<T>>;
	// model: TFormDef<F, T>["formFields"]; // Model from active step
	// model: TFormFieldsStepInformativeDef | TFormFieldsStepConstructiveDef<T>; // Model from active step
	model: TFormFieldsDefStepOut<T>;
	handleChange: FormChangeEvent<T>;
	handlePhoneChange: FormPhoneChangeEvent<T>;
	handleVerificationCodeChange: FormVerificationCodeChangeEvent;
	handleCheckBoxChange: FormInputChangeEvent<T>;
	handleRadioButtonChange: FormChangeEvent<T>;
	handleCheckboxGroupChange: FormCheckboxGroupChangeEvent<T>;
	handleBlur: FormBlurEvent<T>;
	handleVerificationCodeBlur: FormVerificationCodeBlurEvent;
	handleVerificationCodeKeyDown: FormVerificationCodeKeyDownEvent;
	handleSubmit: FormSubmitEvent<T>;
};
