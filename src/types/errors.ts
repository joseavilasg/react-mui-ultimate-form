export type TNormalError = string[];
export type TVerificationCodeError = { message: string; errors: boolean[] };
export type TErrors<T> = Record<keyof T, TNormalError | TVerificationCodeError>;

export type TErrorsCustomSetter<T> = {
	(val: TErrors<T>): TErrors<T>;
};
