import { StepsCompleted } from "@/zustand/slices/formsStateSlice";

import { StepType, TFormDef } from "./formModel";

export type TUseFormStepParams<F extends string, T extends string> = {
	formModel: TFormDef<F, T>;
};

export type StepSetStateActionFunctionParams = {
	activeStep: number;
	stepType: StepType;
	stepId: string;
	stepsLength: number;
	minTimeMs: number;
	stepsCompleted: StepsCompleted;
};
export type StepSetStateActionFunctionResult =
	| {
			activeStep?: number;
			stepsCompleted?: StepsCompleted;
	  }
	| undefined;
export type StepSetStateActionFunction = {
	(state: StepSetStateActionFunctionParams): StepSetStateActionFunctionResult;
};
export type StepSetStateActionData = {
	activeStep: number;
	stepsCompleted: StepsCompleted;
};

export type StepSetStateAction = StepSetStateActionFunction | StepSetStateActionData;

export type TUseFormStepResult = {
	activeStep: number;
	stepType: StepType;
	stepId: string;
	stepsLength: number;
	minTimeMs: number;
	stepsCompleted: StepsCompleted;
	setActiveStep: React.Dispatch<React.SetStateAction<number>>;
	stepDispatch: React.Dispatch<StepSetStateAction>;
};
