import { SxProps, Theme } from "@mui/material";

import { LabelType } from "@/components/GenericFormFields/CustomFields/CheckboxGroupMUI";

import { TFormPersistedValuesDef } from "./formModel";
import { TGenericNormalFieldProps } from "./genericFormFieldHook";
import {
	TCheckboxGroupOptions,
	TCheckboxGroupOptionsFunct,
	TRadioGroupOptions,
	TRadioGroupOptionsFunct,
	TSelectOptions,
	TSelectOptionsFunct,
} from "./options";

type TBaseFormField<T extends string = string, I extends string = string> = {
	name: T;
	label: string;
	type: I;
};

type TNormalFormField<T extends string, I extends string = string> = TBaseFormField<T, I>;
type TSelectFormField<
	T extends string,
	I extends string = string,
	HideFuncRes extends boolean = false,
> = TNormalFormField<T, I> & {
	options?: HideFuncRes extends true ? TSelectOptions : TSelectOptions | TSelectOptionsFunct;
};
type TRadioButtonsGroupFormField<
	T extends string,
	I extends string = string,
	HideFuncRes extends boolean = false,
> = TNormalFormField<T, I> & {
	options?: HideFuncRes extends true
		? TRadioGroupOptions
		: TRadioGroupOptions | TRadioGroupOptionsFunct;
};
type TCheckboxGroupFormField<
	T extends string,
	I extends string = string,
	HideFuncRes extends boolean = false,
> = TNormalFormField<T, I> & {
	options?: HideFuncRes extends true
		? TCheckboxGroupOptions
		: TCheckboxGroupOptions | TCheckboxGroupOptionsFunct;
};

export type TNormalFieldProps<T extends string> = Omit<TNormalFormField<T>, "type"> & {
	persistedValue?: TFormPersistedValuesDef<T>[T];
} & {
	disabled?: boolean;
};
export type TSelectFieldProps<T extends string> = Omit<TSelectFormField<T>, "type"> & {
	persistedValue?: TFormPersistedValuesDef<T>[T];
} & {
	disabled?: boolean;
	renderedValueSeparator?: string;
	disableLazyLoadOptions?: boolean;
};
export type TRadioButtonsGroupFieldProps<T extends string> = Omit<
	TRadioButtonsGroupFormField<T>,
	"type"
> & {
	persistedValue?: TFormPersistedValuesDef<T>[T];
} & {
	disabled?: boolean;
	row?: boolean;
	standalone?: boolean;
	labelType?: LabelType;
	sx?: SxProps<Theme>;
};
export type TCheckboxGroupFieldProps<T extends string> = Omit<
	TCheckboxGroupFormField<T>,
	"type"
> & {
	persistedValue?: TFormPersistedValuesDef<T>[T];
} & {
	disabled?: boolean;
	row?: boolean;
	standalone?: boolean;
	labelType?: LabelType;
	sx?: SxProps<Theme>;
};
export type TVerifyInputMUIProps<T extends string> = TGenericNormalFieldProps<T, false> & {
	sx?: Record<string, unknown>;
	showLabel?: boolean;
	showHelperText?: boolean;
};
