export type TCountry = {
	value: string;
	name: string;
	name_unicode: string;
};
