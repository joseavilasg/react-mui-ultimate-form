import { FormSubmitEvent } from "./events";
import { StepType, TFormDef, TFormValuesDef } from "./formModel";

export type TUseFormValuesParams<F extends string, T extends string> = {
	formModel: TFormDef<F, T>;
};
export type TUseFormValuesResult<T extends string> = {
	isLoading: boolean;
	values: TFormValuesDef<T>;
	handleSubmit: FormSubmitEvent<T>;
	stepInfo: {
		activeStep: number;
		stepType: StepType;
		stepId: string;
		stepsLength: number;
		minTimeMs: number;
	};
};
