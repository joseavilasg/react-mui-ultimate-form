import { NormalInputValue, NormalSelectInputValue, RadioGroupInputValue } from "./inputValues";

export type TSelectOption = { value: NormalSelectInputValue; name?: string };
export type TRadioGroupOption = { value: RadioGroupInputValue; name?: string };
export type TCheckboxGroupOption = { value: NormalInputValue; name?: string };
export type TSelectOptions = TSelectOption[];
export type TRadioGroupOptions = TRadioGroupOption[];
export type TCheckboxGroupOptions = TCheckboxGroupOption[];
export type TSelectOptionsFunct = () => Promise<TSelectOptions | void>;
export type TRadioGroupOptionsFunct = () => Promise<TRadioGroupOptions | void>;
export type TCheckboxGroupOptionsFunct = () => Promise<TCheckboxGroupOptions | void>;
