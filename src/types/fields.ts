import {
	CheckboxGroupInputValue,
	CheckboxInputValue,
	DateInputValue,
	EmailInputValue,
	MultilineInputValue,
	MultipleSelectInputValue,
	NormalInputValue,
	NormalSelectInputValue,
	RadioGroupInputValue,
	VerificationCodeInputValue,
} from "./inputValues";
import { TCheckboxGroupOptions, TRadioGroupOptions, TSelectOptions } from "./options";
import { TValidationGroup } from "./validations";

export type TNormalField<T extends string> = {
	name: T;
	label: string;
	type: "NormalInputValue";
	validations: TValidationGroup<NormalInputValue>;
	initialValue: NormalInputValue;
};
export type TMultilineField<T extends string> = {
	name: T;
	label: string;
	type: "MultilineInputValue";
	validations: TValidationGroup<MultilineInputValue>;
	initialValue: MultilineInputValue;
};
export type TVerificationCodeField<T extends string> = {
	name: T;
	label: string;
	type: "VerificationCodeInputValue";
	validations: TValidationGroup<VerificationCodeInputValue>;
	initialValue: VerificationCodeInputValue;
};
export type TCheckboxField<T extends string> = {
	name: T;
	label: string;
	type: "CheckboxInputValue";
	validations: TValidationGroup<CheckboxInputValue>;
	initialValue: CheckboxInputValue;
};
export type TEmailField<T extends string> = {
	name: T;
	label: string;
	type: "EmailInputValue";
	validations: TValidationGroup<EmailInputValue>;
	initialValue: EmailInputValue;
};
export type TDateField<T extends string> = {
	name: T;
	label: string;
	type: "DateInputValue";
	validations: TValidationGroup<DateInputValue>;
	initialValue: DateInputValue;
};
export type TCountryRegionSelectField<T extends string> = {
	name: T;
	label: string;
	type: "CountryRegionSelectField";
	validations: TValidationGroup<NormalSelectInputValue>;
	initialValue: NormalSelectInputValue;
};
export type TNormalSelectField<T extends string> = {
	name: T;
	label: string;
	type: "NormalSelectInputValue";
	options: TSelectOptions;
	validations: TValidationGroup<NormalSelectInputValue>;
	initialValue: NormalSelectInputValue;
};
export type TMultipleSelectField<T extends string> = {
	name: T;
	label: string;
	type: "MultipleSelectInputValue";
	options: TSelectOptions;
	validations: TValidationGroup<MultipleSelectInputValue>;
	initialValue: MultipleSelectInputValue;
};
export type TRadioGroupField<T extends string> = {
	name: T;
	label: string;
	type: "RadioGroupInputValue";
	options: TRadioGroupOptions;
	validations: TValidationGroup<RadioGroupInputValue>;
	initialValue: RadioGroupInputValue;
};

export type TCheckboxGroupField<T extends string> = {
	name: T;
	label: string;
	type: "CheckboxGroupInputValue";
	options: TCheckboxGroupOptions;
	validations: TValidationGroup<CheckboxGroupInputValue>;
	initialValue: CheckboxGroupInputValue;
};
export type TCustomField<T extends string> = {
	name: T;
	label: string;
	type: `${string}CustomField`;
	validations: TValidationGroup<any>;
	initialValue: any;
};

export type FieldTypes =
	| "NormalInputValue"
	| "MultilineInputValue"
	| "VerificationCodeInputValue"
	| "CheckboxInputValue"
	| "EmailInputValue"
	| "DateInputValue"
	| "CountryRegionSelectField"
	| "NormalSelectInputValue"
	| "MultipleSelectInputValue"
	| "RadioGroupInputValue"
	| "CheckboxGroupInputValue"
	| `${string}CustomField`;

export type DefaultInitialValues = {
	NormalInputValue: NormalInputValue;
	MultilineInputValue: MultilineInputValue;
	VerificationCodeInputValue: VerificationCodeInputValue;
	CheckboxInputValue: CheckboxInputValue;
	EmailInputValue: EmailInputValue;
	DateInputValue: DateInputValue;
	CountryRegionSelectField: NormalSelectInputValue;
	NormalSelectInputValue: NormalSelectInputValue;
	MultipleSelectInputValue: MultipleSelectInputValue;
	RadioGroupInputValue: RadioGroupInputValue;
	CheckboxGroupInputValue: CheckboxGroupInputValue;
	// [key: `${string}CustomField`]: any;
	CustomField: any;
};

export type InitialValues =
	| NormalInputValue
	| MultilineInputValue
	| VerificationCodeInputValue
	| CheckboxInputValue
	| EmailInputValue
	| DateInputValue
	| NormalSelectInputValue
	| NormalSelectInputValue
	| MultipleSelectInputValue
	| RadioGroupInputValue
	| CheckboxGroupInputValue;
