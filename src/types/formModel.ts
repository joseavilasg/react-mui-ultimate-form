import {
	FieldTypes,
	TCheckboxField,
	TCheckboxGroupField,
	TCountryRegionSelectField,
	TCustomField,
	TDateField,
	TEmailField,
	TMultilineField,
	TMultipleSelectField,
	TNormalField,
	TNormalSelectField,
	TRadioGroupField,
	TVerificationCodeField,
} from "./fields";
import {
	CheckboxGroupInputValue,
	CheckboxInputValue,
	DateInputValue,
	EmailInputValue,
	MultilineInputValue,
	MultipleSelectInputValue,
	NormalInputValue,
	NormalSelectInputValue,
	RadioGroupInputValue,
	VerificationCodeInputValue,
} from "./inputValues";

export type TFieldDef<T extends string> =
	| TNormalField<T>
	| TMultilineField<T>
	| TVerificationCodeField<T>
	| TCheckboxField<T>
	| TEmailField<T>
	| TDateField<T>
	| TNormalSelectField<T>
	| TMultipleSelectField<T>
	| TRadioGroupField<T>
	| TCountryRegionSelectField<T>
	| TCheckboxGroupField<T>
	| TCustomField<T>;

export type TFieldDefOut<T extends string> = {
	name: T;
	label: string;
	type: FieldTypes;
};

export type TFieldsDef<T extends string = string> = {
	[K in T]: TFieldDef<K>;
};
export type TFieldsDefOut<T extends string> = {
	[K in T]: TFieldDefOut<K>;
};
export type StepType = "Informative" | "Constructive";
export type TFormFieldsStepInformativeDef = {
	stepId?: string;
	stepType: "Informative";
	minTimeMs?: number;
	title: string;
};
export type TFormFieldsStepConstructiveDef<T extends string> = {
	stepId?: string;
	stepType: "Constructive";
	minTimeMs?: number;
	title?: string;
	fields: Partial<TFieldsDef<T>>;
};
export type TFormFieldsDefStepOut<T extends string> = {
	stepId: string;
	stepType: StepType;
	minTimeMs: number;
	title: string;
	fields: TFieldsDefOut<T>;
};
export type TFormDef<F extends string, T extends string> = {
	formId: F;
	// formFields: TFieldsDef<T> | [TFieldsDef<T>, ...TFieldsDef<T>[]]; // Array form multistep forms
	steps: (TFormFieldsStepInformativeDef | TFormFieldsStepConstructiveDef<T>)[];
};
export type TFormDefOut<F extends string, T extends string> = {
	formId: F;
	steps: TFormFieldsDefStepOut<T>;
};

export type TFormsDef<F extends string, T extends string> = {
	[K in F]: TFormDef<K, T>;
};
export type TFormsDefOut<F extends string, T extends string> = {
	[K in F]: TFormDefOut<K, T>;
};

// function buildFields<T extends string>(fields: TFieldsDef<T>): TFieldsDef<T> {
// 	return fields;
// }

export type IntersectionOf<T extends any[]> = T extends [infer U, ...infer Rest]
	? U & IntersectionOf<Rest>
	: unknown;

export type NormalValueDef = {
	type: "NormalInputValue";
	data: NormalInputValue;
};
export type MultilineValueDef = {
	type: "MultilineInputValue";
	data: MultilineInputValue;
};
export type VerificationCodeValueDef = {
	type: "VerificationCodeInputValue";
	data: VerificationCodeInputValue;
};
export type CheckboxValueDef = {
	type: "CheckboxInputValue";
	data: CheckboxInputValue;
};
export type EmailValueDef = {
	type: "EmailInputValue";
	data: EmailInputValue;
};
export type DateValueDef = {
	type: "DateInputValue";
	data: DateInputValue;
};
export type CountryRegionSelectValueDef = {
	type: "CountryRegionSelectField";
	data: NormalSelectInputValue;
};
export type NormalSelectValueDef = {
	type: "NormalSelectInputValue";
	data: NormalSelectInputValue;
};
export type MultipleSelectValueDef = {
	type: "MultipleSelectInputValue";
	data: MultipleSelectInputValue;
};
export type RadioGroupValueDef = {
	type: "RadioGroupInputValue";
	data: RadioGroupInputValue;
};

export type CheckboxGroupValueDef = {
	type: "CheckboxGroupInputValue";
	data: CheckboxGroupInputValue;
};
export type CustomValueDef = {
	type: `${string}CustomField`;
	data: unknown;
};

export type TFormValueDef =
	| NormalValueDef
	| MultilineValueDef
	| VerificationCodeValueDef
	| CheckboxValueDef
	| EmailValueDef
	| DateValueDef
	| CountryRegionSelectValueDef
	| NormalSelectValueDef
	| MultipleSelectValueDef
	| RadioGroupValueDef
	| CheckboxGroupValueDef
	| CustomValueDef;
export type TFormValuesDef<T extends string> = {
	[K in keyof TFieldsDef<T>]: TFormValueDef;
};
export type TFormPersistedValuesDef<T extends string> = {
	[K in keyof TFieldsDef<T>]?: TFieldsDef<T>[K]["initialValue"];
};
export type TFormTypeInputDef<T extends string> = {
	[K in keyof TFieldsDef<T>]: TFieldsDef<T>[K]["type"];
};
