import { CheckboxGroupInputValue } from "./inputValues";

// useGetRef interfaces and types
export type TGetRef<T extends string | number | symbol = string> = {
	(idx: T): TRef;
};

type HTMLPhoneInputElement = HTMLInputElement & {
	props: {
		value: string;
	};
	numberInputRef: HTMLInputElement;
};

type NormalTRefCurrent = HTMLInputElement;
type PhoneRefCurrent = HTMLPhoneInputElement;
type SelectTRefCurrent = HTMLInputElement & {
	openSelect: () => void;
	closeSelect: () => void;
};
type RadioGroupTRefCurrent = HTMLDivElement & {
	value: string | number;
	focus: () => void;
};
type CheckboxGroupTRefCurrent = HTMLDivElement & {
	value: CheckboxGroupInputValue;
	focus: () => void;
};
type CustomTRefCurrent = HTMLDivElement & {
	value: any;
	focus: () => void;
};
type RecordOfTRefCurrent = Record<number, NormalTRef | SelectTRef>;

export type TRefCurrent<
	T extends
		| "normal"
		| "phone"
		| "select"
		| "radioGroup"
		| "checkboxGroup"
		| "record"
		| "custom" = "normal",
> = T extends "normal"
	? NormalTRefCurrent
	: T extends "phone"
	? PhoneRefCurrent
	: T extends "select"
	? SelectTRefCurrent
	: T extends "radioGroup"
	? RadioGroupTRefCurrent
	: T extends "checkboxGroup"
	? CheckboxGroupTRefCurrent
	: T extends "record"
	? RecordOfTRefCurrent
	: T extends "custom"
	? CustomTRefCurrent
	: never;
export type NormalTRef = React.MutableRefObject<TRefCurrent<"normal"> | null>;
type PhoneTRef = React.MutableRefObject<TRefCurrent<"phone"> | null>;
export type SelectTRef = React.MutableRefObject<TRefCurrent<"select"> | null>;
export type RadioGroupTRef = React.MutableRefObject<TRefCurrent<"radioGroup"> | null>;
export type CheckboxGroupTRef = React.MutableRefObject<TRefCurrent<"checkboxGroup"> | null>;
export type CustomTRef = React.MutableRefObject<TRefCurrent<"custom"> | null>;
type RecordOfTRef = React.MutableRefObject<TRefCurrent<"record"> | null>;
export type TRefObject = Record<string, TRef>;
// type TRefArray = TRef[];

export type TRef =
	| NormalTRef
	| PhoneTRef
	| SelectTRef
	| RadioGroupTRef
	| CheckboxGroupTRef
	| CustomTRef
	| RecordOfTRef;
export type TRefs = React.MutableRefObject<TRefObject>;
