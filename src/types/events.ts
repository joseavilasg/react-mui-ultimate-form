import { SyntheticEvent } from "react";
import { CountryName } from "react-mui-phone-input";

import { StepType, TFormValuesDef } from "./formModel";
import {
	CheckboxGroupInputValue,
	MultipleSelectInputValue,
	NormalSelectInputValue,
	SelectInputValue,
} from "./inputValues";

export type SelectChangeEvent<T extends SelectInputValue = SelectInputValue> =
	| (Event & { target: { value: T; name?: string } })
	| React.ChangeEvent<HTMLInputElement>;

export type CustomTarget<T extends string = string> = Omit<EventTarget, "name"> & {
	name: T;
};
export interface CustomFocusEvent<
	Target = Element,
	RelatedTarget = Element,
	N extends string = string,
> extends SyntheticEvent<Target, FocusEvent> {
	relatedTarget: (EventTarget & RelatedTarget) | null;
	target: Omit<Target, "name"> & CustomTarget<N>;
}
export interface CustomChangeEvent<T = Element, N extends string = string>
	extends SyntheticEvent<T> {
	target: Omit<T, "name"> & CustomTarget<N>;
}
export interface CustomChangeEventAndValue<T = Element, N extends string = string, Value = string>
	extends SyntheticEvent<T> {
	target: Omit<T, "name" | "value"> & CustomTarget<N> & { value: Value };
}

export type CheckboxGroupChangeEvent<T extends string = string> = {
	target: { name: T; value: CheckboxGroupInputValue };
};
export type CustomFieldChangeEvent<T extends string = string, V = any> = {
	target: { name: T; value: V };
};

export type FormCustomChangeEvent<T extends string = string, V = any> = (
	e: CustomFieldChangeEvent<T, V>,
) => void;

export type FormPhoneChangeEvent<T extends string = string> = (
	value: string,
	selectedCountry: CountryName,
	event: CustomChangeEvent<HTMLInputElement, T>,
	formattedValue: string,
) => void;
export type FormVerificationCodeChangeEvent = (e: React.ChangeEvent<HTMLInputElement>) => void;

//  export type FormSelectMUIChangeEvent = (e: SelectChangeEvent<{ value: unknown }>) => void;
export type FormNormalSelectMUIChangeEvent<
	T extends NormalSelectInputValue = NormalSelectInputValue,
> = (e: SelectChangeEvent<T>) => void;
export type FormMultipleSelectMUIChangeEvent<
	T extends MultipleSelectInputValue = MultipleSelectInputValue,
> = (e: SelectChangeEvent<T>) => void;
export type FormNormalSelectChangeEvent = (e: React.ChangeEvent<HTMLSelectElement>) => void;

export type FormInputChangeEvent<T extends string = string> = (
	e: CustomChangeEvent<HTMLInputElement, T>,
) => void;
export type FormChangeEvent<T extends string = string> = (
	e: CustomChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement, T>,
) => void;
export type FormCheckboxGroupChangeEvent<T extends string = string> = (
	e: CustomChangeEventAndValue<HTMLInputElement, T, CheckboxGroupInputValue>,
) => void;

export type FormButtonBlurEvent = (e: React.FocusEvent<HTMLButtonElement>) => void;
export type FormDivBlurEvent = (e: React.FocusEvent<HTMLDivElement>) => void;

export type FormBlurEvent<T extends string = string> = (
	e: CustomFocusEvent<
		HTMLTextAreaElement | HTMLInputElement,
		HTMLTextAreaElement | HTMLInputElement,
		T
	>,
) => void;
export type FormVerificationCodeBlurEvent = (
	e: React.FocusEvent<HTMLTextAreaElement | HTMLInputElement | HTMLButtonElement>,
) => void;
export type CustomKeyboardEvent = React.KeyboardEvent<HTMLInputElement> & {
	keyCode: number;
	target: {
		addEventListener(
			type: string,
			callback: EventListenerOrEventListenerObject | null,
			options?: AddEventListenerOptions | boolean,
		): void;
		/** Dispatches a synthetic event event to target and returns true if either event's cancelable attribute value is false or its preventDefault() method was not invoked, and false otherwise. */
		dispatchEvent(event: Event): boolean;
		/** Removes the event listener in target's event listener list with the same type, callback, and options. */
		removeEventListener(
			type: string,
			callback: EventListenerOrEventListenerObject | null,
			options?: EventListenerOptions | boolean,
		): void;
		dataset: {
			id: string;
		};
	};
};
export type FormVerificationCodeKeyDownEvent = (e: CustomKeyboardEvent) => void;
export type CustomHandleSubmit<T extends string> = {
	(params: {
		values: TFormValuesDef<T>;
		stepInfo: {
			stepsLength: number;
			activeStep: number;
			setActiveStep: React.Dispatch<React.SetStateAction<number>>;
			stepId: string;
			minTimeMs: number;
			stepType: StepType;
		};
	}): void;
};
export type FormSubmitEvent<T extends string> = (
	customHandleSubmit: CustomHandleSubmit<T>,
	toastId: string,
	step?: {
		setActiveStep: React.Dispatch<React.SetStateAction<number>>;
		activeStep: number;
	},
) => void;
