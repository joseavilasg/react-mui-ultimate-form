import { TFormDef } from "./formModel";

export type TManualFormSetter<F extends string, T extends string> = {
	formModel: TFormDef<F, T>;
	// field: T;
};
