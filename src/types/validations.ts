import { CustomChangeEvent, CustomChangeEventAndValue } from "./events";

// type TFormValidations<T> = Record<keyof T, TValidationGroup>;

export type TCustomValidationNormal = [boolean, string];
type TCustomValidationDate = [Date | string, string];
type TValidationVerificationCode = number;
type TValidationNormal = boolean | TCustomValidationNormal;
type TValidationDate = Date | string | TCustomValidationDate;

export type TValidationGroupKeys = keyof TValidationGroup;
export type TValidationGroup<Value = any> = {
	isRequired: TValidationNormal;
	isPhoneNumber?: TValidationNormal;
	isNumber?: TValidationNormal;
	isSlug?: TValidationNormal;
	isEmail?: TValidationNormal;
	isNotEmail?: TValidationNormal;
	isNotLessThan?: TValidationDate;
	isNotGreaterThanToday?: TValidationNormal;
	codeLength?: TValidationVerificationCode;
	custom?: (value: Value) => TCustomValidationNormal;
};

export type TValidationFunctions = {
	isRequired: (value: string | string[] | boolean, errorMessage?: string) => string[];
	isNumber: (value: string, errorMessage?: string) => string[];
	isPhoneNumber: (value: string, errorMessage?: string) => string[];
	isEmail: (value: string, errorMessage?: string) => string[];
	isNotLessThan: (value: string, limit: string) => string[];
	isNotGreaterThanToday: (value: string, errorMessage?: string) => string[];
};

export type ValidateOne<T extends string = string> = {
	(
		e:
			| CustomChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement, T>
			| CustomChangeEventAndValue<HTMLInputElement, T, unknown>,
	): string[];
};
