const en = {
	formValidator: {
		notArrayEmptyText: "You must choose at least one item",
		notEmptyText: "This field is required",
		notEmailText: "This email address seems to be incorrect",
		notPhoneNumberText: "This phone number seems to be incorrect",
		notNumberText: "This is not a number",
		notSlugText: "This is not a slug",
		notLessThanText: `The date cannot be earlier than {{minDate}}`,
		notGreaterThanTodayText: "The date cannot be equal to or later than today",
		notCheckedText: "You must accept this checkbox",
		notVerificationCodeCompleteText: "You must complete all fields",
	},
	mobilePhoneMUIProps: {
		searchPlaceholder: "Search country",
		searchNotFound: "Country not found",
	},
	selectFieldMUIProps: {
		noneOptionText: "-- None --",
		loadingOptionsText: "Loading...",
	},
	autocompleteMUIProps: { noOptionsText: "-- No options --" },
	radioButtonsGroupMUI: {
		noOptionsAvailableText: "No options available",
	},
	formWrapper: {
		buttonSendText: "Send",
		buttonNextText: "Next",
		buttonBackText: "Back",
	},
	toastOptions: {
		warnMessage: "Check the warnings",
	},
};

export default en;
