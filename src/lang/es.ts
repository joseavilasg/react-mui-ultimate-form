const es = {
	formValidator: {
		notArrayEmptyText: "Debe elegir al menos un elemento",
		notEmptyText: "Debe completar este campo",
		notEmailText: "Esta dirección de correo parece ser incorrecta",
		notPhoneNumberText: "Este número parece ser incorrecto",
		notNumberText: "Este no es un número",
		notSlugText: "Este no es un slug",
		notLessThanText: `La fecha no puede ser anterior al año {{minDate}}`,
		notGreaterThanTodayText: "La fecha no puede ser posterior o igual a la fecha de hoy",
		notCheckedText: "Debe aceptar este checkbox",
		notVerificationCodeCompleteText: "Debe completar todos los campos",
	},
	mobilePhoneMUIProps: {
		searchPlaceholder: "Buscar países",
		searchNotFound: "País no encontrado",
	},
	selectFieldMUIProps: {
		noneOptionText: "-- Ninguno --",
		loadingOptionsText: "Cargando...",
	},
	autocompleteMUIProps: { noOptionsText: "-- No hay opciones --" },
	radioButtonsGroupMUI: {
		noOptionsAvailableText: "No hay opciones disponibles",
	},
	formWrapper: {
		buttonSendText: "Enviar",
		buttonNextText: "Siguiente",
		buttonBackText: "Atrás",
	},
	toastOptions: {
		warnMessage: "Verifica las advertencias",
	},
};

export default es;
