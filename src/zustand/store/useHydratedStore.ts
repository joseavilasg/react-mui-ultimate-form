import { useEffect, useState } from "react";
import { useShallow } from "zustand/react/shallow";

import { initialFormsState, type FormsActions, type FormsState } from "../slices/formsStateSlice";
import useBoundStore from "./useBoundStore";

export type MayusCharacter =
	| "A"
	| "B"
	| "C"
	| "D"
	| "E"
	| "F"
	| "G"
	| "H"
	| "I"
	| "J"
	| "K"
	| "L"
	| "M"
	| "N"
	| "Ñ"
	| "O"
	| "P"
	| "Q"
	| "R"
	| "S"
	| "T"
	| "U"
	| "V"
	| "W"
	| "X"
	| "Y"
	| "Z";

const initialStates = {
	...initialFormsState,
};

type StoreState<F extends string, T extends string> = FormsState<F, T>;
type StoreActions<F extends string, T extends string> = {
	formsActions: FormsActions<F, T>;
	pepeActions: { reset: () => void };
};

type Split<T extends string> = T extends `${infer L}${MayusCharacter}${string}`
	? T extends `${L}${string}`
		? L
		: never
	: never;

const useHydratedStore = <
	F extends string,
	T extends string,
	K extends Split<keyof StoreState<F, T>>,
>(
	key: K,
): {
	state: StoreState<F, T>[`${K}State`];
	actions: StoreActions<F, T>[`${K}Actions`];
	hasHydrated: boolean;
} => {
	type StateKey = `${K}State`;
	type ActionsKey = `${K}Actions`;
	const stateKey: StateKey = `${key}State`;

	const [state, setState] = useState<StoreState<F, T>[StateKey]>(initialStates[stateKey]);
	const zustandState = useBoundStore<F, T, FormsState<F, T>[StateKey]>(
		useShallow((persistedState) => persistedState[stateKey]),
	);
	const zustandActions = useBoundStore<F, T, StoreActions<F, T>[ActionsKey]>(
		useShallow((persistedState) => {
			const { [stateKey]: omit, ...rest } = persistedState;

			return { ...rest } as unknown as StoreActions<F, T>[ActionsKey];
		}),
	);

	const [hasHydrated, setHasHydrated] = useState(false);

	useEffect(() => {
		setState(zustandState);
		setHasHydrated(true);
	}, [zustandState]);

	return { state, actions: zustandActions, hasHydrated };
};

export default useHydratedStore;
