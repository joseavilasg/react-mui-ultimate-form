import { create } from "zustand";

// import { createJSONStorage, persist } from "zustand/middleware";
// import { immer } from "zustand/middleware/immer";

import createFormsStateSlice, { FormsSlice } from "../slices/formsStateSlice";

// const useBoundStore =
// 	//  <T extends Record<string, TBaseFormField>>() =>
// 	create<FormsSlice>(createFormsStateSlice) as {
// 		<T extends Record<string, TBaseFormField>>(): FormsSlice<T>;
// 		<T extends Record<string, TBaseFormField>, U>(selector: (s: FormsSlice<T>) => U): U;
// 	};

const useBoundStore =
	//  <T extends Record<string, TBaseFormField>>() =>
	create<FormsSlice>(createFormsStateSlice) as {
		<F extends string, T extends string>(): FormsSlice<F, T>;
		<F extends string, T extends string, U>(selector: (s: FormsSlice<F, T>) => U): U;
	};

// (
// 	immer(
// 		persist(
// 			(...a) => ({
// 				...createFormsStateSlice(...a),
// 			}),
// 			{
// 				name: "globalState", // unique name
// 				// getStorage: () => localStorage, // (optional) by default the 'localStorage' is used
// 				storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used
// 			},
// 		),
// 	),
// )

// export const useBoundStore = <Item, Slice>(selector: (state: FormsSlice<Item>) => Slice) =>
// 	useBoundStore1(selector);

export default useBoundStore;
