import { produce } from "immer";
import { StateCreator } from "zustand";

import { TErrors } from "@/types/errors";
import {
	FormBlurEvent,
	FormChangeEvent,
	FormCheckboxGroupChangeEvent,
	FormInputChangeEvent,
	FormPhoneChangeEvent,
	FormSubmitEvent,
	FormVerificationCodeBlurEvent,
	FormVerificationCodeChangeEvent,
	FormVerificationCodeKeyDownEvent,
} from "@/types/events";
import { StepType, TFormPersistedValuesDef, TFormValuesDef } from "@/types/formModel";
import { TGetRef } from "@/types/refs";

export type StepsCompleted = Record<string, boolean>;
export type FormState<T extends string> = {
	stepId: string;
	activeStep: number;
	stepType: StepType;
	stepsCompleted: StepsCompleted;
	title: string;
	minTimeMs: number;
	values: TFormValuesDef<T>;
	persistedValues?: TFormPersistedValuesDef<T>;
	getRef: TGetRef<T>;
	errors: TErrors<TFormValuesDef<T>>;
	handleChange: FormChangeEvent<T>;
	handlePhoneChange: FormPhoneChangeEvent<T>;
	handleVerificationCodeChange: FormVerificationCodeChangeEvent;
	handleCheckBoxChange: FormInputChangeEvent<T>;
	handleRadioButtonChange: FormChangeEvent<T>;
	handleCheckboxGroupChange: FormCheckboxGroupChangeEvent<T>;
	handleBlur: FormBlurEvent<T>;
	handleVerificationCodeBlur: FormVerificationCodeBlurEvent;
	handleVerificationCodeKeyDown: FormVerificationCodeKeyDownEvent;
	handleSubmit: FormSubmitEvent<T>;
};

export type FormsStateVal<FormId extends string, T extends string> = {
	[K in FormId]: FormState<T>;
};

export type FormsState<F extends string, T extends string> = {
	formsState: FormsStateVal<F, T>;
};

export type FormsActions<F extends string, T extends string> = {
	setFormsState: (
		callback:
			| ((prevState: FormsState<F, T>["formsState"]) => void)
			| FormsState<F, T>["formsState"],
	) => void;
	resetFormsState: () => void;
};

export type FormsSlice<F extends string = string, T extends string = string> = FormsState<F, T> &
	FormsActions<F, T>;

export const initialFormsState: any = {
	formsState: {},
};

const createFormsStateSlice = <F extends string, T extends string>(
	...params: Parameters<StateCreator<FormsSlice<F, T>>>
): ReturnType<StateCreator<FormsSlice<F, T>>> => {
	const [set] = params;
	return {
		formsState: initialFormsState.formsState,
		setFormsState: (callback) => {
			if (typeof callback === "function") {
				set((state) => ({
					formsState: produce<FormsStateVal<F, T>, FormsStateVal<F, T>>(
						state.formsState,
						(draft) => {
							callback(draft);
						},
					),
				}));
			} else {
				return { formsState: callback };
			}
		},
		resetFormsState: () => set({ formsState: initialFormsState.formsState }),
	};
};

export default createFormsStateSlice;
