import { ToastContainer } from "react-toastify";

import "./styles/index.sass";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Accordion, AccordionDetails, AccordionSummary, Typography } from "@mui/material";

import CustomValidationsForm from "./Example/CustomValidationsForm";
import FirstForm from "./Example/FirstForm";
import MultistepForm from "./Example/MultistepForm";
import MultisteplWithFormWrapper from "./Example/MultisteplWithFormWrapper";
import RadioButtonsForm from "./Example/RadioButtonsForm";
import WithPersistedValuesForm from "./Example/WithPersistedValues";
import WithTimerForm from "./Example/WithTimerForm";

function App() {
	return (
		<>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						1. Basic controlled form
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<FirstForm />
				</AccordionDetails>
			</Accordion>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						2. Multistep controlled form
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<MultistepForm />
				</AccordionDetails>
			</Accordion>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						3. Multistep controlled form with form wrapper
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<MultisteplWithFormWrapper />
				</AccordionDetails>
			</Accordion>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						4. With persisted values controlled form
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<WithPersistedValuesForm />
				</AccordionDetails>
			</Accordion>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						5. Custom Validation Form
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<CustomValidationsForm />
				</AccordionDetails>
			</Accordion>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						6. With timer form
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<WithTimerForm />
				</AccordionDetails>
			</Accordion>
			<Accordion TransitionProps={{ unmountOnExit: true }}>
				<AccordionSummary expandIcon={<ExpandMoreIcon />}>
					<Typography variant="h5" fontWeight={600}>
						7. A lot of radio groups
					</Typography>
				</AccordionSummary>
				<AccordionDetails>
					<RadioButtonsForm />
				</AccordionDetails>
			</Accordion>

			<ToastContainer limit={3} />
		</>
	);
}

export default App;
