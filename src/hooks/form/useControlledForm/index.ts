import React, { useEffect, useRef, useState } from "react";
import i18nConfig from "@/plugins/i18n";
import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import { useShallow } from "zustand/react/shallow";

import { TUseControlledFormParams, TUseControlledFormResult } from "@/types/controlledFormHook";
import { TCountry } from "@/types/countryRegion";
import { TErrors } from "@/types/errors";
import { TFieldsDef, TFormValuesDef } from "@/types/formModel";

import useFormHandlers from "../hooks/useFormHandlers";
import useValidationFunctions from "../hooks/useValidationFunctions";
import getActiveStep from "../utils/getActiveStep";
import getDefaultStepsCompleted from "../utils/getDefaultStepsCompleted";
import getInitialValuesFromFields from "../utils/getInitialValuesFromFields";
import setDayJsLocale from "../utils/setDayJsLocale";
import useGetRef from "./../hooks/useGetRef";

const useControlledForm = <F extends string, T extends string>({
	formModel,
	activeStep = 0,
	lang,
}: TUseControlledFormParams<F, T>): TUseControlledFormResult<T> => {
	const i18n = i18nConfig;
	useEffect(() => {
		if (lang) {
			setDayJsLocale(lang);
			i18n.changeLanguage(lang);
		}
	}, [lang]);
	const { steps, formId } = formModel;
	const formModelActiveStepFields: Partial<TFieldsDef<T>> = (() => {
		const ff = steps[activeStep];
		if (ff.stepType === "Constructive") {
			return ff.fields;
		}

		return {};
	})();

	const stepsLength = steps.length;
	const formModelActiveStep = getActiveStep({ steps: steps, activeStep });

	const getRef = useGetRef<T>();
	const formFieldNames: T[] = Object.keys(formModelActiveStepFields) as T[];
	// Form field items can be string, or array of objects in case of fields that are used as select fields
	const verificationCodeName =
		formModelActiveStepFields?.["verificationCode" as T]?.name ?? ("" as T);
	// Get fields length for verification code
	const numberFieldsForVerificationCode = (() => {
		if (Object.prototype.hasOwnProperty.call(formModelActiveStepFields, verificationCodeName)) {
			const modelVerificationCode = formModelActiveStepFields?.[verificationCodeName];
			if (modelVerificationCode) {
				return modelVerificationCode.validations.codeLength || 0;
			}
			return 0;
		}
		return 0;
	})();
	const verificationCodeRef = getRef(verificationCodeName);

	const { setFormsState, formsState } = useBoundStore<F, T, FormsSlice<F, T>>(
		useShallow((s) => s),
	);

	const persistedValues = formsState[formId]?.["persistedValues"];
	const initialValues = getInitialValuesFromFields(formModelActiveStepFields);
	const values = formsState[formId]?.["values"] ?? initialValues;
	const valuesRef = useRef(values);
	const setValues: React.Dispatch<React.SetStateAction<TFormValuesDef<T>>> = (state) => {
		if (typeof state === "function") {
			const newState = state(valuesRef.current);
			valuesRef.current = newState;
			setFormsState((draft) => {
				draft[formId] = {
					...draft[formId],
					values: newState,
				};
			});
		} else {
			valuesRef.current = state;
			setFormsState((draft) => {
				draft[formId] = {
					...draft[formId],
					values: state,
				};
			});
		}
	};
	useEffect(() => {
		if (persistedValues) {
			setValues((prevValues) => {
				const updatedValues = { ...prevValues };

				for (const key in persistedValues) {
					if (
						!prevValues[key].data ||
						!(Object.keys(prevValues[key].data as object).length === 0)
					) {
						updatedValues[key] = { ...updatedValues[key], data: persistedValues[key] };
					}
				}
				return updatedValues;
			});
		}
	}, [persistedValues]);

	// Get initial values for errors
	const newObject = formFieldNames.reduce((obj, item) => {
		return { ...obj, [item]: [] };
	}, {}) as TErrors<TFormValuesDef<T>>;
	const errors = formsState[formId]?.["errors"] ?? newObject;
	const errorsRef = useRef(errors);
	const setErrors: React.Dispatch<React.SetStateAction<TErrors<TFormValuesDef<T>>>> = (state) => {
		if (typeof state === "function") {
			errorsRef.current = state(errorsRef.current);
			setFormsState((draft) => {
				draft[formId] = {
					...draft[formId],
					errors: state(errorsRef.current),
				};
			});
		} else {
			errorsRef.current = state;
			setFormsState((draft) => {
				draft[formId] = {
					...draft[formId],
					errors: state,
				};
			});
		}
	};
	// Phone
	const [country, _setCountry] = useState<TCountry>({
		value: "PE",
		name: "Perú",
		name_unicode: "Peru",
	});

	const countryRef = useRef<TCountry>(country);
	const setCountry = (state: TCountry) => {
		countryRef.current = state;
		_setCountry(state);
	};

	const { validateOne, validateAll, validateAllVerificationCodeFields } = useValidationFunctions({
		formModelActiveStepFields,
		verificationCodeName,
		valuesRef,
		errorsRef,
		getRef,
		setErrors,
		formFieldNames,
		numberFieldsForVerificationCode,
	});

	const {
		handleChange,
		handleVerificationCodeChange,
		handleVerificationCodeKeyDown,
		handlePhoneChange,
		handleCheckBoxChange,
		handleCheckboxGroupChange,
		handleRadioButtonChange,
		handleBlur,
		handleVerificationCodeBlur,
		handleSubmit,
	} = useFormHandlers({
		valuesRef,
		errorsRef,
		setValues,
		setErrors,
		validateOne,
		validateAll,
		formModelActiveStep,
		stepsLength,
		verificationCode: {
			numberFieldsForVerificationCode,
			verificationCodeName,
			verificationCodeRef,
			validateAllVerificationCodeFields,
		},
		phone: {
			setCountry,
		},
	});

	const stepsCompleted = formsState[formId]?.stepsCompleted ?? getDefaultStepsCompleted(steps);

	useEffect(() => {
		const { fields: _, ...rest } = formModelActiveStep;
		setFormsState((draft) => {
			const newValues = { ...initialValues, ...valuesRef.current };
			valuesRef.current = newValues;
			draft[formId] = {
				...draft[formId],
				...rest,
				activeStep,
				stepsCompleted,
				values: newValues,
				errors,
				getRef,
				handleChange,
				handlePhoneChange,
				handleVerificationCodeChange,
				handleCheckBoxChange,
				handleRadioButtonChange,
				handleCheckboxGroupChange,
				handleBlur,
				handleVerificationCodeBlur,
				handleVerificationCodeKeyDown,
				handleSubmit,
			};
		});
	}, [activeStep]);

	const { fields: _, title: __, ...rest } = formModelActiveStep;
	return {
		stepInfo: {
			stepsLength,
			...rest,
		},
		values: values,
		getRef,
		errors,
		model: formModelActiveStep,
		handleChange,
		handleVerificationCodeChange,
		handlePhoneChange,
		handleCheckBoxChange,
		handleRadioButtonChange,
		handleCheckboxGroupChange,
		handleBlur,
		handleVerificationCodeBlur,
		handleVerificationCodeKeyDown,
		handleSubmit,
	};
};

export default useControlledForm;
