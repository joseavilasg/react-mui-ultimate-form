import { IntersectionOf, TFormsDef } from "@/types/formModel";

const buildForms = <F extends string, T extends string, Forms extends TFormsDef<F, T>[]>(
	...forms: Forms
): IntersectionOf<Forms> => {
	return forms.reduce((acc, form) => Object.assign(acc, form), {} as any);
};

export default buildForms;
