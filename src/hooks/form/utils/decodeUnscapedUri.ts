const decodeUnscapedUri = (text: string) => {
	const regex = /u([\dA-F]{4})/gi;

	const unicodeEscaped = text.match(regex);
	const decodedUnicode = (() => {
		if (unicodeEscaped) {
			return unicodeEscaped
				.map((match) => String.fromCharCode(parseInt(match.slice(1), 16)))
				.join("");
		}
		return "";
	})();

	const decodedText = text.replace(regex, decodedUnicode);

	return decodedText;
};

export default decodeUnscapedUri;
