export const toConsumableArray = function (arr: Array<unknown>) {
	const arr2 = Array(arr.length);
	if (Array.isArray(arr)) {
		for (let i = 0; i < arr.length; i++) arr2[i] = arr[i];
		return arr2;
	} else {
		return Array.from(arr);
	}
};
