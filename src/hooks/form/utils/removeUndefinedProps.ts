const removeUndefinedProps = <T extends Record<string, any>>(data: T): Required<T> => {
	return Object.keys(data).reduce<Required<T>>((acc, key) => {
		if (data[key] !== undefined) {
			acc[key as keyof T] = data[key];
		}
		return acc;
	}, {} as Required<T>);
};

export default removeUndefinedProps;
