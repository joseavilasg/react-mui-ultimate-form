import { TFieldsDef, TFormValuesDef } from "@/types/formModel";

const getInitialValuesFromFields = <T extends string>(
	fields: Partial<TFieldsDef<T>>,
): TFormValuesDef<T> => {
	const initialValues: any = {};
	for (const key in fields) {
		initialValues[key] = { data: fields?.[key]?.initialValue, type: fields?.[key]?.type };
	}
	return initialValues;
};

export default getInitialValuesFromFields;
