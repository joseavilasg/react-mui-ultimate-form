import { TFormDef, TFormsDef } from "@/types/formModel";

const buildFormModel = <F extends string, T extends string>(
	model: TFormDef<F, T>,
): TFormsDef<F, T> => {
	return {
		[model.formId]: model,
	} as TFormsDef<F, T>;
};

export default buildFormModel;
