import {
	TFieldsDefOut,
	TFormFieldsDefStepOut,
	TFormFieldsStepConstructiveDef,
	TFormFieldsStepInformativeDef,
} from "@/types/formModel";

type GetActiveStepParams<T extends string> = {
	steps: (TFormFieldsStepInformativeDef | TFormFieldsStepConstructiveDef<T>)[];
	activeStep: number;
};

const getActiveStep = <T extends string>({
	steps,
	activeStep,
}: GetActiveStepParams<T>): TFormFieldsDefStepOut<T> => {
	const ff = steps[activeStep];
	if (ff.stepType === "Informative") {
		return {
			stepId: ff.stepId ?? activeStep.toString(),
			stepType: ff.stepType,
			minTimeMs: ff.minTimeMs ?? 0,
			title: ff.title,
			fields: {} as TFieldsDefOut<T>,
		};
	}
	return {
		stepId: ff.stepId ?? activeStep.toString(),
		stepType: ff.stepType,
		minTimeMs: ff.minTimeMs ?? 0,
		title: ff.title ?? "",
		fields: ff.fields as TFieldsDefOut<T>,
	};
};

export default getActiveStep;
