import { StepsCompleted } from "@/zustand/slices/formsStateSlice";

import { TFormFieldsStepConstructiveDef, TFormFieldsStepInformativeDef } from "@/types/formModel";

const getDefaultStepsCompleted = <T extends string>(
	steps: (TFormFieldsStepInformativeDef | TFormFieldsStepConstructiveDef<T>)[],
): StepsCompleted => {
	return steps.reduce<Record<string, boolean>>((acc, { stepId }, idx) => {
		if (stepId) {
			acc[stepId] = false;
		} else {
			acc[idx.toString()] = false;
		}
		return acc;
	}, {});
};

export default getDefaultStepsCompleted;
