import dayjs from "dayjs";
import en from "dayjs/locale/en";
import es from "dayjs/locale/es";

import { TLanguage } from "@/types/language";

const setDayJsLocale = (language: TLanguage) => {
	const dayjsLocales = {
		en: en,
		es: es,
	};

	const locale = dayjsLocales[language as keyof typeof dayjsLocales];
	if (language !== "en") {
		dayjs.locale(locale);
	}
};

export default setDayJsLocale;
