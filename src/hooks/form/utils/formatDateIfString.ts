import dayjs, { type Dayjs } from "dayjs";
import localizedFormat from "dayjs/plugin/localizedFormat";

import { DateInputValue } from "@/types/inputValues";

const formatDateIfString = (date: DateInputValue): Dayjs => {
	dayjs.extend(localizedFormat);
	if (typeof date === "string") {
		return dayjs(dayjs(date).format("L"));
	}
	return date;
};

export default formatDateIfString;
