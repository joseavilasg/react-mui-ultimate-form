import { getTranslations } from "@/plugins/translations";
import { getI18n } from "react-i18next";

import { TErrors, TNormalError, TVerificationCodeError } from "@/types/errors";
import { CustomChangeEvent } from "@/types/events";
import { TFieldsDef, TFormValuesDef } from "@/types/formModel";
import { TGetRef, TRef, TRefCurrent } from "@/types/refs";
import {
	TCustomValidationNormal,
	TValidationGroup,
	TValidationGroupKeys,
	ValidateOne,
} from "@/types/validations";

import useFormValidator from "./useFormValidator";

type UseValidationFunctionsParms<T extends string> = {
	formModelActiveStepFields: Partial<TFieldsDef<T>>;
	valuesRef: React.MutableRefObject<TFormValuesDef<T>>;
	errorsRef: React.MutableRefObject<TErrors<TFormValuesDef<T>>>;
	setErrors: React.Dispatch<React.SetStateAction<TErrors<TFormValuesDef<T>>>>;
	getRef: TGetRef<T>;
	verificationCodeName: T;
	numberFieldsForVerificationCode: number;
	formFieldNames: T[];
};

/**
 *	updateErrors and focusErrorInput run with errors ref since these functions are executed inside another function
 * @returns void
 */
const useValidationFunctions = <T extends string>({
	formModelActiveStepFields,
	valuesRef,
	errorsRef,
	setErrors,
	getRef,
	verificationCodeName,
	numberFieldsForVerificationCode,
	formFieldNames,
}: UseValidationFunctionsParms<T>) => {
	const { formValidator } = useFormValidator();
	const verificationCodeRef = getRef(verificationCodeName);

	const validateIsRequired = (
		value: string | string[] | boolean,
		errorMessage?: string,
	): string[] => {
		if (typeof value === "boolean") {
			// for check
			return formValidator(value).isChecked(errorMessage);
		} else if (Array.isArray(value)) {
			// for arrays
			return formValidator(value).isNotArrayEmpty(errorMessage);
		} else {
			// for simple string
			return formValidator(value).isNotEmpty(errorMessage);
		}
	};

	const validateIsNumber = (value: string, errorMessage?: string): string[] => {
		return formValidator(value).isNumber(errorMessage);
	};

	const validateIsEmail = (value: string, errorMessage?: string): string[] => {
		return formValidator(value).isEmail(errorMessage);
	};
	const validateIsNotEmail = (value: string, errorMessage?: string): string[] => {
		return formValidator(value).isNotEmail(errorMessage);
	};

	const validateIsPhoneNumber = (value: string, errorMessage?: string): string[] => {
		return formValidator(value.toString()).isPhoneNumber(errorMessage);
	};

	const validateIsNotLessThan = (value: string, limit: string): string[] => {
		return formValidator(value).isNotLessThan(limit);
	};

	const validateIsNotGreaterThanToday = (value: string, errorMessage?: string): string[] => {
		return formValidator(value).isNotGreaterThanToday(errorMessage);
	};

	const validateIsSlug = (value: string, errorMessage?: string) => {
		return formValidator(value).isSlug(errorMessage);
	};

	/**Uses errors ref because this function is executed inside of another function. */
	const updateErrors = (name: T) => {
		const ref = getRef(name);
		const event = {
			target: {
				value:
					(ref?.current as TRefCurrent)?.value ||
					(ref?.current as TRefCurrent<"phone">)?.props?.value ||
					"",
				name,
				checked: (ref?.current as TRefCurrent)?.checked || false,
				type: (ref?.current as TRefCurrent)?.type,
			},
		} as CustomChangeEvent<HTMLInputElement, Extract<T, string>>;
		setErrors((val) => {
			return { ...val, [event.target.name]: validateOne(event) };
		});
	};

	/**Uses errors ref because this function is executed inside of another function. */
	const focusErrorInput = <T extends string, N extends string | number | symbol>(
		fieldKeys: string[],
		getRef: TGetRef<N>,
		errorsRef: React.MutableRefObject<TErrors<TFormValuesDef<T>>>,
		firstVerificationCodeErrorIndex: number | null,
		verificationCodeRef: TRef,
	) => {
		const firstErrorIndex = Object.keys(errorsRef.current).findIndex((key) =>
			fieldKeys.includes(key),
		);
		const firstFormElementWithError = getRef(
			Object.keys(errorsRef.current)[firstErrorIndex] as N,
		).current;
		if (firstFormElementWithError) {
			if (Object.hasOwn(firstFormElementWithError, "focus")) {
				// Select or Radio Group input
				(firstFormElementWithError as TRefCurrent<"select">)?.focus();
				if (Object.hasOwn(firstFormElementWithError as TRefCurrent<"select">, "openSelect")) {
					(firstFormElementWithError as TRefCurrent<"select">)?.openSelect();
				}
			} else if (
				Object.hasOwn(firstFormElementWithError, "value") ||
				Object.hasOwn(firstFormElementWithError, "checked")
			) {
				// Normal input or check box
				(firstFormElementWithError as TRefCurrent)?.focus();
			} else if (
				Object.hasOwn(firstFormElementWithError as TRefCurrent<"phone">, "numberInputRef")
			) {
				// Phone input
				(firstFormElementWithError as TRefCurrent<"phone">)?.numberInputRef?.focus();
			} else if (firstVerificationCodeErrorIndex !== null) {
				// Code verification inputs
				focusVerificationCodeErrorInput(firstVerificationCodeErrorIndex, verificationCodeRef);
			}
		}
	};

	const focusVerificationCodeErrorInput = (index: number, verificationCodeRef: TRef) => {
		(
			(verificationCodeRef.current as Record<string, TRef>)[index].current as HTMLInputElement
		)?.focus();
	};

	const validationFunctions: {
		[K in Exclude<TValidationGroupKeys, "custom">]: (...params: any) => string[];
	} = {
		isRequired: validateIsRequired,
		isEmail: validateIsEmail,
		isNotEmail: validateIsNotEmail,
		isPhoneNumber: validateIsPhoneNumber,
		isNotLessThan: validateIsNotLessThan,
		isNotGreaterThanToday: validateIsNotGreaterThanToday,
		isNumber: validateIsNumber,
		isSlug: validateIsSlug,
		codeLength: () => {
			return [""];
		},
	};

	// Parse required validation to use in form
	const validateOne: ValidateOne<T> = (e) => {
		const { value, name, type } = e.target;
		const formModelActiveStepField = formModelActiveStepFields[name];
		let validationField: TValidationGroup | null = null;
		let validate: string[] = [];

		if (formModelActiveStepField) {
			validationField = formModelActiveStepField.validations;
		}
		if (validationField) {
			const validations = Object.keys(validationField) as TValidationGroupKeys[];

			const getCustomErrorMessageFromModel = (
				fieldRule: TValidationGroupKeys,
			): string | undefined => {
				const typeFieldRuleValue = typeof validationField?.[fieldRule];
				if (typeFieldRuleValue === "boolean") {
					return undefined;
				} else {
					return (validationField?.[fieldRule] as TCustomValidationNormal)[1] as string;
				}
			};

			validations.forEach((validation: TValidationGroupKeys) => {
				if (validationField?.[validation]) {
					if (validation === "isRequired" && type === "checkbox") {
						const { checked } = (e as CustomChangeEvent<HTMLInputElement, T>).target;
						validate = [
							...validate,
							...validationFunctions[validation](
								checked,
								getCustomErrorMessageFromModel(validation),
							),
						];
					} else if (validation === "isNotLessThan") {
						validate = [
							...validate,
							...validationFunctions[validation](value, validationField?.[validation]),
						];
					} else if (validation === "custom") {
						validate = [
							...validate,
							...(() => {
								const customFunctionValidator = validationField?.[validation];
								if (customFunctionValidator) {
									const [isValid, message] = customFunctionValidator(value);
									if (!isValid) return [message];
								}
								return [];
							})(),
						];
					} else {
						validate = [
							...validate,
							...validationFunctions[validation](
								value,
								getCustomErrorMessageFromModel(validation),
							),
						];
					}
				}
			});
		}

		return validate;
	};

	const validateAllVerificationCodeFields = (
		fields: number,
	): { res: boolean; firstVerificationCodeErrorIndex: number | null } => {
		if (!fields) {
			return { res: true, firstVerificationCodeErrorIndex: null };
		}

		const validationField: TValidationGroup | null = (() => {
			if (
				Object.prototype.hasOwnProperty.call(formModelActiveStepFields, verificationCodeName)
			) {
				const modelVerificationCode = formModelActiveStepFields?.[verificationCodeName];
				if (modelVerificationCode) {
					return modelVerificationCode.validations;
				}
				return null;
			}
			return null;
		})();
		// Verification code fields
		let flag = true;

		let errorIndex: number | null = null;

		const errorForCell: boolean[] = [];

		(valuesRef.current[verificationCodeName].data as string[]).forEach((value, index) => {
			if (value.length == 1) {
				errorForCell[index] = false;
			} else {
				errorForCell[index] = true;
				errorIndex = errorIndex === null ? index : errorIndex;
			}
		});

		setErrors((state) => {
			return {
				...state,
				[verificationCodeName]: {
					message:
						(validationField?.isRequired as TCustomValidationNormal)?.[1] ||
						getTranslations(getI18n().t).formValidator.notVerificationCodeCompleteText,
					errors: errorForCell,
				},
			};
		});

		(errorsRef.current[verificationCodeName] as TVerificationCodeError).errors.forEach((e) => {
			if (e == true) {
				flag = false;
			}
		});

		if (flag) {
			setErrors((state) => {
				return {
					...state,
					[verificationCodeName]: [],
				} as TErrors<TFormValuesDef<T>>;
			});
		}

		return { res: flag, firstVerificationCodeErrorIndex: errorIndex };
	};

	const validateAll = () => {
		const { firstVerificationCodeErrorIndex } = validateAllVerificationCodeFields(
			numberFieldsForVerificationCode,
		);
		formFieldNames
			.filter((name) => name != verificationCodeName)
			.forEach((name) => updateErrors(name));

		const keysOfFieldWithError = Object.entries<TErrors<TFormValuesDef<T>>[T]>(errorsRef.current)
			.filter((t) => {
				const [key, value] = t;
				return (
					((value as TNormalError).length > 0 ||
						(value as TVerificationCodeError)?.errors?.length > 0) &&
					formFieldNames.includes(key as T)
				);
			})
			.map(([key]) => key);

		if (keysOfFieldWithError.length > 0) {
			focusErrorInput(
				keysOfFieldWithError,
				getRef,
				errorsRef,
				firstVerificationCodeErrorIndex,
				verificationCodeRef,
			);
			return false;
		}

		return true;
	};

	return {
		validationFunctions,
		validateAllVerificationCodeFields,
		validateAll,
		focusVerificationCodeErrorInput,
		focusErrorInput,
		updateErrors,
		validateOne,
	};
};

export default useValidationFunctions;
