import { KEYS_CODE_VALUE, toastOptions } from "@/const";
import { getTranslations } from "@/plugins/translations";
import { getI18n } from "react-i18next";
import { toast } from "react-toastify";

import { TCountry } from "@/types/countryRegion";
import { TErrors, TNormalError } from "@/types/errors";
import {
	CustomKeyboardEvent,
	FormBlurEvent,
	FormChangeEvent,
	FormCheckboxGroupChangeEvent,
	FormInputChangeEvent,
	FormPhoneChangeEvent,
	FormSubmitEvent,
} from "@/types/events";
import { TFormFieldsDefStepOut, TFormValuesDef } from "@/types/formModel";
import { TRef, TRefObject } from "@/types/refs";
import { ValidateOne } from "@/types/validations";

import countriesList from "../const/country-list";
import { toConsumableArray } from "../utils/toConsumableArray";

type UseFormHandlersParams<T extends string> = {
	validateOne: ValidateOne<T>;
	validateAll: () => boolean;
	valuesRef: React.MutableRefObject<TFormValuesDef<T>>;
	errorsRef: React.MutableRefObject<TErrors<TFormValuesDef<T>>>;
	setValues: React.Dispatch<React.SetStateAction<TFormValuesDef<T>>>;
	setErrors: React.Dispatch<React.SetStateAction<TErrors<TFormValuesDef<T>>>>;
	formModelActiveStep: TFormFieldsDefStepOut<T>;
	stepsLength: number;
	verificationCode: {
		verificationCodeName: T;
		numberFieldsForVerificationCode: number;
		verificationCodeRef: TRef;
		validateAllVerificationCodeFields: (fields: number) => {
			res: boolean;
			firstVerificationCodeErrorIndex: number | null;
		};
	};
	phone: {
		setCountry: (state: TCountry) => void;
	};
};
const useFormHandlers = <T extends string>({
	validateOne,
	validateAll,
	valuesRef,
	errorsRef,
	setValues,
	setErrors,
	formModelActiveStep,
	stepsLength,
	verificationCode: {
		verificationCodeName,
		numberFieldsForVerificationCode,
		verificationCodeRef,
		validateAllVerificationCodeFields,
	},
	phone: { setCountry },
}: UseFormHandlersParams<T>) => {
	const handleVerificationCodeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const index = parseInt(e.target.dataset.id || "");
		if (e.target.value === "" || !e.target.validity.valid) {
			return;
		}
		let next: TRef | null = null;
		const value = e.target.value;
		const vals = valuesRef.current[verificationCodeName].data as string[];
		const newVals = [...vals];

		if (value.length > 1) {
			let nextIndex = value.length + index - 1;
			if (nextIndex >= numberFieldsForVerificationCode) {
				nextIndex = numberFieldsForVerificationCode - 1;
			}
			next = (verificationCodeRef.current as TRefObject)[nextIndex];
			const split = value.split("");
			split.forEach(function (item, i) {
				const cursor = index + i;
				if (cursor < numberFieldsForVerificationCode - 1) {
					vals[cursor] = item;
				}
			});
			setValues((val) => {
				return {
					...val,
					[verificationCodeName]: { ...val[verificationCodeName], data: newVals },
				};
			});
		} else {
			next = (verificationCodeRef.current as TRefObject)[index + 1];
			newVals[index] = value;
			setValues((val) => {
				return {
					...val,
					[verificationCodeName]: { ...val[verificationCodeName], data: newVals },
				};
			});
		}

		if (next) {
			(next.current as HTMLInputElement).focus();
			(next.current as HTMLInputElement).select();
		}

		setErrors((state) => {
			return {
				...state,
				[verificationCodeName]: Array(numberFieldsForVerificationCode).fill(false),
			} as TErrors<TFormValuesDef<T>>;
		});
	};

	const handleVerificationCodeKeyDown = (e: CustomKeyboardEvent) => {
		const index = parseInt(e.target.dataset.id);
		const prevIndex = index - 1;
		const nextIndex = index + 1;
		const initialIndex = 0;
		const lastIndex = numberFieldsForVerificationCode - 1;
		const prev = (verificationCodeRef.current as TRefObject)[prevIndex];
		const next = (verificationCodeRef.current as TRefObject)[nextIndex];
		const first = (verificationCodeRef.current as TRefObject)[initialIndex];
		const last = (verificationCodeRef.current as TRefObject)[lastIndex];

		switch (e.keyCode) {
			case KEYS_CODE_VALUE.BACKSPACE.code: {
				e.preventDefault();
				const vals = toConsumableArray(
					valuesRef.current[verificationCodeName].data as string[],
				);
				if (vals[index]) {
					vals[index] = "";
					setValues((val) => {
						return {
							...val,
							[verificationCodeName]: { ...val[verificationCodeName], data: vals },
						};
					});
				} else if (prev) {
					vals[prevIndex] = "";
					(prev.current as HTMLInputElement).focus();
					setValues((val) => {
						return {
							...val,
							[verificationCodeName]: { ...val[verificationCodeName], data: vals },
						};
					});
				}
				break;
			}
			case KEYS_CODE_VALUE.LEFT.code:
				e.preventDefault();
				if (prev) {
					(prev.current as HTMLInputElement).focus();
				}
				break;
			case KEYS_CODE_VALUE.RIGHT.code:
				e.preventDefault();
				if (next) {
					(next.current as HTMLInputElement).focus();
				}
				break;
			case KEYS_CODE_VALUE.HOME.code:
				e.preventDefault();
				if (first) {
					(first.current as HTMLInputElement).focus();
				}
				break;
			case KEYS_CODE_VALUE.END.code:
				e.preventDefault();
				if (last) {
					(last.current as HTMLInputElement).focus();
				}
				break;
			case KEYS_CODE_VALUE.UP.code:
			case KEYS_CODE_VALUE.DOWN.code:
				e.preventDefault();
				break;
			default:
				break;
		}
	};

	const handlePhoneChange: FormPhoneChangeEvent<T> = (
		_,
		selectedCountry,
		event,
		formattedValue,
	) => {
		setCountry(
			countriesList.find(
				(c) => c.value.toLowerCase() == selectedCountry.iso2.toLowerCase(),
			) as TCountry,
		);
		const e = event;
		e.target.value = formattedValue;
		setValues((val) => {
			return { ...val, [e.target.name]: { ...val[e.target.name], data: formattedValue } };
		});
		setErrors((val) => {
			return {
				...val,
				[e.target.name]: validateOne(e),
			} as TErrors<TFormValuesDef<T>>;
		});
	};

	const handleChange: FormChangeEvent<T> = (e) => {
		const { name, value } = e.target;
		setValues((val) => {
			return { ...val, [name]: { ...val[name], data: value } };
		});
		// Show error only if field had error before, so we can hide error while writing
		if ((errorsRef.current[name] as TNormalError)?.length) {
			setErrors((state) => {
				return { ...state, [name]: validateOne(e) } as TErrors<TFormValuesDef<T>>;
			});
		}
	};

	const handleCheckBoxChange: FormInputChangeEvent<T> = (e) => {
		const { name, checked } = e.target;
		setValues((val: TFormValuesDef<T>) => {
			return { ...val, [name]: { ...val[name], data: checked } };
		});
		// Show error only if field had error before, so we can hide error while writing
		if ((errorsRef.current[name] as TNormalError)?.length) {
			setErrors((state) => {
				return { ...state, [name]: validateOne(e) } as TErrors<TFormValuesDef<T>>;
			});
		}
	};

	const handleRadioButtonChange: FormChangeEvent<T> = (e) => {
		const { name, value } = e.target;
		setValues((val: TFormValuesDef<T>) => {
			return { ...val, [name]: { ...val[name], data: value } };
		});
		// Show error only if field had error before, so we can hide error while writing
		if ((errorsRef.current[name] as TNormalError)?.length) {
			setErrors((state) => {
				return { ...state, [name]: validateOne(e) } as TErrors<TFormValuesDef<T>>;
			});
		}
	};

	const handleCheckboxGroupChange: FormCheckboxGroupChangeEvent<T> = (e) => {
		const { name, value } = e.target;
		setValues((val: TFormValuesDef<T>) => {
			return { ...val, [name]: { ...val[name], data: value } };
		});
		// Show error only if field had error before, so we can hide error while writing
		if ((errorsRef.current[name] as TNormalError)?.length) {
			setErrors((state) => {
				return { ...state, [name]: validateOne(e) } as TErrors<TFormValuesDef<T>>;
			});
		}
	};

	const handleBlur: FormBlurEvent<T> = (e) => {
		const { name } = e.target;
		setErrors((val) => {
			return { ...val, [name]: validateOne(e) } as TErrors<TFormValuesDef<T>>;
		});
	};

	const handleVerificationCodeBlur = (
		e: React.FocusEvent<HTMLTextAreaElement | HTMLInputElement | HTMLButtonElement>,
	) => {
		e;
		validateAllVerificationCodeFields(numberFieldsForVerificationCode);
	};

	const handleSubmit: FormSubmitEvent<T> = (customHandleSubmit, toastId, step) => {
		if (validateAll()) {
			const { fields: _, title: __, ...rest } = formModelActiveStep;
			if (step) {
				const { activeStep, setActiveStep } = step;
				customHandleSubmit({
					values: valuesRef.current,
					stepInfo: {
						stepsLength,
						activeStep,
						setActiveStep,
						...rest,
					},
				});
			} else {
				customHandleSubmit({
					values: valuesRef.current,
					stepInfo: {
						stepsLength,
						activeStep: 0,
						setActiveStep: () => void 0,
						...rest,
					},
				});
			}
		} else {
			toast(getTranslations(getI18n().t).toastOptions.warnMessage, {
				...toastOptions,
				toastId,
				type: "warning",
			});
		}
	};

	return {
		handleChange,
		handleVerificationCodeChange,
		handleVerificationCodeKeyDown,
		handlePhoneChange,
		handleCheckBoxChange,
		handleRadioButtonChange,
		handleCheckboxGroupChange,
		handleBlur,
		handleVerificationCodeBlur,
		handleSubmit,
	};
};

export default useFormHandlers;
