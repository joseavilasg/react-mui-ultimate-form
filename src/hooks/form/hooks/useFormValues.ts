import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import { useShallow } from "zustand/react/shallow";

import { TFormValuesDef } from "@/types/formModel";
import { TUseFormValuesParams, TUseFormValuesResult } from "@/types/formValuesHook";

const useFormValues = <F extends string, T extends string>({
	formModel,
}: TUseFormValuesParams<F, T>): TUseFormValuesResult<T> => {
	const { formsState } = useBoundStore<F, T, FormsSlice<F, T>>(useShallow((s) => s));
	const formState = formsState[formModel.formId];
	if (!formState) {
		return {
			handleSubmit: () => void 0,
			values: {} as TFormValuesDef<T>,
			stepInfo: {
				activeStep: 0,
				stepsLength: 1,
				stepType: formModel.steps[0].stepType,
				stepId: formModel.steps[0].stepId ?? "0",
				minTimeMs: formModel.steps[0].minTimeMs ?? 0,
			},
			isLoading: true,
		};
	}
	const { values, handleSubmit, activeStep, stepType, stepId, minTimeMs } = formState;

	return {
		values,
		handleSubmit,
		stepInfo: { activeStep, stepType, stepId, minTimeMs, stepsLength: formModel.steps.length },
		isLoading: false,
	};
};

export default useFormValues;
