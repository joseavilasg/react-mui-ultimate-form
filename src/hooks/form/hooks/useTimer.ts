import { useEffect, useState } from "react";
import dayjs, { Dayjs } from "dayjs";

function calculateTimeLeft(targetDate: Dayjs) {
	const currentTime = dayjs();
	const timeDiff = dayjs(targetDate).diff(currentTime, "second");
	const days = Math.floor(timeDiff / 86400); // 86400 segundos en un día
	const hours = Math.floor((timeDiff % 86400) / 3600); // 3600 segundos en una hora
	const minutes = Math.floor((timeDiff % 3600) / 60); // 60 segundos en un minuto
	const seconds = timeDiff % 60;

	return {
		days,
		hours,
		minutes,
		seconds,
		total: timeDiff,
	};
}

type UseTimerParams = {
	targetDateTime: Dayjs;
	onNSecondsLeft?: { secondsLeft: number; action: () => void }[];
};
const useTimer = ({ targetDateTime, onNSecondsLeft }: UseTimerParams) => {
	const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(targetDateTime));

	useEffect(() => {
		const timer = setInterval(() => {
			const newTimeLeft = calculateTimeLeft(targetDateTime);

			if (newTimeLeft.total <= 0) {
				clearInterval(timer);
				setTimeLeft({ days: 0, hours: 0, minutes: 0, seconds: 0, total: 0 });
			} else {
				setTimeLeft(newTimeLeft);
				if (onNSecondsLeft) {
					onNSecondsLeft.forEach(({ secondsLeft, action }) => {
						if (newTimeLeft.total === secondsLeft) {
							action();
						}
					});
				}
			}
		}, 1000);

		return () => {
			clearInterval(timer);
		};
	}, [targetDateTime]);

	return timeLeft;
};

export default useTimer;
