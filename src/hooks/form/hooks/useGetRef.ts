import React, { useRef } from "react";

import { TGetRef, TRef, TRefs } from "@/types/refs";

export const useArrayRef = () => {
	const refs: Array<unknown> = [];
	return [refs, (el: unknown) => el && refs.push(el)];
};

const useGetRef = <T extends string | number | symbol>(): TGetRef<T> => {
	// let getRef = useGetRef();
	const refs: TRefs = useRef({});

	const getRef: TGetRef<T> = (idx: T) => {
		const ref = (refs.current[idx as string] ??= React.createRef<TRef["current"]>() as TRef);
		return ref;
	};

	return React.useCallback<TGetRef<T>>(getRef, [refs]);
};

export default useGetRef;
