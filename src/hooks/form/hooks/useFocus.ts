import React from "react";

const useFocus = (htmlElRef: React.MutableRefObject<any>): [() => void] => {
	const setFocus = (): void => {
		htmlElRef?.current?.focus?.();
	};

	return [React.useCallback(setFocus, [htmlElRef])];
};

export default useFocus;
