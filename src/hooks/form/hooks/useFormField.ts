import { useEffect, useRef } from "react";
import { DEFAULT_INITIAL_VALUES } from "@/const";
import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import { useShallow } from "zustand/react/shallow";

import { FieldTypes } from "@/types/fields";
import { TUseFormFieldParams, TUseFormFieldResult } from "@/types/formFieldHook";
import { TFormValuesDef } from "@/types/formModel";
import { ValueTypes } from "@/types/inputValues";

import getActiveStep from "../utils/getActiveStep";

const useFormField = <F extends string, T extends string>({
	formModel,
	fieldName,
	persistedValue,
}: TUseFormFieldParams<F, T>): TUseFormFieldResult<T> => {
	const { formsState, setFormsState } = useBoundStore<F, T, FormsSlice<F, T>>(
		useShallow((s) => s),
	);
	const formState = formsState[formModel.formId];
	const prevPersistedValue = useRef<Partial<TFormValuesDef<T>>[T] | TFormValuesDef<T>[T]>();
	useEffect(() => {
		if (persistedValue !== prevPersistedValue.current) {
			setFormsState((draft) => {
				draft[formModel.formId] = {
					...draft[formModel.formId],
					persistedValues: {
						...draft[formModel.formId]?.persistedValues,
						[fieldName]: persistedValue,
					},
				};
			});
		}
	}, [persistedValue, prevPersistedValue.current, formState]);
	useEffect(() => {
		prevPersistedValue.current = persistedValue;
	}, [persistedValue]);

	function assertNotCustomFieldType(
		type: FieldTypes,
	): asserts type is Exclude<FieldTypes, `${string}CustomField`> {
		if (type?.endsWith("CustomField")) throw new Error(`${type} is a CustomField type`);
	}
	function assertCustomFieldType(type: FieldTypes): asserts type is "CustomField" {
		if (!type?.endsWith("CustomField")) throw new Error(`${type} is not a CustomField type`);
	}

	const getDefaultInitialValue = () => {
		const ff = formModel.steps;
		let val: { data: ValueTypes; type: FieldTypes } = { data: "", type: "CustomField" };
		ff.forEach((e) => {
			if (e.stepType === "Constructive") {
				const type = e.fields[fieldName]?.type;
				if (type) {
					if (type?.endsWith("CustomField")) {
						assertCustomFieldType(type);
						val = { data: DEFAULT_INITIAL_VALUES["CustomField"], type: "CustomField" };
					} else {
						assertNotCustomFieldType(type);
						val = { data: DEFAULT_INITIAL_VALUES[type], type };
					}
				}
			}
		});
		return val as TFormValuesDef<T>[T];
	};

	if (!formState) {
		return {
			field: { name: "" as T, label: "", type: "" as FieldTypes },
			error: false,
			errorText: "",
			handleBlur: () => void 0,
			handleChange: () => void 0,
			handleCheckBoxChange: () => void 0,
			handlePhoneChange: () => void 0,
			handleRadioButtonChange: () => void 0,
			handleCheckboxGroupChange: () => void 0,
			handleSubmit: () => void 0,
			handleVerificationCodeBlur: () => void 0,
			handleVerificationCodeChange: () => void 0,
			handleVerificationCodeKeyDown: () => void 0,
			ref: { current: null },
			required: false,
			value: getDefaultInitialValue(),
			isLoading: true,
		};
	}

	const { values, getRef, errors, activeStep, ...rest } = formState;

	const { steps: _steps } = formModel;

	const steps = getActiveStep({ steps: _steps, activeStep });
	const formField = steps.fields[fieldName];

	const value: TFormValuesDef<T>[T] = (
		values[fieldName]?.data
			? { ...getDefaultInitialValue(), ...values[fieldName] }
			: getDefaultInitialValue()
	) as TFormValuesDef<T>[T]; // It's undefined when changing activeStep
	// console.log({ uff: value, values });
	const ref = getRef(fieldName);
	const error = (() => {
		const err = errors[fieldName];
		if (Array.isArray(err)) {
			return Boolean(err.length);
		}
		return err?.errors;
	})();
	const errorText = (() => {
		const err = errors[fieldName];
		if (Array.isArray(err)) {
			return err[0];
		}
		return err?.message;
	})();
	const required = (() => {
		const steps = formModel.steps;
		let required = false;
		steps.forEach((step) => {
			if (step.stepType === "Constructive") {
				if (step.fields[fieldName]) {
					const requiredValidation = step.fields?.[fieldName]?.validations.isRequired;
					if (typeof requiredValidation === "boolean") {
						required = requiredValidation;
					} else {
						required = requiredValidation?.[0] ?? false;
					}
				}
			}
		});
		return required;
	})();

	return { value, required, ref, error, errorText, field: formField, isLoading: false, ...rest };
};

export default useFormField;
