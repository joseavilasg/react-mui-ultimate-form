import { getTranslations } from "@/plugins/translations";
import dayjs from "dayjs";
import { isPossiblePhoneNumber } from "libphonenumber-js";
import { getI18n } from "react-i18next";

const useFormValidator = () => {
	const i18n = getI18n();
	const translations = getTranslations(i18n.t).formValidator;

	const formValidator = (value: string | string[] | boolean) => {
		const result: string[] = [];

		return {
			isNotArrayEmpty(msg = translations.notArrayEmptyText) {
				if (Array.isArray(value) && value.length === 0) {
					result.push(msg);
				}

				return result;
			},
			isNotEmpty(msg = translations.notEmptyText) {
				if (!(value as string)) {
					result.push(msg);
				}

				return result;
			},

			isEmail(msg = translations.notEmailText) {
				// /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
				if (
					!/^(([^<>()[\]\\.,;:\s@”]+(\.[^<>()[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/.test(
						value as string,
					)
				) {
					result.push(msg);
				}
				return result;
			},
			isNotEmail(msg = "Esto parece ser una dirección de correo") {
				// /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
				if (
					/^(([^<>()[\]\\.,;:\s@”]+(\.[^<>()[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/.test(
						value as string,
					)
				) {
					result.push(msg);
				}
				return result;
			},
			isPhoneNumber(msg = translations.notPhoneNumberText) {
				if (!isPossiblePhoneNumber(value as string)) {
					result.push(msg);
				}
				return result;
			},
			isNumber(msg = translations.notNumberText) {
				if (!/^[-]?\d+$/.test(value as string)) {
					result.push(msg);
				}
				return result;
			},
			isSlug(msg = translations.notSlugText) {
				const regexSlug = /^[a-z0-9-]+$/;
				if (!regexSlug.test(value as string)) {
					result.push(msg);
				}
				return result;
			},
			isLetter(str: string) {
				return str.toLowerCase() != str.toUpperCase();
			},
			// Dates
			// `La fecha no puede ser anterior al año ${dayjs(minDate).year()}`
			isNotLessThan(
				minDate: string,
				msg = translations.notLessThanText(dayjs(minDate).year().toString()),
			) {
				if (value as string) {
					if (dayjs(value as string).diff(minDate) < 0) {
						result.push(msg);
					}
				}
				return result;
			},
			isNotGreaterThanToday(msg = translations.notGreaterThanTodayText) {
				if (value as string) {
					if (dayjs().diff(dayjs(value as string, "DD/MM/YYYY"), "d") <= 0) {
						result.push(msg);
					}
				}
				return result;
			},
			// Checkbox
			isChecked(msg = translations.notCheckedText) {
				if (!value as boolean) {
					result.push(msg);
				}
				return result;
			},
		};
	};
	return { formValidator };
};

export default useFormValidator;
