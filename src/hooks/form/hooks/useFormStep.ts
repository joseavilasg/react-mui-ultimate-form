import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import { useShallow } from "zustand/react/shallow";

import { StepSetStateAction, TUseFormStepParams, TUseFormStepResult } from "@/types/formStepHook";

import removeUndefinedProps from "../utils/removeUndefinedProps";

const useFormStep = <F extends string, T extends string>({
	formModel,
}: TUseFormStepParams<F, T>): TUseFormStepResult => {
	const { formsState, setFormsState } = useBoundStore<F, T, FormsSlice<F, T>>(
		useShallow((s) => s),
	);
	const formState = formsState[formModel.formId];

	const stepsLength = formModel.steps.length;

	if (!formState) {
		return {
			activeStep: 0,
			stepsLength: 1,
			stepType: formModel.steps[0].stepType,
			stepId: formModel.steps[0].stepId ?? "0",
			minTimeMs: formModel.steps[0].minTimeMs ?? 0,
			setActiveStep: () => void 0,
			stepsCompleted: {},
			stepDispatch: () => void 0,
		};
	}

	const { activeStep, stepType, stepId, minTimeMs, stepsCompleted } = formState;

	const setActiveStep: React.Dispatch<React.SetStateAction<number>> = (newState) => {
		if (typeof newState === "function") {
			setFormsState((draft) => {
				draft[formModel.formId] = {
					...draft[formModel.formId],
					activeStep: newState(formState.activeStep),
				};
			});
		} else {
			setFormsState((draft) => {
				draft[formModel.formId] = {
					...draft[formModel.formId],
					activeStep: newState,
				};
			});
		}
	};

	const stepDispatch: React.Dispatch<StepSetStateAction> = (callback) => {
		if (typeof callback === "function") {
			const newState = callback({
				activeStep,
				stepType,
				stepId,
				minTimeMs,
				stepsLength,
				stepsCompleted,
			});
			if (newState) {
				setFormsState((draft) => {
					draft[formModel.formId] = {
						...draft[formModel.formId],
						...removeUndefinedProps(newState),
					};
				});
			}
		} else {
			setFormsState((draft) => {
				draft[formModel.formId] = {
					...draft[formModel.formId],
					...callback,
				};
			});
		}
	};

	return {
		activeStep,
		stepType,
		stepId,
		stepsLength,
		minTimeMs,
		setActiveStep,
		stepsCompleted,
		stepDispatch,
	};
};

export default useFormStep;
