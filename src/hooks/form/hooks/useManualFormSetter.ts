import { useState } from "react";
import { FormsSlice } from "@/zustand/slices/formsStateSlice";
import useBoundStore from "@/zustand/store/useBoundStore";
import { useShallow } from "zustand/react/shallow";

import { TErrors } from "@/types/errors";
import { TFormValuesDef } from "@/types/formModel";
import { TManualFormSetter } from "@/types/manualFormSetter";

const useManualFormSetter = <F extends string, T extends string>({
	formModel,
}: TManualFormSetter<F, T>) => {
	const { formsState, setFormsState } = useBoundStore<F, T, FormsSlice<F, T>>(
		useShallow((s) => s),
	);
	const formState = formsState[formModel.formId];
	const [fieldName, setFieldName] = useState<T>("" as T);

	const setValue = <V>(field: T, value: V) => {
		setFieldName(field);
		setFormsState((draft) => {
			draft[formModel.formId] = {
				...draft[formModel.formId],
				values: {
					...draft[formModel.formId]?.values,
					[field]: value,
				},
			};
		});
	};

	if (!formState) {
		return {
			setValue,
			handleSubmit: () => void 0,
			value: { value: "", type: "" } as unknown as TFormValuesDef<T>[T],
			error: [] as TErrors<TFormValuesDef<T>>[T],
			activeStep: 0,
			isLoading: true,
		};
	}

	const { values, handleSubmit, activeStep, errors } = formState;

	return {
		setValue,
		handleSubmit,
		value: values?.[fieldName] ?? { data: "", type: "" },
		activeStep,
		error: errors?.[fieldName] ?? [],
		isLoading: false,
	};
};

export default useManualFormSetter;
