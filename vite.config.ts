import { resolve } from "path";
import react from "@vitejs/plugin-react-swc";
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";
import eslintPlugin from "vite-plugin-eslint";
import svgr from "vite-plugin-svgr";
import tsConfigPaths from "vite-tsconfig-paths";

import * as packageJson from "./package.json";

const packageName = "react-mui-ultimate-form";

const projectRootDir = resolve(__dirname);

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		dts({
			include: ["."],
			exclude: ["./src/App.tsx", "./src/main.tsx", "./node_modules"],
		}),
		react(),
		tsConfigPaths(),
		svgr(),
		eslintPlugin(),
	],
	resolve: {
		alias: {
			"@": resolve(projectRootDir, "src"),
		},
	},
	build: {
		outDir: "dist",
		sourcemap: false,
		target: "esnext",
		minify: "terser",
		terserOptions: {
			compress: {
				drop_console: true,
				drop_debugger: true,
			},
		},
		cssMinify: "esbuild",
		lib: {
			entry: resolve(".", "index.ts"),
			name: packageName,
			formats: ["es", "umd"],
			fileName: (format) => `${packageName}.${format}.js`,
		},
		copyPublicDir: false,
		rollupOptions: {
			external: [...Object.keys(packageJson.peerDependencies)],
		},
		// rollupOptions: {
		// 	external: ["react", "react-dom"],
		// 	output: {
		// 		globals: {
		// 			react: "React",
		// 		},
		// 	},
		// },
	},
});
