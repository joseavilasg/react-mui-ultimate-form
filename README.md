# React MUI Ultimate Form

Ultimate form controller for your react app, based on MUI.

[![npm version](https://img.shields.io/npm/v/react-mui-ultimate-form.svg?style=flat)](https://www.npmjs.com/package/react-mui-ultimate-form)
[![npm downloads](https://img.shields.io/npm/dm/react-mui-ultimate-form.svg?style=flat)](https://www.npmjs.com/package/react-mui-ultimate-form)
[![MRs Welcome](https://img.shields.io/badge/MRs-welcome-brightgreen.svg)](https://gitlab.com/joseavilasg/react-mui-ultimate-form/-/merge_requests)

This is a personal proyect. If you want to contribute, feel free to do it. I'll be improving this project and will generate the documentation soon.

## Installation

```shell-script
npm i react-mui-ultimate-form
```

#### [Demo](https://joseavilasg.gitlab.io/react-mui-ultimate-form/)

## License

[![GitLab license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/joseavilasg/react-mui-ultimate-form/-/blob/main/LICENSE)
