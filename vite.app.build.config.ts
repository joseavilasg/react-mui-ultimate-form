import { resolve } from "path";
import react from "@vitejs/plugin-react-swc";
import { defineConfig } from "vite";
import eslintPlugin from "vite-plugin-eslint";
import svgr from "vite-plugin-svgr";
import tsConfigPaths from "vite-tsconfig-paths";

const projectRootDir = resolve(__dirname);

// https://vitejs.dev/config/
export default defineConfig({
	base: "",
	plugins: [react(), tsConfigPaths(), svgr(), eslintPlugin()],
	resolve: {
		alias: {
			"@": resolve(projectRootDir, "src"),
		},
	},
	build: {
		outDir: "public",
	},
});
